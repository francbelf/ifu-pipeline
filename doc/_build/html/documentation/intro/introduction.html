

<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<head>
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <title>Introduction &mdash; The TARDIS Pipeline  documentation</title>
  

  
  
  
  

  

  
  
    

  

  <link rel="stylesheet" href="../../_static/css/theme.css" type="text/css" />
  <link rel="stylesheet" href="../../_static/pygments.css" type="text/css" />
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link rel="next" title="Download" href="../download/downloadIntro.html" />
    <link rel="prev" title="The TARDIS Pipeline" href="../../index.html" /> 

  
  <script src="../../_static/js/modernizr.min.js"></script>

</head>

<body class="wy-body-for-nav">

   
  <div class="wy-grid-for-nav">

    
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search">
          

          
            <a href="../../index.html" class="icon icon-home"> The TARDIS Pipeline
          

          
          </a>

          
            
            
          

          
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="../../search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>

          
        </div>

        <div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="main navigation">
          
            
            
              
            
            
              <p class="caption"><span class="caption-text">Contents</span></p>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Introduction</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#purpose-and-functionality">Purpose and Functionality</a></li>
<li class="toctree-l2"><a class="reference internal" href="#philosophy">Philosophy</a></li>
<li class="toctree-l2"><a class="reference internal" href="#screenshots-and-example-plots">Screenshots and Example Plots</a></li>
<li class="toctree-l2"><a class="reference internal" href="#projects">Projects</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="../download/downloadIntro.html">Download</a></li>
<li class="toctree-l1"><a class="reference internal" href="../usage/usage.html">Usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualization/visualization.html">Visualization</a></li>
<li class="toctree-l1"><a class="reference internal" href="../final_remarks/final_remarks.html">Final Remarks</a></li>
</ul>

            
          
        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap">

      
      <nav class="wy-nav-top" aria-label="top navigation">
        
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="../../index.html">The TARDIS Pipeline</a>
        
      </nav>


      <div class="wy-nav-content">
        
        <div class="rst-content">
        
          















<div role="navigation" aria-label="breadcrumbs navigation">

  <ul class="wy-breadcrumbs">
    
      <li><a href="../../index.html">Docs</a> &raquo;</li>
        
      <li>Introduction</li>
    
    
      <li class="wy-breadcrumbs-aside">
        
            
            <a href="../../_sources/documentation/intro/introduction.rst.txt" rel="nofollow"> View page source</a>
          
        
      </li>
    
  </ul>

  
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
            
  <div class="section" id="introduction">
<span id="id1"></span><h1>Introduction<a class="headerlink" href="#introduction" title="Permalink to this headline">¶</a></h1>
<div class="section" id="purpose-and-functionality">
<h2>Purpose and Functionality<a class="headerlink" href="#purpose-and-functionality" title="Permalink to this headline">¶</a></h2>
<p>This pipeline is designed to be a convenient all-in-one framework for the analysis of IFU data from any instrument. This
includes all necessary steps, from the read-in of the reduced IFU-cube, over the preparation of the data, the actual
data analysis, to the automatic generation of publication-quality plots. We further increase convenience by providing a
dedicated visualization software with a graphical user interface.</p>
<p>In particular, the pipeline conducts the following major steps:</p>
<dl class="docutils">
<dt>Data Preparation</dt>
<dd><ul class="first last simple">
<li>Read-in of the IFU-cube</li>
<li>Log-rebinning of the spectra</li>
<li>Voronoi-binning of the data</li>
<li>Preparation of the stellar template library</li>
</ul>
</dd>
<dt>Main Analysis</dt>
<dd><ul class="first last simple">
<li>Extraction of stellar kinematics via unregularized pPXF (denoted PPXF module)</li>
<li>Extraction of stellar population properties via regularized pPXF (denoted SFH module)</li>
<li>Extraction of emission line kinematics via GandALF (denoted GANDALF module)</li>
<li>Extraction of line strength indices and single stellar population equivalent population properties (denoted LS
module)</li>
</ul>
</dd>
<dt>Final Steps</dt>
<dd><ul class="first last simple">
<li>Simplify the inspection of the data products by providing the dedicated visualization software <em>Mapviewer</em></li>
<li>Automatic generation of publication-quality plots during runtime</li>
</ul>
</dd>
</dl>
<div class="line-block">
<div class="line"><br /></div>
</div>
</div>
<div class="section" id="philosophy">
<h2>Philosophy<a class="headerlink" href="#philosophy" title="Permalink to this headline">¶</a></h2>
<p>The aim of this project is to provide a sophisticated IFU analysis pipeline that has the following properties:</p>
<ul class="simple">
<li><strong>Convenient</strong> It is convenient by providing an all-in-one framework from the read-in of the data, over the analysis, to the automatic production of publication-quality plots</li>
<li><strong>General</strong>: It works well with different instruments in the context of different collaborations</li>
<li><strong>Modular</strong>: It allows to turn individual analysis modules on/off or replace them without affecting the integrity of the analysis procedure</li>
<li><strong>Parallel</strong>: It is equipped with a stable parallelisation supporting the use of large cluster systems and thus the rapid processing of large amounts of data</li>
<li><strong>Extensive</strong>: It is extensive enough to cope with a variety of analysis procedures (amongst others pPXF and GandALF) and their many different flavours</li>
<li><strong>Flexible</strong>: It is flexible enough to work on bins, spaxels as well as manually defined apertures</li>
<li><strong>Intelligent</strong>: It automatically detects already available output and continues the analysis from there. It further allows to reuse of results (e.g. the Voronoi binning) between similar runs</li>
<li><strong>Visualization</strong>: It provides the dedicated visualization software <em>Mapviewer</em> with an advanced and fully interactive GUI, as well as automatically generated publication-quality plots</li>
<li><strong>Clean Code</strong>: The code is clean and well-structured, making specific modifications by users easy</li>
<li><strong>Expandable</strong>: The code is designed is such a way to simplify the implementation of add-ons in the analysis framework (e.g. modules for Steckmap or pyParadise)</li>
<li><strong>Ongoing Development</strong>: It is used by various collaborations, actively maintained, and further analysis modules are in development</li>
</ul>
<p>I emphasize again that thanks to the very modular design of the pipeline, it is easy to modify, replace and expand
this code to meet the specific requirements of your project. In particular, it is easily possible to, for instance,
replace the GandALF module by pyPARADISE, in order to conduct the emission line analysis. Similarly, one can add
specific modules, or simply replace a few functions, to e.g. facilitate an alternative binning scheme or emission line
subtraction technique.</p>
<p><em>Please don’t hesitate to get in touch with me if you have any questions reagarding the applicability of this code to
your project.</em></p>
<div class="line-block">
<div class="line"><br /></div>
</div>
</div>
<div class="section" id="screenshots-and-example-plots">
<h2>Screenshots and Example Plots<a class="headerlink" href="#screenshots-and-example-plots" title="Permalink to this headline">¶</a></h2>
<p>THESE FIGURES WILL BE REPLACED WITH FIGURES OF THE FULL RUN THAT WILL BE PRODUCED FOR THE PAPER!!!</p>
<div class="figure" id="id2" style="width: 100%">
<img alt="../../_images/mapviewerScreenshot.png" src="../../_images/mapviewerScreenshot.png" />
<p class="caption"><span class="caption-text">Screenshot of the visualization software <em>Mapviewer</em>. Shown is the stellar velocity map of NGC???? as well as the
underlying spectra, fits and residuals. Latter can be plotted in the right-hand column of the software by simply
performing a mouse-click on a particular position in the map.</span></p>
</div>
<a class="reference internal image-reference" href="../../_images/NGC0000_ppxf.pdf"><img alt="../../_images/NGC0000_ppxf.pdf" src="../../_images/NGC0000_ppxf.pdf" style="width: 49%;" /></a>
<a class="reference internal image-reference" href="../../_images/NGC0000_lambdaR.pdf"><img alt="../../_images/NGC0000_lambdaR.pdf" src="../../_images/NGC0000_lambdaR.pdf" style="width: 49%;" /></a>
<p><em>Example plots of the galaxy NGC???? as produced by the pipeline. Shown are maps of the stellar kinematics (left plot)
and the lambda_r parameter (right plot). The plotted quantities are displayed in the upper left of each panel while the
minimum and maximum displayed values are stated in the lower right.</em></p>
<div class="line-block">
<div class="line"><br /></div>
</div>
</div>
<div class="section" id="projects">
<h2>Projects<a class="headerlink" href="#projects" title="Permalink to this headline">¶</a></h2>
<p>This code has already been used in the data analysis of the <a class="reference external" href="https://www.muse-timer.org">TIMER</a> and
<a class="reference external" href="http://www.na.astro.it/Fornax3D/Fornax3D/Welcome.html">F3D</a> collaborations.</p>
<a class="reference external image-reference" href="https://www.muse-timer.org"><img alt="../../_images/timer.png" src="../../_images/timer.png" style="width: 20%;" /></a>
<div class="line-block">
<div class="line"><br /></div>
</div>
</div>
</div>


           </div>
           
          </div>
          <footer>
  
    <div class="rst-footer-buttons" role="navigation" aria-label="footer navigation">
      
        <a href="../download/downloadIntro.html" class="btn btn-neutral float-right" title="Download" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right"></span></a>
      
      
        <a href="../../index.html" class="btn btn-neutral" title="The TARDIS Pipeline" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left"></span> Previous</a>
      
    </div>
  

  <hr/>

  <div role="contentinfo">
    <p>
        &copy; Copyright 2018, Adrian Bittner.

    </p>
  </div>
  Built with <a href="http://sphinx-doc.org/">Sphinx</a> using a <a href="https://github.com/rtfd/sphinx_rtd_theme">theme</a> provided by <a href="https://readthedocs.org">Read the Docs</a>. 

</footer>

        </div>
      </div>

    </section>

  </div>
  


  

    <script type="text/javascript">
        var DOCUMENTATION_OPTIONS = {
            URL_ROOT:'../../',
            VERSION:'',
            LANGUAGE:'None',
            COLLAPSE_INDEX:false,
            FILE_SUFFIX:'.html',
            HAS_SOURCE:  true,
            SOURCELINK_SUFFIX: '.txt'
        };
    </script>
      <script type="text/javascript" src="../../_static/jquery.js"></script>
      <script type="text/javascript" src="../../_static/underscore.js"></script>
      <script type="text/javascript" src="../../_static/doctools.js"></script>

  

  <script type="text/javascript" src="../../_static/js/theme.js"></script>

  <script type="text/javascript">
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>