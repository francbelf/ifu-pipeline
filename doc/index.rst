.. IFU Analysis Pipeline documentation master file, created by
   sphinx-quickstart on Mon Sep 17 12:18:43 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

The TARDIS Pipeline
=====================

**TARDIS**: **T**\ ool for the **A**\ nalysis of **R**\ educed **D**\ ata from **I**\ ntegral-field **S**\ pectroscopy


|

Authors
**************
To be determined. 


|

Abstract
**************
Not yet. 


|

.. note:: 
   Please do not forget to quote Bittner et al. (2019) if you use this pipeline in any publication. 

.. toctree::
   :maxdepth: 1
   :caption: Contents

   documentation/intro/introduction
   documentation/download/downloadIntro
   documentation/usage/usage
   documentation/visualization/visualization
   documentation/final_remarks/final_remarks

