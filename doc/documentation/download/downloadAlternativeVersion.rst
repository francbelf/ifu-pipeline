.. _downloadAlternativeVersion: 

*****************************
Download Alternative Versions
*****************************

On this page you can download alternative versions of the pipeline that have been adapted by users/collaborations to 
meet their specific needs. 

.. note:: 
   Please do not forget to quote Bittner et al. (2019) if you use this pipeline for any publication. 

|

F3D / M. Sarzi Version
-----------------------------


This version differs from the main version of the pipeline in that it supports the extraction of an optimal spectral
template library. In other words, instead of using, for instance, the full EMiles template library for the analysis one
can manually extract a smaller spectral library that is specifically designed for the galaxy in consideration. As this
optimal template library contains fairly less templates than the EMiles library, the analysis will be substantially
faster. 


Extraction of the optimal template library
"""""""""""""""""""""""""""""""""""""""""""

The usage of an optimal template library is fully controlled by the Config-file switch ``OPT_TEMP``. The switch accepts
two comma-separated values, e.g. ``1,1000``. The first value defines the mode of the template extraction: ``0`` turns
the extraction off and uses the default spectral library (e.g. EMiles) instead. ``1`` extracts optimal templates based
on a Voronoi tesselation at a signal-to-noise ratio given by the second value which is passed to ``OPT_TEMP``. ``2``
opens a graphical-user interface which allows the user to manually define apertures and use the integrated spectra of
these apertures in the following. 

The extracted spectra, regardless of the extraction procedure, are passed to pPXF and Gandalf, analysed with the
standard spectral library (e.g. EMiles), and the optimal combination of templates returned by these routines is saved as
optimal spectral template library. This library is then used in the following. 


Further Differences
""""""""""""""""""""""""""""

Moreover, the MC simulations of PPXF will use as spectral template only the one optimal template returned by pPXF for
the particular bin in consideration. 

If Gandalf is executed in SPAXEL level with the optimal template library, the initial guess on the stellar kinematics
passed to Gandalf is *not* based on the results of the PPXF module. Instead, Gandalf is first executed on BIN level to
extract an optimal template for each Gandalf bin. Subsequently, pPXF is rerun on SPAXEL level using the one optimal
template extracted during the BIN level run of Gandalf. The Gandalf run is then performed on SPAXEL level, using the one
optimal template extracted during the Gandalf BIN level run, and the initial guesses on the stellar kinematics extracted
from the SPAXEL level PPXF run. 


|

**For further details, please have a careful look at the source code or contact Marc Sarzi.**

* `F3D/Sarzi Version, 01.07.2018 <https://apod.nasa.gov/>`_

|

