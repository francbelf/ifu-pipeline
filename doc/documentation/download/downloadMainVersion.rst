.. _downloadMainVersion: 

***********************
Download the Pipeline
***********************

On this page you can download the main version of the pipeline. While the first link contains only the source code of
the pipeline, the second link also includes an example IFU-cube, pre-defined configuration files, the necessary
directory structure and should work readily.  Please see :ref:`tutorial` for further details on the second version. 

* `Pipeline V1.0, 01.07.2018 <https://apod.nasa.gov/>`_
* `Tutorial V1.0, 01.07.2018 <https://apod.nasa.gov/>`_


.. note:: 
   Please do not forget to quote Bittner et al. (2019) if you use this pipeline for any publication. 

|

**Release Notes**

* **V1.0**, 01.07.2018: First version



|   

