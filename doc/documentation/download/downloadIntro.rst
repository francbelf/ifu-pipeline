.. _DownloadIntro:

*******************
Download
*******************

On the following pages you can download the pipeline. There is a main version maintained by myself which is probably
most suitable for the majority of the users.  

Other users/collaborations have slightly adapted the pipeline to their
specific needs. Fortunately, some of these users have agreed to share their adaptions of the code on this webpage. These
valuable resources can be downloaded in the section with alternative versions. 

.. toctree::
   :maxdepth: 1

   downloadMainVersion
   downloadAlternativeVersion
