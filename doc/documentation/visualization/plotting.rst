.. _plotting:

===================
Plotting Routines
===================

The TARDIS pipeline produces publication-quality plots of relevant outputs automatically during runtime. Please find
examples of all available plots below. 

In addition, the same plotting routines the pipeline calls during runtime can also be executed independently from the
pipeline, in particular to adapt the minimum/maximum values displayed in the maps as well as the isophotes. Upon
execution of the plotting the routines, you will be prompted to input the minimum level of the isophotes, as well as all
minimum/maximum values to be displayed for each quantity. Once you entered all values, the routines will display a preview of the
plot that will be produced. Close this plot and state whether you want to save it, or restart with entering new
values. To keep the values from the previous iteration, just hit ENTER. Iterate until you are satisfied with your plot. 

Please note that negative tick labels and axis labels might appear shifted in vertical direction during the preview of
the plot.  Unfortunately, this behaviour is a known matplotlib bug. However, when saving the plot to a pdf-file this
behaviour should disappear naturally.  If the problem does not disappear, you can try to use ``import matplotlib;
matplotlib.use('pdf')`` manually *BEFORE* importing any other matplotlib module! Note that in this case the plot cannot
be displayed but only saved to a pdf-file. 

Please find below an overview of the available plotting routines and their call sequences.



|

Stellar Kinematics
-----------------------

:: 

   ./utilities/util_plot_kinematics.py -r RUNNAME -f FLAG

with ``RUNNAME`` being the runname, e.g. "NGC0000_Example", and flag defining the module, e.g. "PPXF" or "SFH". 

.. figure:: ../../_static/NGC0000_ppxf.pdf
   :figwidth: 100%

   THIS FIGURE WILL BE REPLACED WITH A PLOT OF THE FULL RUN THAT WILL BE PRODUCED FOR THE PAPER!!!

|

Lambda_R Parameter
----------------------

::

   ./utilities/util_plot_lambdar.py -r RUNNAME

with ``RUNNAME`` being the runname, e.g. "NGC0000_Example". 

.. figure:: ../../_static/NGC0000_lambdaR.pdf
   :figwidth: 100%

   THIS FIGURE WILL BE REPLACED WITH A PLOT OF THE FULL RUN THAT WILL BE PRODUCED FOR THE PAPER!!!

|

Emission Line Kinematics
--------------------------

::

   ./utilities/util_plot_gandalf.py -r RUNNAME -l LEVEL -e LINE -t PLOTTYPE -a AON_THRESHOLD

with ``RUNNAME`` being the runname, e.g. "NGC0000_Example" and ``LEVEL`` being either "BIN" or "SPAXEL". ``LINE`` is the
LINE_ID, as specified in the first extension of "_gandalf_*.fits" while ``PLOTTYPE`` states the quantity to be plotted
("V" velocity, "Sigma" velocity dispersion, "A" amplitude, "F" flux). ``AON_THRESHOLD`` specifies the minimum
amplitude-over-noise ratio for the emission line to be displayed in the map. 

.. figure:: ../../_static/NGC0000_gandalf-[OIII]5006.77_V_BIN.pdf
   :figwidth: 100%

   THIS FIGURE WILL BE REPLACED WITH A PLOT OF THE FULL RUN THAT WILL BE PRODUCED FOR THE PAPER!!!

|

Stellar Population Properties
--------------------------------

::

   ./utilities/util_plot_spp.py -r RUNNAME -f FLAG

with ``RUNNAME`` being the runname, e.g. "NGC0000_Example", and "FLAG" defining the module, e.g. "SFH" or "LS". 

.. figure:: ../../_static/NGC0000_spp_ppxf.pdf
   :figwidth: 100%

   THIS FIGURE WILL BE REPLACED WITH A PLOT OF THE FULL RUN THAT WILL BE PRODUCED FOR THE PAPER!!!

|

Line Strength Indices
---------------------------

::

   utilities/util_plot_ls.py -r RUNNAME

with ``RUNNAME`` being the runname, e.g. "NGC0000_Example". 

.. figure:: ../../_static/NGC0000_ls.pdf
   :figwidth: 100%

   THIS FIGURE WILL BE REPLACED WITH A PLOT OF THE FULL RUN THAT WILL BE PRODUCED FOR THE PAPER!!!



|

