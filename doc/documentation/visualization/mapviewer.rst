.. _mapviewer:

====================
Mapviewer
====================

The TARDIS pipeline generates a large variety of high-level data products, including for instance various maps, spectra
and their fits. To simplify the access and visual inspection of these data products, we provide a *Mapviewer* with an
advanced graphical-user interface that is specifically designed for this purpose. In particular, it is neither part of
the analysis procedure nor capable of setting the configurations for the analysis. Instead, this routine is a highly
optimized and interactive data visualization application. Figure 1 displays a screenshot of the *Mapviewer* with 
the output of the galaxy NGC????. The usage of this routine will be described in the following. 


.. figure:: ../../_static/mapviewerScreenshot.png
   :figwidth: 100%

   **Fig. 1:** Screenshot of the *Mapviewer* illustrating the output of the pipeline for the galaxy NGC????.
   THIS FIGURE WILL BE REPLACED WITH A SCREENSHOT OF THE FULL RUN THAT WILL BE PRODUCED FOR THE PAPER!!!



|

Start-Up
--------------------

The *Mapviewer* can be run from within the pipeline directory with the command::

   ./Mapviewer

Upon start-up of the software a file-open dialogue will open. Use this dialogue to select the output you intend
to display with the *Mapviewer*. Please note that that you cannot select a specific output file ("\*.fits") but instead
the entire output directory you are interested in (e.g. ``data/NGC0000/NGC0000_Example/``). 


The *Mapviewer* provides different modes for the individual modules of the pipeline. More specifically, the different
modes provide an optimized layout for the illustration of the results, in particular for the SFH and LS modules, while
reducing the memory requirements of the routine as only the selected part of the output is loaded. In order to run the
*Mapviewer* in a specific mode, run the command::

   ./Mapviewer -m MODE

with MODE being the name of the respective module (PPXF, GANDALF, SFH, LS; not case-sensitive) or its first letter. 



|

Basics
--------------------

.. _Emsellem et al. 2007: https://ui.adsabs.harvard.edu/#abs/2007MNRAS.379..401E/abstract

.. |changeLimits| image:: ../../_static/changeLimits.png 
   :scale: 30%


The Main Plots
""""""""""""""""""""

The standard mode of the *Mapviewer* consists of the following plots: In the upper left the map of the selected quantity
is displayed. Below the map, there is a plot of the non-parametric star-formation history as extracted by the
regularized run of pPXF. The right-hand column displays the spectra (black), best fits (red) and residuals (green) of
the non-regularized pPXF run (first panel), Gandalf run (second panel) and regularized pPXF run (third panel).  Masked
regions of the spectral range are highlighted in grey and their residuals in blue.  The fourth panel displays the
weights of the templates in the age-metallicity grid from the regularized pPXF run.  If any of the data is not
available, the plot stays empty. 

To plot spectra in the right-hand column of the *Mapviewer*, just click on the corresponding bin/spaxel in the map.
Alternatively, you can select the spectra according to their bin/spaxel ID (see :ref:`selectID_dialogue`).

Please note that all of these panels are fully interactive with the matplotlib standard toolcase, in particular
zooming/moving/saving of the plots are possible. Be reminded that the bin selection by clicking on the map is not
possible as long as any of the matplotlib tools are selected. 

I highlight the possibility to modify the minimum/maximum value displayed in the map. To this end, click on the "Edit
axis" |changeLimits| button in the matplotlib toolbar. Then select the axes you intend to modify, click on the tab
"Images" and adapt "Min. value" and "Max. value" as desired. 



|

Menu Bar
"""""""""""""""""""""

* File

   * ``Open``: Open another output directory
   * ``Settings``: Open the settings dialogue (see :ref:`settings_dialogue`)
   * ``Select ID``: Open the bin/spaxel selection dialogue (see :ref:`selectID_dialogue`)
   * ``Info``: Open the info dialogue (see :ref:`info_dialogue`)
   * ``Exit``: Exit the *Mapviewer*

* Table

   * ``BIN_ID``: The BIN_ID per spaxel
   * ``Flux``: The Flux per spaxel
   * ``SNR``: The signal-to-noise ratio per spaxel
   * ``SNRbin``: The signal-to-noise ratio per bin
   * ``NSPAX``: The number of spaxels per bin


* PPXF

   * ``V``, ``SIGMA``, ``H3``, ``H4``: The properties of the line-of-sight velocity distribution as extracted by pPXF. 
   * ``LAMBDA_R``: The lambda_r parameter as proxy for the projected stellar angular momentum (see `Emsellem et al.
     2007`_)


* Gandalf

   * ``[OIII]_5006.77``: The kinematics of the given emission line. The emission line is specified in the form
     "LineIdentifier_Wavelength". The submenu allows to select velocity, velocity dispersion, line flux and amplitude.  

* SFH

   * ``Metals``, ``Age``, ``Alpha``: The stellar population properties extracted in the regularized pPXF run.
   * ``V``, ``SIGMA``, ``H3``, ``H4``: The stellar kinematics extracted during the run of the regularized pPXF. If the
     stellar kinematics are fixed to those of the non-regularized run of pPXF, these kinematics will be identical with
     those in the "PPXF" menu.
   * ``V_Dif``, ``SIGMA_Dif``, ``H3_Dif``, ``H4_Dif``: The absolute difference of the stellar kinematics between the run
     of the non-regularized and regularized pPXF. If the stellar kinematics are fixed to those of the non-regularized
     run of pPXF, these maps will be 0.0 everywhere.

* Line Strength

   * ``LSAge``, ``LSMetal``, ``LSAlpha``: Single stellar population equivalent population properties as estimated from
     the line strength indices. 
   * ``Fe5015``: The line strength indices, with names as defined in the configuration file "ls_bands.conf". 


|

Options
--------------------



.. _settings_dialogue:

Settings Dialogue
""""""""""""""""""""


.. figure:: ../../_static/settings.png
   :align: right
   :figwidth: 25%
    
The settings dialogue allows to set a few major settings of the *Mapviewer*:

   * Restrict to Voronoi region: If this checkbox is set, all spaxels that do not meet the minimum SNR threshold
     will not be displayed. If this is unset, those spaxel will be displayed with the value of their nearest
     Voronoi-bin.
   * Select whether the results of Gandalf are displayed on bin or spaxel level. Please note that this option is only
     available if results for both the bin and spaxel level are available. 
   * Set the minimum AoN threshold for emission lines to be displayed in the map. 
   * Set the color of the bin and spaxel marker.



.. _info_dialogue:

Info Dialogue
""""""""""""""""""""

.. figure:: ../../_static/info.png
   :align: right
   :figwidth: 25%

The info dialogue simply displays the configurations under which the current run has been performed. More specifically,
this displays the content of the ``USED_PARAMS.fits`` file. 


|
|
|
|
|
|
|


.. _selectID_dialogue:

Bin/Spaxel Selection Dialogue
""""""""""""""""""""""""""""""

.. figure:: ../../_static/selectBINID.png
   :align: right
   :figwidth: 25%

By default, the bin or spaxel which spectrum is displayed in the right-hand part of the *Mapviewer* is selected by
clicking on the map. However, the bin/spaxel can also be selected by its ID. To this end, open the bin/spaxel selection
dialogue, enter the bin or spaxel ID and hit the corresponding button. If the ID is not an integer number or not
available in the bin/spaxel ID list, the textbox will display "Error!". 




|

