.. _visualization: 

***************
Visualization
***************

In this section I describe the visualization options that the TARDIS pipeline provides. In particular, the pipeline is 
equipped with the dedicated visualization software *Mapviewer* that facilitates a graphical-user interface. In addition, 
publication-quality plots are produced during runtime of the pipeline. 

.. toctree::
   :maxdepth: 1
   
   mapviewer
   plotting

