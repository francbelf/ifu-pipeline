.. _installation:

Installation
======================

This pipeline does *not* require an installation. The only necessary steps are 

* Install and activate the conda environment
* Setup the directory structure
* Place input and configuration files

Please find a description of theses steps below. 

|

Conda Environments
""""""""""""""""""""""
Install and activate the conda environment ``requirementsPython3.yml`` of the pipeline::

    conda env create -f requirementsPython3.yml
    conda activate TardisPipeline

For further instructions on the use and management of conda environments, please see the 
`Conda Documentation <https://conda.io>`_.



Analysis Software
-------------------
The conda environment automatically installs suitable versions of the required scientific software, in particular pPXF
(see Cappellari et al. `2004 <https://ui.adsabs.harvard.edu/?#abs/2004PASP..116..138C/abstract>`_, `2017
<https://ui.adsabs.harvard.edu/?#abs/2017MNRAS.466..798C>`_), and the Voronoi binning method (see Cappellari and Copin
`2003 <https://ui.adsabs.harvard.edu/?#abs/2003MNRAS.342..345C>`_).  If one prefers to use a specific version of these
methods, just place the corresponding source code in ``sitePackages/ppxf`` and ``sitePackages/voronoi``. The pipeline
will preferably use the source code available in these directories before importing the system-wide installed version of
the routines. 

.. warning::
   WHAT ABOUT GANDALF AND THE LS-CODE???



Compatible Python Versions
---------------------------
The pipeline is designed to run with Python 3. In principle, the code should also support Python 2.7, however, as many
public packages (see `Python3Statement <https://python3statement.org>`_), including pPXF and the Voronoi binning method,
will or already have dropped Python 2.7 support, I strongly encourage everybody to use the pipeline together with Python
3. 

.. warning::
   WHAT ABOUT THE ERROR CALCULATION OF GANDALF???



Parallelisation
--------------------------
The parallelisation of the pipeline uses the multiprocessing module of Python's Standard Library. In particular, it uses
multiprocessing.Queue and multiprocessing.Process providing a maximum of stability and control over the parallel
processes.  In addition, the threading/parallelisation of other Python native modules, such as numpy or scipy, is
suppressed. Thus, the number of active processes should never exceed the number of cores defined in the Config-file, nor
should any process be able to claim more than 100% CPU usage. 

The drawback of the Python multiprocessing module is that it does not natively support the use of multiple nodes on
large computing clusters. However, at this point the use of one node (with e.g. 32 cores) should be sufficient for most
kinds of analysis. Implementing a distributed memory parallelisation in the TARDIS pipeline is nevertheless a long-term
objective. 

The implemented parallelisation has been tested on various machines: This includes hardware from laptop up to
cluster-sized systems, as well as Linux and MacOS operating systems. 



|

Directory Structure
""""""""""""""""""""""

The pipeline enforces the usage of the following directory structure. Please note that the galaxy name used in 
point 20, 21 and 22 must be identical. A short description of the files and directories is provided below: 
::

 1 |  projectDirectory/
   |  │ 
 2 |  ├─ pipeline/  
 3 |  │  ├─ Config
 4 |  │  ├─ MainPipeline.py*
 5 |  │  ├─ Mapviewer.py
 6 |  │  ├─ configurationTemplates/
 7 |  │  ├─ doc/
 8 |  │  ├─ mapviewer/
 9 |  │  ├─ readData/
 10|  │  │  ├─ [Instrument].py
 11|  │  │  └─ LSF-Config_[Instrument]
 12|  │  ├─ requirementsPython3.yml
 13|  │  ├─ sitePackages/
 14|  │  ├─ spectralTemplates/
 15|  │  │  ├─ [Library]/
 16|  │  │  └─ [Library].fits
 17|  │  └─ utilities/
 18|  │     └─ util_plot_*.py
   |  │ 
 19|  └─ data
 20|     └─ NGC0000/
 21|        ├─ NGC0000.fits
 22|        └─ NGC0000_[RunName]
 23|           ├─ USED_PARAMS.fits
 24|           ├─ emission_lines.setup
 25|           ├─ ls_bands.conf
 26|           ├─ [Outputs]
 27|           └─ maps/ 


#. ``projectDirectory/``: The main directory, e.g. the name of your project
#. ``pipeline/``: Contains the entire pipeline code
#. ``Config``: The main configuration file
#. ``MainPipeline.py*``: The main pipeline routine. Use this file to start the pipeline
#. ``Mapviewer.py*``: The *Mapviewer* routine
#. ``configurationTemplates/``: Contains templates for all configuration files. If the required configuration files are
   not provided in the output directory, these configuration files will be copied and used instead. 
#. ``doc/``: Contains this documentation
#. ``mapviewer/``: Contains the source code of the *Mapviewer* routine
#. ``readData/``: Contains the read-in routines and their corresponding LSF's
#. ``[Instrument].py``: The read-in routine. [Instrument] is e.g. MUSE-WFM and corresponds to the *IFU* keyword in the Config-file
#. ``LSF-Config_[Instrument]``: Specifies the LSF of [Instrument]
#. ``requirementsPython3.yml``: The conda-environment file
#. ``sitePackages/``: May contain local copies of the scientific software (e.g. pPXF) to be used instead of the
   system-wide installed versions. 
#. ``spectralTemplates/``: Contains the spectral template libraries
#. ``[Library]/``: Directory that contains the spectral templates. The directory name [Library] corresponds to the *SSP_LIB* keyword in the Config-file. 
#. ``[Library].fits``: Contains the model file which specifies the relation between line strength indices and stellar
   population properties. 
#. ``utilities/``: Contains the pipeline code
#. ``util_plot_*.py``: The plotting routines of the pipeline. These can be called independently of the pipeline to redo the plots 
#. ``data``: The data directory: Contains input and output data
#. ``NGC0000/``: Input and output of the galaxy NGC0000. *Note*: No underscore in the galaxy name allowed!
#. ``NGC0000.fits``: Reduced IFU cube. *Note*: The filename must be [Galaxy].fits. No underscore or other changes allowed!
#. ``NGC0000_[RunName]``: Output directory of the run as specified with the *RUN_NAME* keyword in the Config-file
#. ``USED_PARAMS.fits``: Saves the parameters the pipeline was executed with. *Do NEVER delete this file, as the pipeline checks the parameters in this file and the Config-file for consistency when the pipeline is restarted.*  
#. ``emission_lines.setup``: Files specifying all relevant lines. Used to mask lines in pPXF and fit lines in GandALF
#. ``ls_bands.conf``: File specifying the bands for the measurements of line strength indices
#. ``[Outputs]``: All output files. Please see :ref:`output` for further details
#. ``maps/``: Contains the publication-quality plots produced on runtime

|

