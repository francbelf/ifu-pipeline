.. _running: 

Running the Pipeline
======================

Once the reduced IFU cube is in place and the configuration completed, the pipeline is ready to be run. You can
start the pipeline from within the pipeline directory with the command::

    ./MainPipeline

.. figure:: ../../_static/runningPipeline.png
   :align: right
   :figwidth: 50%

There is no need to pass any command-line arguments, as all configuration switches and parameters are passed via the
Config-files. 

On startup, the pipeline prints all settings from the configuration files.  During runtime, it informs about every
executed/skipped step by printing a message and/or progress bar in the console as well as in the logfile. It further
informs about errors and warnings in the analysis. 

|
|


Stuff that comes along handy
------------------------------

Checks at Start-Up
""""""""""""""""""
At start-up the code checks the parameters in the Config-file for feasibility. Common mistakes are detected
automatically and handled in a suitable way. Further warnings are printed to screen and logfile if physically not
sensible combinations of parameters are passed. 

|

Restarting the Pipeline
"""""""""""""""""""""""""
Before starting the analysis, the code checks for output data that might already be available in the output directory of
the run. If the full set of output files is already available, the galaxy is skipped and the analysis continues with the
next galaxy. If only a part of the output is readily available, the pipeline continues the analysis from there. I
highlight that this allows to stop and later continue the analysis after the completion of each module. For instance,
one might first complete the extraction of stellar kinematics for a set of galaxies before starting the analysis of
emission line kinematics or population properties. 

In case a run that already produced output is restarted, the pipeline checks that the parameters in the current
Config-file and those saved in the output directory of the given run are identical. If those parameters are not
identical, the code prints an error message stating the deviating parameters and skips the analysis of this galaxy. Of
course, parameters denoted "Main Switches" in the Config-file are ignored in this consistency check.  I highlight that
this behaviour can be avoided by either changing the parameters in the Config-file or the corresponding values in the
header of the ``USED_PARAMS.fits``. 

|

Recycling of data products
""""""""""""""""""""""""""
Further, it is possible to reuse existing data products. For instance, if one intends to execute several different tests
on the same galaxy while exploiting an identical binning scheme for those tests, one can just copy the ``*_table.fits``
file in the output directories of all planned runs. The pipeline automatically detects the file, and reuses it for the
current analysis. I emphasize that this is not only possible with the ``*_table.fits`` file, but with all relevant
outputs. 

|

Measurements in a given Aperture
""""""""""""""""""""""""""""""""""
The TARDIS pipeline naturally supports the analysis of spectra integrated in manually defined apertures, e.g. to conduct
measurements of the central velocity and velocity dispersion of a galaxy: 

1. Start the analysis with your favoured setup
2. Abort the analysis after the Voronoi-binning has completed. Make sure that the ``*_table.fits`` exists in the output
   directory while ``*_AllSpectra.fits``, ``*_VorSpectra.fits`` and ``*_VorSpectra_linear.fits`` have not been written
   yet. Alternatively, just delete latter files. 
3. Manually redefine the Voronoi bins in ``*_table.fits``: For instance, assign all spaxels that are within the spatial
   aperture to ``BIN_ID`` 0, while all other spaxels are assigned to ``BIN_ID`` 1. 
4. Restart the analysis: The pipeline reuses the ``*_table.fits`` file and thus runs on only two bins with ``BIN_ID`` =
   0 corresponding to your manually defined aperture.  






|

