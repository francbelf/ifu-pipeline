.. _output: 

Output
==============


This section provides a throughout overview of all output files produced by the different modules of the pipeline. 
I further explain all column specifiers used within the various fits-tables. 

**Mapping bins to spaxels:**
The file ``NGC0000_table.fits`` contains all necessary information to reconstruct the Voronoi binning, map bins to
spaxels and vice versa. In particular, this file contains one line per spaxel and states the BIN_ID as well as spatial
coordinates of every spaxel. 

In all output files that contain one row per bin, the row number corresponds to the BIN_ID given in
``NGC0000_table.fits``. In output files consisting of one row per spaxel, the rows are sorted in the same way as in
``NGC0000_table.fits`` so that these tables can be matched one-to-one. 



|

General Outputs
----------------
- ``LOGFILE``: Logfile of the pipeline run
- ``USED_PARAMS.fits``: Saves the parameters of the pipeline run in the header of the primary extension. The LSF is saved in extension 1. *Do not delete this file, as the pipeline checks the parameters in this file and the Config-file for consistency whenever the pipeline is restarted.*  
- ``maps/``: Directory containing the produced maps



|

Preparatory Steps
------------------
* ``NGC0000_table.fits``: Contains information to reconstruct the Voronoi-binning. Most importantly, it defines a ``BIN_ID`` for each spaxel. 

   - **Columns**: ``ID`` Spaxel ID \| ``BIN_ID`` Bin ID \| ``X`` ``Y`` Pixel coordinates of spaxels in arcsec \| ``XBIN`` ``YBIN`` Pixel coordinates of bins in arcsec \| ``FLUX`` Flux in the spaxel \| ``SNR`` Signal-to-noise ratio in the spaxel \| ``SNRBIN`` Signal-to-noise ratio in the bin \| ``NSPAX`` Number of spaxels in the bin
   - **Rows**: *One line per spaxel.*

* ``NGC0000_AllSpectra.fits``: Contains the spectrum of each spaxel. 
  
   - **Columns**: ``SPEC`` Spectra \| ``ESPEC`` Variance spectra
   - **Rows**: *One line per spaxel.*

* ``NGC0000_VorSpectra.fits``: Contains the Voronoi-binned spectra equally spaced in velocity domain (logarithmically binned). 
  
   - **Columns**: ``SPEC`` Spectra \| ``ESPEC`` Variance spectra
   - **Rows**: *One line per bin.*

* ``NGC0000_VorSpectra_linear.fits``: Contains the Voronoi-binned spectra equally spaced in wavelength domain (linearly binned). 
  
   - **Columns**: ``SPEC`` Spectra \| ``ESPEC`` Variance spectra
   - **Rows**: *One line per bin.*



|

pPXF
------------------
- ``NGC0000_ppxf.fits``: Results of the extraction of stellar kinematics and their errors
  
   - **Columns**: ``BIN_ID`` Bin ID \| ``V`` ``SIGMA`` ``H3`` ``H4`` ``H5`` ``H6`` Stellar kinematics \| ``ERR_*`` Errors on the stellar kinematics from MC-simulations \| ``FORM_ERR_*`` Formal errors on the stellar kinematics \| ``LAMBDA_R`` Lambda_R parameter as proxy for the stellar angular momentum
   - **Rows**: *One line per bin.*
   
- ``NGC0000_ppxf-optimalTemplates.fits``: Optimal template for the fit on each bin. 
  
   - **Columns**: ``OPTIMAL_TEMPLATES`` Optimal templates
   - **Rows**: *One template per bin.*

- ``NGC0000_ppxf-goodpix.fits``: Array of pixels that are not masked during the fit
  
   - **Columns**: ``GOODPIX`` Array of not masked spectral pixels
   - **Rows**: *One row only*

- ``NGC0000_ppxf-bestfit.fits``: The best fit to the spectrum. 
  
   - **Columns**: ``BIN_ID`` Bin ID \| ``BESTFIT`` The best fit to the spectrum
   - **Rows**: *One fit per bin.*



|

GandALF
------------------
All GandALF output can be available at both ``BIN`` and ``SPAXEL`` level. 

- ``NGC0000_gandalf_BIN.fits``, Extension 1: 

   - **Columns**: ``LINE_ID`` The ID of the emission line. This corresponds to the index of the third dimension in extension 2. \| ``i`` ``name`` ``_lambda`` ``action`` ``kind`` ``a`` ``v`` ``s`` ``fit`` The fit parameters as specified in "emission_line.setup"
   - **Rows**: *One line per specified emission line.*

- ``NGC0000_gandalf_BIN.fits``, Extension 2: 

   - **Columns**: ``FLUX`` Flux of the line \| ``AMPL`` Amplitude of the line \| ``V`` ``SIGMA`` Emission line kinematics \| ``AON`` Amplitude-over-noise ratio \| ``EBMV`` Measured extinction
   - **Rows**: *One line per bin/spaxel.*
   - **Third dimension**: Every element in this table (row,columns) contains as many values as lines are defined in Extension 1. The line ID specified in Extension 1 is the index of the array in this third dimension. :

- ``NGC0000_gandalf-weights_BIN.fits``:

   - **Columns**: ``NWEIGHTS`` Normalized weights assigned to each template during the fit
   - **Rows**: *One line per bin/spaxel.*

- ``NGC0000_gandalf-optimalTemplate_BIN.fits``:

   - **Columns**: ``OPTIMAL_TEMPLATE`` Optimal templates 
   - **Rows**: *One template per bin/spaxel.*

- ``NGC0000_gandalf-goodpix_BIN.fits``: Array of pixels that are not masked during the fit

   - **Columns**: ``GOODPIX`` Array of not masked spectral pixels
   - **Rows**: *One row only.*

- ``NGC0000_gandalf-cleaned_BIN.fits``: Emission line subtracted spectra

   - **Columns**: ``SPEC`` Emission line subtracted spectra
   - **Rows**: *One line per bin/spaxel.*

- ``NGC0000_gandalf-bestfit_BIN.fits``: The best fit to the spectrum, including continuum and emission lines

   - **Columns**: ``BESTFIT`` The best fit to the spectrum
   - **Rows**: *One fit per bin/spaxel.*



|

SFH
-----------------
- ``NGC0000_sfh.fits``:

   - **Columns**: ``BIN_ID`` Bin ID \| ``LOGAGE`` Logarithmic age \| ``METAL`` Metallicity \| ``ALPHA`` Alpha enhancement \| ``V`` ``SIGMA`` ``H3`` ``H4`` ``H5`` ``H6`` Stellar kinematics \| ``FORM_ERR_*`` Formal errors on the stellar kinematics
   - **Rows**: *One line per bin.*

- ``NGC0000_sfh-weights.fits``: The weights assigned to each template during the fit

   - **Columns**: ``WEIGHTS`` The weights
   - **Rows**: *One line per bin.*


- ``NGC0000_sfh-goodpix.fits``: Array of pixels that are not masked during the fit

   - **Columns**: ``GOODPIX`` Array of not masked spectral pixels
   - **Rows**: *One row only*

- ``NGC0000_sfh-bestfit.fits``: The best fit to the spectrum

   - **Columns**: ``BIN_ID`` Bin ID \| ``BESTFIT`` The best fit to the spectrum
   - **Rows**: *One fit per bin.*



| 

Line Strengths
-----------------
- ``NGC0000_ls.fits``, Extension 1: Line strength indices and their errors as estimated from MC-simulations

   - **Columns**: ``Fe5015`` Line strength index, names as defined in "ls_bands.conf" \| ``ERR_*`` Errors on the line strength indices as estimated from MC-simulations
   - **Rows**: *One line per bin*

- ``NGC0000_ls.fits``, Extension 2: Single stellar population equivalent population properties as estimated from the line strength indices

   - **Columns**: ``AGE`` Age \| ``METAL`` Metallicity \| ``ALPHA`` Alpha enhancement \| ``lnP`` Chi2 \| ``Flag`` 0 if the solution is close to the boundary of the parameter space; 1 otherwise
   - **Rows**: *One line per bin*
   - **Third dimension**: ``AGE`` ``METAL`` ``ALPHA`` have 101 entries per bin, stating the percentiles from 0 to 100 in steps of 1. 



|

