.. _tutorial:

Tutorial
==================



How to get started
******************
This pipeline is a very extensive tool, providing various techniques which can be called in different flavours.  The
next sections provide a detailed explanation on the :ref:`installation`, :ref:`configuration`, :ref:`running` and
:ref:`output` of the code. However, this might be a bit overwhelming to get started. Thus I provide a tutorial below.

Step 1 
"""""""""""""""""""
Download the tutorial version of the pipeline from the :ref:`downloadIntro` section. This tarball does not only contain
the pipeline itself, but the entire necessary directory structure, an example cube, and working examples of the
configuration files. 

Step 2
"""""""""""""""""""
Extract the downloaded tarball by running::

    tar -xvzf tardisTutorial.tar.gz

Step 3
"""""""""""""""""""
Install and activate the conda environment ``requirementsPython3.yml`` of the pipeline::

    cd tardisTutorial/pipeline
    conda env create -f requirementsPython3.yml
    conda activate TardisPipeline

For further information on the use and management of conda environments, please see the 
`Conda Documentation <https://conda.io>`_.

Step 4 
"""""""""""""""""""
That's it! Now you are already ready to run the pipeline yourself. To this end, just start the pipeline by typing::

    ./MainPipeline.py

The pipeline will start by printing the settings from the Config-file, and immediately continue with the preparation of
the data (reading the cube, generating the Voronoi bins, log-rebinning the spectra). Subsequently, the pipeline starts
to run pPXF.  It informs you about every step that is conducted by printing a message/progress bar in the console, as
well as saving this information in a ``LOGFILE``. 

The output will be saved in ``../data/NGC0000/NGC0000_Example/``. The most important output files are:

* ``USED_PARAMS.fits``: Saves the configuration on which the run was conducted. *Never delete this file, as it is necessary to restart the pipeline!* 
* ``LOGFILE``: The logfile
* ``NGC0000_table.fits``: A table containing all information to reconstruct the used Voronoi binning
* ``NGC0000_VorSpectra.fits``: A table containing the Voronoi-binned spectra
* ``NGC0000_ppxf.fits``: A table containing the stellar kinematics

For a full explanation of all produced output files see :ref:`output`. 



|

Next Steps
*********************

After your first successful run of the pipeline, you will certainly want to move forward to explore the full capability
of this tool. Below, I provide a few ideas on how to move ahead. 


Look at the output
""""""""""""""""""""
Have a closer look at the output files, get a feeling for the information that is there and how it is saved.  Also look
at the plots, and try out the *Mapviewer* (To be fair, the plots and the *Mapviewer* are way more impressive once you
have run the pipeline on a full cube instead of the tiny example cube). 


Edit the Config-file
""""""""""""""""""""
In the next step, you should have a closer look at the Config-file. Try to turn on GANDALF and the regularised pPXF
(SFH) and restart the pipeline as above in Step 4 - the pipeline automatically detects that pPXF results are already
available and only conducts the other analysis. 

In order to start a new analysis run, just add another line in the Config-file with a new ``RUN_NAME``. Of course the
output directory will automatically change accordingly. 

You should also start to play around with the other parameters in the Config-file. Please note that any changes of the
parameters (unless those denoted *Main Switches*) will require that you to start a new analysis-run with a new
``RUN_NAME``. 

Use your own cube
""""""""""""""""""
Once you feel comfortable with the usage of the pipeline, try to run your own cube. Create a new directory for the
galaxy in the ``data`` directory (with the name of your galaxy), place the cube therein (with the plain name of your
galaxy as filename) and define a new analysis run in the Config-file. 


Read the other sections 
"""""""""""""""""""""""
Eventually, you should read through the next sections (:ref:`installation`, :ref:`configuration`, :ref:`running` and
:ref:`output`) to get a proper overview of how to configure this tool and become aware of all of its capabilities.
**There is way more to explore!!** 
    
|

