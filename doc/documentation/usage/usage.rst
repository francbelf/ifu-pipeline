.. _usage: 

***************
Usage
***************

In this section I provide an introduction into the usage of the pipeline. First, I provide a :ref:`tutorial` which
explains the basic steps to get started.  The following subsections :ref:`installation`, :ref:`configuration`,
:ref:`running` and :ref:`output` provide a more throughout overview of the features and capabilities of this extensive
code. It is highly recommended to read through these sections. 

.. toctree::
   :maxdepth: 1
   
   tutorial
   installation
   configuration
   running
   output


