.. _configuration:

Configuration
===================

Introduction
-------------------

The configuration of the pipeline mainly consists of the four following files:

   * ``Config``: This file provides the main configuration of the pipeline. It defines which modules to execute, and
     specifies the main parameters for the analysis. 
   * ``LSF-Config_[Instrument]``: This file defines the LSF of the used instrument. 
   * ``emission_lines.setup``: This file specifies various emission lines. These lines will be masked during the run of
     pPXF and fitted with Gandalf. 
   * ``ls_bands.conf``: This file states the bands of the line strength indices measured by the LS module. It further
     defines which indices to consider for the conversion of indices to stellar population properties. 

The files ``emission_lines.setup`` and ``ls_bands.conf`` can be different for each run of the pipeline. These files must
thus be located in the respective output directories. If these files are not available in the output directory at
start-up of the pipeline, default versions of these files will be copied from the ``configurationTemplates/`` directory 
automatically. 

A detailed description of each configuration file is provided below. 



|

The Configuration Files
-------------------------

Config
"""""""""""""""""""
The ``Config`` file provides the main configuration for the pipeline. It defines which modules to execute, and specifies
the main parameters for the analysis. A throughout description of all Config-file switches is provided below. 

| File location: ``pipeline/Config``
| Download file: :download:`Config <../../_static/Config.txt>`

**Main switches**

- ``DEBUG``:          Switch to activate debug mode (0 No / 1 Yes): Pipeline runs on one, central line of pixels. Bear in mind to clean the output directory after running in DEBUG mode!
- ``GANDALF``:        Run GANDALF to extract emission line kinematics (0 No / 1 Yes [Voronoi binned spectra] / 2 Yes [All spectra] )
- ``SFH``:            Run regularized PPXF to extract stellar population properties (0 No / 1 Yes)
- ``LINE_STRENGTH``:  Extract line strength indices and convert them to SSP-equivalent population properties (0 No / 1 Yes)
- ``PARALLEL``:       Switch to activate multiprocessing (0 No / 1 Yes)
- ``NCPU``:           Number of cores to use for multiprocessing



**General settings**

- ``RUN_NAME``:       Name of the run in the form [Galaxyname]_[RunName], e.g. NGC0000_Example. This also defines the name of the output directory. *Note*: The name must contain a underscore between [Galaxyname] and [RunName], but must not contain any other underscores. 
- ``IFU``:            Identifier of the instrument: Filename of the corresponding read-in routine in ``readData/`` without the extension .py
- ``LMIN``:           Minimum wavelength (in restframe) to be considered [in Angst.]
- ``LMAX``:           Maximum wavelength (in restframe) to be considered [in Angst.]
- ``ORIGIN``:         Origin of the coordinate system in pixel coordinates: x,y (*Note*: No spaces allowed!). If this is not set to the centre of the galaxy, the obtained values for LAMBDA_R will be incorrect. 
- ``REDSHIFT``:       An initial guess of the systemic velocity of the system [in km/s]
- ``SIGMA``:          An initial guess of the velocity dispersion of the system [in km/s]
- ``TARGET_SNR``:     Target signal-to-noise ratio for the Voronoi binning
- ``MIN_SNR``:        Minimum signal-to-noise ratio per spaxel to be considered for the Voronoi binning
- ``SSP_LIB``:        Subdirectory of ``spectralTemplates/`` containing the library of spectral templates
- ``FWHM_TEMP``:      Spectral resolution of the templates
- ``NORM_TEMP``:      Normalise the spectral template library to obtain light- or mass-weighted results (``LIGHT`` / ``MASS``)



**PPXF settings**

- ``MOM``:            Number of kinematic moments to be extracted
- ``ADEG``:           Degree of the additive Legendre polynomial. Set ``ADEG`` = -1 to not include any additive polynomials
- ``MDEG``:           Degree of the multiplicative Legendre polynomial. Set ``MDEG`` = 0 to not include any multiplicative polynomials
- ``MC_PPXF``:        Number of Monte-Carlo simulations to extract errors on the stellar kinematics. Formal errors are saved in any case. 



**GANDALF settings**

- ``FOR_ERRORS``:     Derive errors on the emission line analysis (0 No / 1 Yes). *Note*: Due to limitations in Gandalf, this is only possible when using Python 2.7
- ``REDDENING``:      Include the effect of reddening by dust in the GANDALF fit. Put in the form ``0.1,0.1``. No spaces allowed.
- ``EBmV``:           Deredden the spectra for the galactic extinction in the direction of the target. Use e.g. EBmV = A_v / 3.2



**PPXF_SFH settings**

- ``REGUL_ERR``:      Regularization error for the regularized run of pPXF. *Note*: Regularization = 1 / REGUL_ERR
- ``FIXED``:          Fix stellar kinematics to the results obtained with the unregularized run of pPXF (0 No / 1 Yes)



**LINE_STRENGTH settings**

- ``CONV_COR``:       Spectral resolution in Angstroem at which the indices are measured
- ``MC_LS``:          Number of Monte-Carlo simulations in order to obtain errors on the line strength indices. *Note*: This must be turned on. 
- ``NWALKER``:        Number of walkers for the MCMC algorithm (used for the conversion of indices to population properties)
- ``NCHAIN``:         Number of iterations in the MCMC algorithm (used for the conversion of indices to population properties)



**NOTE**

- Lines starting with the symbol ``#`` will be ignored. 
- Do not comment the header line of the table!



|

LSF-Config_[Instrument]
""""""""""""""""""""""""

| File location: ``pipeline/readData/LSF-Config_[Instrument]``
| Download file: :download:`LSF-Config_[Instrument] <../../_static/LSF-Config_MUSE-WFM.txt>`

The definition of the LSF, ``LSF-Config_[Instrument]``, is specific to the used instrument. With setting the ``IFU``
switch in the main Config-file to, for instance, "MUSE" the respective read-in routine ``readData/MUSE.py`` and
LSF-configuration file ``LSF-Config_MUSE`` will be used for all runs with this instrument. 
If it is necessary to use different LSF's for different runs with the same instrument you either 

   * have to edit the ``LSF-Config_[Instrument]`` after each run    
   * or define different ``[Instrument]`` names for the different LSF's

The file has two columns: The first column contains the wavelength in Angstroem while the second column specifies the
FWHM of the instrument at the given wavelength. The used LSF is saved in Extension 1 of the ``USED_PARAMS.fits`` output
file. 

The pipeline creates a linear interpolation function using ``scipy.interpolate.interp1d`` between wavelength and FWHM,
so that the FWHM can be reconstructed at any given wavelength. Later, the FWHM array is reconstructed for the given
wavelength array and converted to km/s. It is then applied to the stellar templates via ``ppxf_util.gaussian_filter1d``
and passed to GANDALF with the ``int_disp`` keyword. Please note that this setup allows to account for both a wavelength
dependent and independent LSF.



|

emission_lines.setup
""""""""""""""""""""""""

| File location: Output directory, e.g. ``data/NGC0000/NGC0000_Example/emission_lines.setup``
| Download file: :download:`emission_lines.setup <../../_static/emission_lines.setup.txt>`

The ``emission_lines.setup`` file is used to define relevant emission lines.  These lines will be masked during the run
of pPXF and fitted by GANDALF. It further allows to specify sky lines or mask spectral regions affected by adaptive
optics lasers. 

The file is structured as follows:

- ``i_line``: Index of the line
- ``name``: Defines the name of the emission line
- ``lambda``: Defines the wavelength of the emission line
- ``action``: Defines what should be done with the emission line: Set "m" to mask the emission line during the run of pPXF, but
  fit it during the run of Gandalf. Set "i" to ignore the line. This is equivalent to removing the line from the configuration
  file. Set "f" to fit the lines with Gandalf while not masking the lines during the run of pPXF.
- ``l-kind``: Define whether the line is a singlet (l) or part of a doublet (d25). Latter number specifies the index of
  the other doublet line. 
- ``A_i``: Relative amplitudes of doublet lines. Set to 1.0 for singlet lines. 
- ``V_g``: Initial guess on the velocity. *Note*: This should be set to 0, as the pipeline uses the velocity from the
  stellar kinematics analysis as initial guess on the gaseous kinematics by default. 
- ``sig_g``: Initial guess on the velocity dispersion. 
- ``fit-kind``: Kind of the fit to be performed: Set "f" to fit the line freely. Set "t17" to tie the line to the fit of the
  line with ``i_line`` 17. 



|

ls_bands.conf
""""""""""""""""""""""""
 
| File location: Output directory, e.g. ``data/NGC0000/NGC0000_Example/ls_bands.conf``
| Download file: :download:`ls_bands.conf <../../_static/ls_bands.conf>`

The file ``ls_bands.conf`` defines the wavelength bands of the line strength indices to be measured. More specifically,
column ``b3`` and ``b4`` state the main band while ``b1`` and ``b2``, as well as ``b5`` and ``b6`` determine the
corresponding side bands. Column ``b7`` states whether the index is an atomic (1) or a molecular (2) index. Further the
names of the indices are defined. These names will also represent the column names in the output table
``NGC0000_ls.fits`` extension 1. 

The column ``spp`` specifies which indices will be considered in the conversion of line strength indices to
SSP-equivalent population properties (1 used / 0 ignored). 



|

Spectral Templates 
""""""""""""""""""""""""

The spectral template library is selected with the ``SSP_LIB`` switch in the main Config-file. This keyword sets the
name of the subdirectory within ``spectralTemplates/`` which contains the library. For instance, if ``SSP_LIB`` is set
to "miles/" the files ``spectralTemplates/miles/*.fits`` are used as templates. 

For the conversion of line strength indices to SSP-equivalent population properties, it is necessary to provide a
model-file which states the expected line strength indices for various ages, metallicities and alpha enhancements. This
model-file is also selected via the ``SSP_LIB`` switch. For instance, if ``SSP_LIB`` is set to "miles/" the file
``spectralTemplates/miles.fits`` will be used as model-file.   



|

