.. _introduction:

Introduction
=================

Purpose and Functionality
""""""""""""""""""""""""""
This pipeline is designed to be a convenient all-in-one framework for the analysis of IFU data from any instrument. This
includes all necessary steps, from the read-in of the reduced IFU-cube, over the preparation of the data, the actual
data analysis, to the automatic generation of publication-quality plots. We further increase convenience by providing a
dedicated visualization software with a graphical user interface. 

In particular, the pipeline conducts the following major steps: 

Data Preparation
  * Read-in of the IFU-cube
  * Log-rebinning of the spectra
  * Voronoi-binning of the data
  * Preparation of the stellar template library

Main Analysis
  * Extraction of stellar kinematics via unregularized pPXF (denoted PPXF module) 
  * Extraction of stellar population properties via regularized pPXF (denoted SFH module) 
  * Extraction of emission line kinematics via GandALF (denoted GANDALF module) 
  * Extraction of line strength indices and single stellar population equivalent population properties (denoted LS
    module)

Final Steps
  * Simplify the inspection of the data products by providing the dedicated visualization software *Mapviewer*
  * Automatic generation of publication-quality plots during runtime



|

Philosophy
""""""""""""""""""

The aim of this project is to provide a sophisticated IFU analysis pipeline that has the following properties: 

* **Convenient** It is convenient by providing an all-in-one framework from the read-in of the data, over the analysis, to the automatic production of publication-quality plots
* **General**: It works well with different instruments in the context of different collaborations
* **Modular**: It allows to turn individual analysis modules on/off or replace them without affecting the integrity of the analysis procedure
* **Parallel**: It is equipped with a stable parallelisation supporting the use of large cluster systems and thus the rapid processing of large amounts of data
* **Extensive**: It is extensive enough to cope with a variety of analysis procedures (amongst others pPXF and GandALF) and their many different flavours 
* **Flexible**: It is flexible enough to work on bins, spaxels as well as manually defined apertures
* **Intelligent**: It automatically detects already available output and continues the analysis from there. It further allows to reuse of results (e.g. the Voronoi binning) between similar runs
* **Visualization**: It provides the dedicated visualization software *Mapviewer* with an advanced and fully interactive GUI, as well as automatically generated publication-quality plots
* **Clean Code**: The code is clean and well-structured, making specific modifications by users easy
* **Expandable**: The code is designed is such a way to simplify the implementation of add-ons in the analysis framework (e.g. modules for Steckmap or pyParadise)
* **Ongoing Development**: It is used by various collaborations, actively maintained, and further analysis modules are in development

I emphasize again that thanks to the very modular design of the pipeline, it is easy to modify, replace and expand
this code to meet the specific requirements of your project. In particular, it is easily possible to, for instance,
replace the GandALF module by pyPARADISE, in order to conduct the emission line analysis. Similarly, one can add
specific modules, or simply replace a few functions, to e.g. facilitate an alternative binning scheme or emission line
subtraction technique. 

*Please don't hesitate to get in touch with me if you have any questions reagarding the applicability of this code to
your project.*



|

Screenshots and Example Plots
""""""""""""""""""""""""""""""""
THESE FIGURES WILL BE REPLACED WITH FIGURES OF THE FULL RUN THAT WILL BE PRODUCED FOR THE PAPER!!!

.. figure:: ../../_static/mapviewerScreenshot.png
   :figwidth: 100 %

   Screenshot of the visualization software *Mapviewer*. Shown is the stellar velocity map of NGC???? as well as the
   underlying spectra, fits and residuals. Latter can be plotted in the right-hand column of the software by simply
   performing a mouse-click on a particular position in the map. 

.. image:: ../../_static/NGC0000_ppxf.pdf
   :width: 49 %
.. image:: ../../_static/NGC0000_lambdaR.pdf
   :width: 49 %

*Example plots of the galaxy NGC???? as produced by the pipeline. Shown are maps of the stellar kinematics (left plot)
and the lambda_r parameter (right plot). The plotted quantities are displayed in the upper left of each panel while the
minimum and maximum displayed values are stated in the lower right.* 



|

Projects
""""""""""""""""""
This code has already been used in the data analysis of the `TIMER <https://www.muse-timer.org>`_ and
`F3D <http://www.na.astro.it/Fornax3D/Fornax3D/Welcome.html>`_ collaborations. 

.. image:: ../../_static/timer.png
   :width: 20 %
   :target: https://www.muse-timer.org


|

