# Package dependencies of TardisPipeline
numpy==1.15
scipy==1.1.0
matplotlib==2.1.0
astropy==2.0
ppxf==6.7.14
vorbin==3.1.3
extinction==0.4.0
