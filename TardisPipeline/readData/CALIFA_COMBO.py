from astropy.io import fits
from astropy import table
import numpy as np
from scipy.interpolate import interp1d
import os
import logging
import extinction

from TardisPipeline.utilities import util as pipeline
from  TardisPipeline.utilities  import util_prepare

cvel = 299792.458


# ======================================
# Routine to set DEBUG mode
# ======================================
def set_debug(cube, xext, yext):
    cube['x'] = cube['x'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['y'] = cube['y'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['snr'] = cube['snr'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['signal'] = cube['signal'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['noise'] = cube['noise'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]

    cube['spec'] = cube['spec'][:, int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['error'] = cube['error'][:, int(yext / 2) * xext:(int(yext / 2) + 1) * xext]

    return (cube)

def sn_func(index, signal=None, noise=None):
    """
    Default function to calculate the S/N of a bin with spaxels "index".

    The Voronoi binning algorithm does not require this function to have a
    specific form and this default one can be changed by the user if needed
    by passing a different function as

        ... = voronoi_2d_binning(..., sn_func=sn_func)

    The S/N returned by sn_func() does not need to be an analytic
    function of S and N.

    There is also no need for sn_func() to return the actual S/N.
    Instead sn_func() could return any quantity the user needs to equalize.

    For example sn_func() could be a procedure which uses ppxf to measure
    the velocity dispersion from the coadded spectrum of spaxels "index"
    and returns the relative error in the dispersion.

    Of course an analytic approximation of S/N, like the one below,
    speeds up the calculation.

    :param index: integer vector of length N containing the indices of
        the spaxels for which the combined S/N has to be returned.
        The indices refer to elements of the vectors signal and noise.
    :param signal: vector of length M>N with the signal of all spaxels.
    :param noise: vector of length M>N with the noise of all spaxels.
    :return: scalar S/N or another quantity that needs to be equalized.
    """

    sn = np.sum(signal[index])/np.sqrt(np.sum(noise[index]**2))

    # The following commented line illustrates, as an example, how one
    # would include the effect of spatial covariance using the empirical
    # Eq.(3) from Sanchez 2016 (CALIFA DR3)
    # Note however that the formula is not accurate for large bins.

    sn /= 1 + 1.08*np.log10(index.size)

    return  sn

def get_CALIFA_LSF_FWHM(x, version="udf10"):
    """Return the polynomial defining the FWHM resolution
    for the MUSE data, 
    x: wavelength in angstrom, 
    lsf: FWHM LSF in angstrom.
    """
    lsf = np.zeros(len(x))+6.
    return (lsf)


def reshape_extintion_curve(extinction_curve, cube):
    extra_dims = cube.ndim - extinction_curve.ndim
    new_shape = extinction_curve.shape + (1,) * extra_dims
    reshaped_extinction_curve = extinction_curve.reshape(new_shape)
    return reshaped_extinction_curve


def read_sky_mask_pixels(configs, shape):
    mask = np.zeros(shape)

    rootname = str(configs['RUN_NAME']).split('_')[0]
    datafile = str(configs['DATADIR']) + '/data/' + rootname + '/' + rootname + '_stars.txt'

    try:
        stars = ascii.read(datafile)
        xs, ys, rs = np.array(stars['col1']), np.array(stars['col2']), np.array(stars['col3'])
        nstars = len(xs)

        grid_x, grid_y = np.meshgrid(np.array(range(shape[1])),
                                     np.array(range(shape[0])))
        for ii in range(nstars):
            wmask = ((grid_x - xs[ii]) ** 2 + (grid_y - ys[ii]) ** 2) < rs[ii] ** 2
            mask[wmask] = 1
    except:
        pass

    return (mask)

def mask_around_lines():
    """Returns the half-width of the mask around emission lines for the stellar kinematics fitting in km/s"""
    mask_halfwidth=800
    return(mask_halfwidth)


# ======================================
# Routine to load MUSE-cubes
# ======================================
def read_cube(configs, tasks):
    
    pipeline.prettyOutput_Running("Reading the CALIFA-COMBO cube")
    logging.info("Reading the CALIFA-COMBO cube")

    # --> Reading the cube
    hdu = fits.open(configs['DATAFILE'])
    hdr = hdu[0].header

    # --> Read wavelength
    # and getting the wavelength info
    # if hdr['CTYPE3'] == 'WAVELENGTH':
    wave_type = 'lin'
    wave = hdr['CRVAL3'] + np.arange(hdr['NAXIS3']) * hdr['CD3_3']
    # velscale the approximate values obtained by (hdr['CD3_3'])/7400*cvel
    # which leads to no undersampling in the MILES wav. range < 7400 AA
    velscale = 80.
   
    # --> read data, error and LSF
    data = hdu[0].data
    stat = hdu[1].data
    s = np.shape(data)
    # Take care of LSF
    LSF = get_CALIFA_LSF_FWHM(wave)
    # Linear interpolation of wavelength and FWHM, so that it can easily be used for both stellar and emission templates
    LSF_InterpolationFunction = interp1d(wave, LSF, 'linear', fill_value='extrapolate')

    # --> Correct spectra for galactic extinction
    if configs['EBmV'] is not None:
        Rv = 3.1
        Av = Rv * configs['EBmV']
        ones = np.ones_like(wave)
        extinction_curve = extinction.apply(extinction.ccm89(wave, Av, Rv), ones)
        reshaped_extinction_curve = reshape_extintion_curve(extinction_curve, data)
        data = data / reshaped_extinction_curve
        stat = stat / reshaped_extinction_curve

    # --> Getting the spatial coordinates
    # generating header wcs
    wcs = {'CD1_1': [hdr['CD1_1']], 'CD2_2': [hdr['CD2_2']], 'NAXIS1': [hdr['NAXIS1']], 'NAXIS2': [hdr['NAXIS2']],
        'CRVAL1': [hdr['CRVAL1']], 'CRVAL2': [hdr['CRVAL2']], 'CUNIT1': [hdr['CUNIT1']], 'CUNIT2': [hdr['CUNIT2']],
        'CRPIX1': [hdr['CRPIX1']], 'CRPIX2': [hdr['CRPIX2']], 'FUNIT': [10 ** (-15)]}
    # generating geometry
    xaxis = np.arange(s[2]) * abs(hdr['CD1_1']) * 3600.0
    yaxis = np.arange(s[1]) * abs(hdr['CD2_2']) * 3600.0
    x, y = np.meshgrid(xaxis, yaxis)
    x = np.reshape(x, [s[1] * s[2]])
    y = np.reshape(y, [s[1] * s[2]])
    pixelsize = abs(hdr['CD1_1']) * 3600.0

    # mask stars
    mask_sky = read_sky_mask_pixels(configs, (s[1], s[2]))
    ww = (mask_sky == 1)
    data[:, ww] = np.nan

    # --> reshape flux and error array into spaxel table
    spec = np.reshape(data, [s[0], s[1] * s[2]])
    espec = np.reshape(stat, [s[0], s[1] * s[2]])
    logging.info("Spaxel table generated")

    # --> quality control of the spectra
    # no negative noise values
    # also CALIFA has strange data artefacts with noise =10**(-16) which breaks ppxf 
    # espec > 999. should remove all the masked pixels 
    w_neg_noise = (espec <= 0) | (np.isfinite(espec) == 0) | (np.isfinite(spec) == 0) | (espec > 999.) | (espec < 0.001) 
    espec[w_neg_noise] = 9999.
    spec[w_neg_noise]=0.
    # Applying some wavelength cuts in the REST FRAME!
    # idx = (wave >= configs['LMIN'] * (1.0 + configs['REDSHIFT'] / cvel)) & \
    #     (wave <= configs['LMAX'] * (1.0 + configs['REDSHIFT'] / cvel))
    # spec = spec[idx, :]
    # espec = espec[idx, :]
    # wave = wave[idx]

    #fix width of mask around lines
    configs['MASK_WIDTH']=mask_around_lines()

    # Removing obviously defective pixels: Remove spaxel with any nan or negative values
    ## TO FIX!!!!
    # this is written this way to avoid that changing  the wavelength range changes the number of good pixels
    # which causes all sort of headaches when you want to re-run the same galaxy with differenty wavelength range
    idx2 = (wave >= 4850. * (1.0 + configs['REDSHIFT'] / cvel)) & \
        (wave <= 7000. * (1.0 + configs['REDSHIFT'] / cvel))
    
    # one where it's masked and zero if not
    mask = np.where(espec[idx2, :]==9999, 0, 1)
    # fraction of good pixels in the spectrum
    tot_good_pix = np.sum(mask, axis=0)/mask.shape[0]
    # if the fraction is higher than 80% then all good
    idx_08_good = tot_good_pix>0.8
    spec = spec[:, idx_08_good]
    espec = espec[:, idx_08_good]
    x = x[idx_08_good]
    y = y[idx_08_good]

    # --> Computing the SNR 
    # checking is SNR WAV range makes sense
    if (np.min(wave) > configs['SN_WAV'][0]) or (np.max(wave) < configs['SN_WAV'][1]):
        message = "SN_WAV is not fully included within the wavelength range of the data. Galaxy will be skipped"
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        tasks['SKIP_GALAXY'] = True

    spec2 = np.ma.MaskedArray(spec, mask = (espec==9999.))
    espec2 = np.ma.MaskedArray(espec, mask = (espec==9999.))
    in_wave = (wave > configs['SN_WAV'][0]) & (wave < configs['SN_WAV'][1]) 
    signal = np.mean(spec2[in_wave, :], axis=0)
    noise = np.mean(espec2[in_wave, :], axis=0)
    snr = signal / noise
    # negative S/N givesd problems later on
    snr[snr<=0]=0.01
    snr_sfh = snr

    # --> Saving the cube geometry
    # Storing eveything into a structure
    cube = {'x': x, 'y': y, 'wave': wave, 'spec': spec, 'error': espec, 'snr': snr, 'snr_sfh': snr_sfh, \
            'signal': signal, 'noise': noise, 'velscale': velscale, 'pixelsize': pixelsize,
            'LSF_InterpolationFunction': LSF_InterpolationFunction, 'wave_type': wave_type}
    #saving wcs and geometry
    util_prepare.save_WCS(configs, wcs, cube)

    # --> Constrain cube to one central row if switch DEBUG is set
    if configs['DEBUG'] == True: 
        xy_extent = np.array([data.shape[2], data.shape[1]])
        cube = set_debug(cube, xy_extent[0], xy_extent[1])

    # write to screen and log
    pipeline.prettyOutput_Done("Reading the CALIFA-COMBO cube")
    print("             Read " + str(len(cube['x'])) + " spectra!")
    logging.info("Read " + str(len(cube['x'])) + " spectra!")

    return (cube, wcs, configs)
