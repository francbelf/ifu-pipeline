from astropy.io import fits
from astropy import table
import numpy as np
from scipy.interpolate import interp1d
import os
import logging
import extinction
from scipy.signal import savgol_filter
from TardisPipeline.utilities import util as pipeline
from  TardisPipeline.utilities  import util_prepare
try:
    from TardisPipeline.sitePackages.voronoi_2d_binning import _sn_func
except:
    from vorbin.voronoi_2d_binning import _sn_func

cvel = 299792.458


# ======================================
# Routine to set DEBUG mode
# ======================================
def set_debug(cube, xext, yext):
    cube['x'] = cube['x'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['y'] = cube['y'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['snr'] = cube['snr'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['signal'] = cube['signal'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['noise'] = cube['noise'][int(yext / 2) * xext:(int(yext / 2) + 1) * xext]

    cube['spec'] = cube['spec'][:, int(yext / 2) * xext:(int(yext / 2) + 1) * xext]
    cube['error'] = cube['error'][:, int(yext / 2) * xext:(int(yext / 2) + 1) * xext]

    return (cube)

# #this S/N function assumes errors are independent
sn_func = _sn_func


def get_KCWI_LSF_FWHM(x):
    """Return the polynomial defining the FWHM resolution
    for the MUSE data, 
    x: wavelength in angstrom, 
    lsf: FWHM LSF in angstrom.
    """
    lsf = np.zeros(len(x))+3.3
    # 3.3A assuming R=1500 at 5000A, but this may need to be adjusted if higher res. mode is used
    return lsf

def get_MUSE_polyFWHM(x, version="udf10"):
    """Return the polynomial defining the FWHM resolution
    for the MUSE data, 
    x: wavelength in angstrom, 
    lsf: FWHM LSF in angstrom.
    """
    if version is "udf10":
    #equation 8 of Bacon et al 2017
        return 5.866 * 1e-8 * x ** 2 - 9.187 * 1e-4 * x + 6.04

    elif version is "HDFS":
        return 6.266 * 1e-8 * x ** 2 - 9.824 * 1e-4 * x + 6.286

    elif version is 'pipeline':
        fwhm_lsf_TO = np.array([2.71341653, 2.67218614, 2.63285769, 2.59554322, 2.5603268,
                                2.52726421, 2.4963874, 2.46771472, 2.44124781, 2.41698236,
                                2.39490904, 2.37501593, 2.35729457, 2.34174201, 2.32836005,
                                2.31717184, 2.30819808, 2.30149535, 2.29711644, 2.29516249,
                                2.29575013, 2.29902041, 2.30516998, 2.31442672, 2.32707595,
                                2.34346802, 2.3640224, 2.38924588, 2.4196544, 2.45618387])

        aa_fwhm_lsf_TO = np.array([4650., 4810.34483337, 4970.68966675, 5131.03450012,
                                   5291.3793335, 5451.72416687, 5612.06900024, 5772.41383362,
                                   5932.75866699, 6093.10350037, 6253.44833374, 6413.79316711,
                                   6574.13800049, 6734.48283386, 6894.82766724, 7055.17250061,
                                   7215.51733398, 7375.86216736, 7536.20700073, 7696.55183411,
                                   7856.89666748, 8017.24150085, 8177.58633423, 8337.9311676,
                                   8498.27600098, 8658.62083435, 8818.96566772, 8979.3105011,
                                   9139.65533447, 9300.00016785])
        lsf = np.interp(x, aa_fwhm_lsf_TO, fwhm_lsf_TO, left=fwhm_lsf_TO[0], right=fwhm_lsf_TO[-1])
        return (lsf)


def reshape_extintion_curve(extinction_curve, cube):
    extra_dims = cube.ndim - extinction_curve.ndim
    new_shape = extinction_curve.shape + (1,) * extra_dims
    reshaped_extinction_curve = extinction_curve.reshape(new_shape)
    return reshaped_extinction_curve


def read_sky_mask_pixels(configs, shape):
    mask = np.zeros(shape)

    rootname = str(configs['RUN_NAME']).split('_')[0]
    datafile = str(configs['DATADIR']) + '/data/' + rootname + '/' + rootname + '_stars.txt'

    try:
        stars = ascii.read(datafile)
        xs, ys, rs = np.array(stars['col1']), np.array(stars['col2']), np.array(stars['col3'])
        nstars = len(xs)

        grid_x, grid_y = np.meshgrid(np.array(range(shape[1])),
                                     np.array(range(shape[0])))
        for ii in range(nstars):
            wmask = ((grid_x - xs[ii]) ** 2 + (grid_y - ys[ii]) ** 2) < rs[ii] ** 2
            mask[wmask] = 1
    except:
        pass

    return (mask)

def mask_around_lines():
    """Returns the half-width of the mask around emission lines for the stellar kinematics fitting in km/s"""
    mask_halfwidth=400
    return(mask_halfwidth)


# ======================================
# Routine to load KCWI-cubes
# ======================================
def read_cube(configs, tasks):
    
    pipeline.prettyOutput_Running("Reading the KCWI cube")
    logging.info("Reading the KCWI cube")

    # --> Reading the cube
    hdu = fits.open(configs['DATAFILE'])
    hdr = hdu[0].header

    wave_type = 'lin'
    wave = hdr['CRVAL3'] + np.arange(hdr['NAXIS3']) * hdr['CDELT3']
    velscale = 50.

    # --> read data, error and LSF
    data = hdu[0].data
    try:
        # If variance is present in data cube
        stat = np.sqrt(hdu[1].data)
    except IndexError:
        pipeline.prettyOutput_Running("Create map of the noise based on the background statistics")
        rec0 = (wave > (5200 * (1.0 + configs['REDSHIFT'] / cvel))) & (wave < (5550 * (1.0 + configs['REDSHIFT'] / cvel)))
        rec1 = (wave[rec0] > (5300 * (1.0 + configs['REDSHIFT'] / cvel))) & (wave[rec0] < (5500 * (1.0 + configs['REDSHIFT'] / cvel)))
        rec = (wave > (5300 * (1.0 + configs['REDSHIFT'] / cvel))) & (
                    wave < (5500 * (1.0 + configs['REDSHIFT'] / cvel)))

        stat = np.nanstd(data[rec, :,:] - savgol_filter(data[rec0,:,:], 15, 3, axis=0)[rec1,:,:], axis=0)
        stat = np.ones_like(data)*stat[None, :, :]
    pipeline.prettyOutput_Running(f"Median S/N level: {np.nanmedian(data/stat)}")
    s = np.shape(data)
    # Take care of LSF
    LSF = get_KCWI_LSF_FWHM(wave, version="udf10")
    # Linear interpolation of wavelength and FWHM, so that it can easily be used for both stellar and emission templates
    LSF_InterpolationFunction = interp1d(wave, LSF, 'linear', fill_value='extrapolate')

    # --> Correct spectra for galactic extinction
    if configs['EBmV'] is not None:
        Rv = 3.1
        Av = Rv * configs['EBmV']
        ones = np.ones_like(wave)
        extinction_curve = extinction.apply(extinction.ccm89(wave, Av, Rv), ones)
        reshaped_extinction_curve = reshape_extintion_curve(extinction_curve, data)
        data = data / reshaped_extinction_curve
        stat = stat / reshaped_extinction_curve

    # --> Getting the spatial coordinates
    # generating header wcs
    wcs = {'CD1_1': [hdr['CDELT1']], 'CD2_2': [hdr['CDELT2']], 'NAXIS1': [hdr['NAXIS1']], 'NAXIS2': [hdr['NAXIS2']],
        'CRVAL1': [hdr['CRVAL1']], 'CRVAL2': [hdr['CRVAL2']], 'CUNIT1': ['deg'], 'CUNIT2': ['deg'],
        'CRPIX1': [hdr['CRPIX1']], 'CRPIX2': [hdr['CRPIX2']], 'FUNIT': [10 ** (-15)]}
    # generating geometry
    xaxis = np.arange(s[2]) * abs(hdr['CDELT1']) * 3600.0
    yaxis = np.arange(s[1]) * abs(hdr['CDELT2']) * 3600.0
    x, y = np.meshgrid(xaxis, yaxis)
    x = np.reshape(x, [s[1] * s[2]])
    y = np.reshape(y, [s[1] * s[2]])
    pixelsize = abs(hdr['CDELT1']) * 3600.0

    # mask stars
    mask_sky = read_sky_mask_pixels(configs, (s[1], s[2]))
    ww = (mask_sky == 1)
    data[:, ww] = np.nan

    # --> reshape flux and error array into spaxel table
    spec = np.reshape(data, [s[0], s[1] * s[2]])
    espec = np.reshape(stat, [s[0], s[1] * s[2]])
    logging.info("Spaxel table generated")

    # --> quality control of the spectra
    # no negative noise values
    # note that noise = 1 also catches the AO gap
    w_neg_noise = (espec <= 0) | (np.isfinite(espec) == 0) | (np.isfinite(spec) == 0)
    espec[w_neg_noise] = 9999.
    spec[w_neg_noise]=0.
    # Applying some wavelength cuts in the REST FRAME!
    idx = (wave >= configs['LMIN'] * (1.0 + configs['REDSHIFT'] / cvel)) & \
        (wave <= configs['LMAX'] * (1.0 + configs['REDSHIFT'] / cvel))
    spec = spec[idx, :]
    espec = espec[idx, :]
    wave = wave[idx]

    #fix width of mask around lines
    configs['MASK_WIDTH']=mask_around_lines()

    # Removing obviously defective pixels: Remove spaxel with any nan or negative values
    ## TO FIX!!!!
    # this is written this way to avoid that changing  the wavelength range changes the number of good pixels
    # which causes all sort of headaches when you want to re-run the same galaxy with differenty wavelength range
    idx2 = (wave >= 3650. * (1.0 + configs['REDSHIFT'] / cvel)) & \
        (wave <= 5550. * (1.0 + configs['REDSHIFT'] / cvel))
    
    # one where it's masked and zero if not
    mask = np.where(espec[idx2, :]==9999, 0, 1)
    # fraction of good pixels in the spectrum
    tot_good_pix = np.sum(mask, axis=0)/mask.shape[0]
    # if the fraction is higher than 80% then all good
    in_wave = (wave > configs['SN_WAV'][0]) & (wave < configs['SN_WAV'][1])

    idx_08_good = np.where(np.logical_and(tot_good_pix>0.8, np.median(espec[in_wave, :], axis = 0) != 9999 ))[0] 
    #Also remove spaxels which have problems in the wavelength range where SNR is calculated
    spec = spec[:, idx_08_good]
    espec = espec[:, idx_08_good]
    x = x[idx_08_good]
    y = y[idx_08_good]

    # --> Computing the SNR 
    # checking is SNR WAV range makes sense
    if (np.min(wave) > configs['SN_WAV'][0]) or (np.max(wave) < configs['SN_WAV'][1]):
        message = "SN_WAV is not fully included within the wavelength range of the data. Galaxy will be skipped"
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        tasks['SKIP_GALAXY'] = True

    spec2 = np.ma.MaskedArray(spec, mask = (espec==9999.))
    espec2 = np.ma.MaskedArray(espec, mask = (espec==9999.))

    signal = np.mean(spec2[in_wave, :], axis=0)
    noise = np.mean(espec2[in_wave, :], axis=0)
    snr = signal / noise
    # negative S/N givesd problems later on
    snr[snr<=0]=0.01
    snr_sfh = snr

    # --> Saving the cube geometry
    # Storing eveything into a structure
    cube = {'x': x, 'y': y, 'wave': wave, 'spec': spec, 'error': espec, 'snr': snr, 'snr_sfh': snr_sfh, \
            'signal': signal, 'noise': noise, 'velscale': velscale, 'pixelsize': pixelsize,
            'LSF_InterpolationFunction': LSF_InterpolationFunction, 'wave_type': wave_type}
    #saving wcs and geometry
    util_prepare.save_WCS(configs, wcs, cube)

    # --> Constrain cube to one central row if switch DEBUG is set
    if configs['DEBUG'] == True: 
        xy_extent = np.array([data.shape[2], data.shape[1]])
        cube = set_debug(cube, xy_extent[0], xy_extent[1])

    # write to screen and log
    pipeline.prettyOutput_Done("Reading the KCWI cube")
    print("             Read " + str(len(cube['x'])) + " spectra!")
    logging.info("Read " + str(len(cube['x'])) + " spectra!")

    return (cube, wcs, configs)
