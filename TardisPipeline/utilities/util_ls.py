from astropy.io import fits, ascii
import numpy as np
from multiprocessing import Queue, Process

import time
import logging
import os
import pdb

import utilities.util               as pipeline
import utilities.util_plot_ls       as util_plot_ls
import utilities.util_plot_spp      as util_plot_spp

import sitePackages.lineStrength.lsindex_spec   as lsindex
import sitePackages.lineStrength.ssppop_fitting as ssppop

try:
    # Try to use local version in sitePackages
    from sitePackages.ppxf.ppxf_util import gaussian_filter1d
except:
    # Then use system installed version instead
    from ppxf.ppxf_util import gaussian_filter1d

cvel  = 299792.458



def workerLS(inQueue, outQueue):
    for wave, spec, espec, redshift, configs, lickfile, names, index_names,\
        model_indices, params, tri, labels, outdir, nbins, i\
        in iter(inQueue.get, 'STOP'):

        indices, errors, vals, percentile = run_ls( wave, spec, espec, redshift, configs, lickfile, names, index_names,\
        model_indices, params, tri, labels, outdir, nbins, i )

        outQueue.put(( i, indices, errors, vals, percentile ))


def run_ls(wave, spec, espec, redshift, configs, lickfile, names, index_names,\
           model_indices, params, tri, labels, outdir, nbins, i): 

    pipeline.printProgress(i, nbins, barLength = 50)
    nindex = len(index_names)

    try:
        timing = time.time()
        names, indices, errors = lsindex.lsindex\
                    (wave, spec, espec, redshift[0], lickfile, sims=configs['MC_LS'], z_err=redshift[1], plot=0)
    
        # Get the indices in consideration
        data  = np.zeros(nindex)
        error = np.zeros(nindex)
        for o in range( nindex ):
            idx = np.where( names == index_names[o] )[0]
            data[o]  = indices[idx]
            error[o] = errors[idx]
    
        vals   = np.zeros(len(labels)*3+2)
        chains = np.zeros((int(configs['NWALKER']*configs['NCHAIN']/2), len(labels)))
        vals[:], chains[:,:] = ssppop.ssppop_fitting\
            (data, error, model_indices, params, tri, labels, configs['NWALKER'], configs['NCHAIN'], False, 0, i, nbins, outdir)
    
        percentiles = np.percentile( chains, np.arange(101), axis=0 )
    
        return(indices, errors, vals, percentiles)

    except:
        return( np.nan, np.nan, np.nan, np.nan )


def save_ls(names, ls_indices, ls_errors, vals, percentile, index_names, labels, nwalker, nchain, outdir, rootname):

    # Save results
    outfits = outdir+rootname+'_ls.fits'
    pipeline.prettyOutput_Running("Writing: "+outfits)
   
    # Primary HDU
    priHDU = fits.PrimaryHDU()

    # Extension 1: Table HDU with LS output data
    cols = []
    ndim  = len(names)
    for i in range(ndim):
       cols.append( fits.Column(name=names[i],        format='D', array=ls_indices[:,i] ))
       cols.append( fits.Column(name="ERR_"+names[i], format='D', array=ls_errors[:,i]  ))
    lsHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    lsHDU.name = "LS_DATA"

    # Extension 2: Table HDU with SSP-equivalent output data
    nparam  = len(labels)
    cols = []
    for i in range(nparam):
        cols.append( fits.Column(name=labels[i],           format='101D', array=percentile[:,:,i] ))
    cols.append( fits.Column(    name='lnP',               format='D',    array=vals[:,-2]        ))
    cols.append( fits.Column(    name='Flag',              format='D',    array=vals[:,-1]        ))
    sspHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols)) 
    sspHDU.name = "SSP_DATA"

    # Create HDU list and write to file
    HDUList = fits.HDUList([priHDU, lsHDU, sspHDU])
    HDUList.writeto(outfits, overwrite=True)

    pipeline.prettyOutput_Done("Writing: "+outfits)
    logging.info("Wrote: "+outfits)


def log_unbinning(lamRange, spec, oversample=1, flux=True):

    # Length of arrays
    n = len(spec)
    m = n * oversample

    # Log space
    dLam = (lamRange[1]-lamRange[0]) / (n - 1)             # Step in log-space
    lim = lamRange + np.array([-0.5, 0.5])*dLam            # Min and max wavelength in log-space
    borders = np.linspace( lim[0], lim[1], n+1 )           # OLD logLam in log-space
    
    # Wavelength domain
    logLim     = np.exp(lim)                               # Min and max wavelength in Angst.
    lamNew     = np.linspace( logLim[0], logLim[1], m+1 )  # new logLam in Angstroem
    newBorders = np.log(lamNew)                            # new logLam in log-space

    # Translate indices of arrays so that newBorders[j] corresponds to borders[k[j]]
    k = np.floor( (newBorders-lim[0]) / dLam ).astype('int')

    # Construct new spectrum
    specNew = np.zeros(m)
    for j in range(0, m-1):
        a = (newBorders[j]   - borders[k[j]])   / dLam
        b = (borders[k[j+1]] - newBorders[j+1]) / dLam
        
        specNew[j] = np.sum( spec[k[j]:k[j+1]] ) - a*spec[k[j]] - b*spec[k[j+1]]

    # Rescale flux
    if flux == True:
        specNew = specNew / ( newBorders[1:] - newBorders[:-1] ) * np.mean( newBorders[1:] - newBorders[:-1] ) * oversample

    # Shift back the wavelength arrays
    lamNew = lamNew[:-1] + 0.5 * (lamNew[1]-lamNew[0])

    return( specNew, lamNew )


def runModule_LINESTRENGTH(LINE_STRENGTH, PARALLEL, configs, velscale, LSF_InterpolationFunction, outdir, rootname):

    if LINE_STRENGTH == True:
        print("\033[0;37m"+" - - - - - Running LINE STRENGTHS - - - - -"+"\033[0;39m")
        logging.info(" - - - Running LINE STRENGTHS - - - ")

        # Read cleaned spectra
        logging.info("Reading "+outdir+rootname+"_gandalf-cleaned_BIN.fits")
        hdu_spec  = fits.open(outdir+rootname+'_gandalf-cleaned_BIN.fits')
        hdu_espec = fits.open(outdir+rootname+'_VorSpectra.fits')
        oldspec  = np.array( hdu_spec[1].data.SPEC   )
        oldespec = np.sqrt( np.array( hdu_espec[1].data.ESPEC ) )
        wave     = np.array( hdu_spec[2].data.LOGLAM )
        nbins    = oldspec.shape[0]
        npix     = oldspec.shape[1]
        lamRange = np.array([ wave[0], wave[-1] ])
        spec     = np.zeros( oldspec.shape  )
        espec    = np.zeros( oldespec.shape )

        # Rebin the cleaned spectra from log to lin
        pipeline.prettyOutput_Running("Rebinning the cleaned spectra from log to lin")
        for i in range( nbins ):
            pipeline.printProgress(i, nbins, barLength = 50)
            spec[i,:], wave = log_unbinning( lamRange, oldspec[i,:] )
        pipeline.prettyOutput_Done("Rebinning the cleaned spectra from log to lin", progressbar=True)

        # Rebin the error spectra from log to lin
        pipeline.prettyOutput_Running("Rebinning the error spectra from log to lin")
        for i in range( nbins ):
            pipeline.printProgress(i, nbins, barLength = 50)
            espec[i,:], _ = log_unbinning( lamRange, oldespec[i,:] )
        pipeline.prettyOutput_Done("Rebinning the error spectra from log to lin", progressbar=True)

        # Read PPXF results
        ppxf_data     = fits.open(outdir+rootname+'_ppxf.fits')[1].data
        redshift      = np.zeros((nbins, 2))              # Dimensionless z
        redshift[:,0] = np.array( ppxf_data.V[:] ) / cvel # Redshift
        redshift[:,1] = np.array( ppxf_data.FORM_ERR_V[:] ) / cvel # Error on redshift

        # Read ls_bands.conf 
        lickfile = outdir+'ls_bands.conf'
        tab   = ascii.read(lickfile, comment='\s*#')
        names = tab['names']

        # Convolution Correction
        FWHM_dif = np.sqrt( configs['CONV_COR']**2 - LSF_InterpolationFunction(wave)**2 )
        sigma    = (FWHM_dif / wave) * cvel / 2.355 / velscale
        for i in range(0, spec.shape[0]):
            spec[i,:] = gaussian_filter1d(spec[i,:], sigma)

        # Get indices that are considered in SSP-conversion
        idx         = np.where( tab['spp'] == 1 )[0]
        index_names = tab['names'][idx].tolist()

        # Loading model predictions
        model_indices, params, tri, labels = ssppop.load_models(configs['SSP_LIB'].rstrip('/')+".fits", index_names)

        # Arrays to store results
        ls_indices = np.zeros((nbins, len(names)));    ls_indices[:,:] = np.nan
        ls_errors  = np.zeros((nbins, len(names)));    ls_errors[:,:]  = np.nan
        vals       = np.zeros((nbins, len(labels)*3+2))
        percentile = np.zeros((nbins, 101, len(labels)))

        # Run LS Measurements
        start_time = time.time()
        if PARALLEL == True:
            pipeline.prettyOutput_Running("Running LINE_STRENGTH in parallel mode")            
            logging.info("Running LINE_STRENGTH in parallel mode")            

            # Create Queues
            inQueue  = Queue()
            outQueue = Queue()
        
            # Create worker processes
            ps = [Process(target=workerLS, args=(inQueue, outQueue))
                  for _ in range(configs['NCPU'])]
        
            # Start worker processes
            for p in ps: p.start()
        
            # Fill the queue
            for i in range(nbins):
                inQueue.put( ( wave, spec[i,:], espec[i,:], redshift[i,:], configs, lickfile, names, index_names,\
                               model_indices, params, tri, labels, outdir, nbins, i ) )
        
            # now get the results with indices
            ls_tmp = [outQueue.get() for _ in range(nbins)]
        
            # send stop signal to stop iteration
            for _ in range(configs['NCPU']): inQueue.put('STOP')

            # stop processes
            for p in ps: p.join()
        
            # Get output
            index = np.zeros(nbins)
            for i in range(0, nbins):
                index[i]          = ls_tmp[i][0]
                ls_indices[i,:]   = ls_tmp[i][1]
                ls_errors[i,:]    = ls_tmp[i][2]
                vals[i,:]         = ls_tmp[i][3]
                percentile[i,:,:] = ls_tmp[i][4]

            # Sort output
            argidx = np.argsort( index )
            ls_indices = ls_indices[argidx,:]
            ls_errors  = ls_errors[argidx,:]
            vals       = vals[argidx,:]
            percentile = percentile[argidx,:,:]

            pipeline.prettyOutput_Done("Running LINE_STRENGTH in parallel mode", progressbar=True)

        if PARALLEL == False:
            pipeline.prettyOutput_Running("Running LINE_STRENGTH in serial mode")            
            logging.info("Running LINE_STRENGTH in serial mode")            

            for i in range(nbins):
                ls_indices[i,:], ls_errors[i,:], vals[i,:], percentile[i,:,:] = run_ls\
                        (wave, spec[i,:], espec[i,:], redshift[i,:], configs, lickfile, names, index_names,\
                        model_indices, params, tri, labels, outdir, nbins, i)

            pipeline.prettyOutput_Done("Running LINE_STRENGTH in serial mode", progressbar=True)

        print("             Running LINE_STRENGTH on %s spectra took %.2fs" % (nbins, time.time() - start_time))
        print("")
        logging.info("Running LINE_STRENGTH on %s spectra took %.2fs using %i cores" % (nbins, time.time() - start_time, configs['NCPU']))

        # Save Results
        save_ls(names, ls_indices, ls_errors, vals, percentile, index_names, labels, configs['NWALKER'], configs['NCHAIN'], outdir, rootname)

        # Do Plots
        try:
            pipeline.prettyOutput_Running("Producing line strength maps")
            logging.info("Producing line strength maps")
            util_plot_ls.plot_maps(outdir.split('/')[-2])
            util_plot_spp.plot_maps("LS", outdir.split('/')[-2])
            pipeline.prettyOutput_Done("Producing line strength maps")
        except:
            pipeline.prettyOutput_Failed("Producing line strength maps")
            logging.warning("Failed to produce line strength maps. Analysis continues!")
            pass

        print("\033[0;37m"+" - - - - - LINE STRENGTHS done - - - - -"+"\033[0;39m")
        print("")
        logging.info(" - - - LINE STRENGTHS Done - - - \n")
        
    elif LINE_STRENGTH == False:
        print("")
        print(pipeline.prettyOutput_WarningPrefix()+"Skipping LINE STRENGTHS!")
        print("")
        logging.warning("Skipping LINE STRENGTHS\n")
