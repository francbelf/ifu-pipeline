import numpy    as np
from astropy.io import fits
from multiprocessing import Queue, Process
from   scipy.interpolate import interp1d
from astropy import constants
from astropy import table 
import copy
import time
import logging

from  TardisPipeline.utilities import util as pipeline
from  TardisPipeline.utilities  import util_prepare
from  TardisPipeline.utilities  import util_templates

import pdb


try:
    # Try to use local version in sitePackages
    from sitePackages.ppxf.ppxf import ppxf
    from sitePackages.ppxf.ppxf.ppxf_util import air_to_vac
except: 
    # Then use system installed version instead
    from ppxf.ppxf import ppxf
    from ppxf.ppxf_util import air_to_vac

cvel = constants.c.to('km/s').value

def workerPPXF(inQueue, outQueue):
    for templates, bin_data, noise, velscale, mask_ppxf, \
        adeg, mdeg, offset, velscale_ratio, \
        tpl_comp, moments, start, bounds, tied, gas_comp,gas_names,\
        nbins, i \
        in iter(inQueue.get, 'STOP'):

        sol, kin_err, chi2, gas_flux, gas_flux_err, gas_names, bestfit, gas_bestfit, stkin, stkin_err = \
            run_ppxf(templates, bin_data, noise, velscale, mask_ppxf, \
            adeg, mdeg, offset, velscale_ratio, \
            tpl_comp, moments, start, bounds, tied, gas_comp,gas_names,\
            nbins, i)

        outQueue.put(( i, sol, kin_err, chi2, gas_flux, gas_flux_err, gas_names, bestfit, gas_bestfit,
        stkin, stkin_err ))


def run_ppxf( templates, bin_data, noise, velscale, mask_ppxf, \
            adeg, mdeg, offset, velscale_ratio, \
            tpl_comp, moments, start, bounds, tied, gas_comp,gas_names,\
            nbins, i):

    pipeline.printProgress( i, nbins, barLength=50 )

    try:
        # Call PPXF
        #pdb.set_trace()
        pp = ppxf(templates, bin_data, noise, velscale, mask=mask_ppxf, 
            degree=adeg, mdegree=mdeg, vsyst=offset,  velscale_ratio=velscale_ratio,
            component=tpl_comp, moments=moments, start=start, bounds =bounds,
            tied=tied, gas_component=gas_comp, gas_names= gas_names,
            plot=False, quiet=True)

        # note here I am only passing out of the function the gas kinematics (pp.sol[1:])
        # to also save the stellar kinematics one would need to add pp.sol[0] and same for pp.error
        return(np.array(pp.sol[1:]) , np.array(pp.error[1:]), pp.chi2, pp.gas_flux, pp.gas_flux_error, 
            pp.gas_names, pp.bestfit, pp.gas_bestfit, pp.sol[0], pp.error[0])

    except:
        #raise Exception
        print('bad')
        return( np.nan, np.nan, np.nan, np.nan, np.nan ,np.nan ,np.nan ,np.nan ,np.nan ,np.nan )


def save_ppxf_emlines(rootname, outdir, linesfitted, 
        gas_flux_in_units, gas_err_flux_in_units,vel_final, vel_err_final,
        sigma_final_measured, sigma_err_final, chi2, templates_sigma, bestfit, gas_bestfit, stkin, ubins, extra):
   
        # ========================
        # SAVE RESULTS
        outfits_ppxf = outdir+rootname+'_emlines.fits'
        pipeline.prettyOutput_Running("Writing: "+outfits_ppxf.split('/')[-1])
    
        # Primary HDU
        priHDU = fits.PrimaryHDU()
    
        # Table HDU with PPXF output data
        cols = []
        cols.append( fits.Column(name='BIN_ID',         format='J', array=ubins             ))
        cols.append( fits.Column(name='V_STARS2',         format='D', array=stkin[:,0]             ))
        cols.append( fits.Column(name='SIGMA_STARS2',         format='D', array=stkin[:,1]             ))
        cols.append( fits.Column(name='CHI2_TOT',         format='D', array=chi2             ))
        #emission lines names
        names = np.char.array( linesfitted['name'] )+np.char.array(['{:d}'.format(int(j)) for j in linesfitted['lambda']])
       

        for i in range(len(names)):
            cols.append( fits.Column(name=names[i]+'_FLUX' , format='D', array=gas_flux_in_units[:,i]  ))
            cols.append( fits.Column(name=names[i]+'_FLUX_ERR' , format='D', array=gas_err_flux_in_units[:,i]  ))
            cols.append( fits.Column(name=names[i]+'_VEL' , format='D', array=vel_final[:,i]  ))
            cols.append( fits.Column(name=names[i]+'_VEL_ERR' , format='D', array=vel_err_final[:,i]  ))
            cols.append( fits.Column(name=names[i]+'_SIGMA' , format='D', array=sigma_final_measured[:,i]  ))
            cols.append( fits.Column(name=names[i]+'_SIGMA_ERR' , format='D', array=sigma_err_final[:,i]  ))
            cols.append( fits.Column(name=names[i]+'_SIGMA_CORR' , format='D', array=np.zeros( sigma_final_measured.shape[0] ) + templates_sigma[i]  ))
            
            if (extra is not None) and ('h3' in extra.keys()):
                cols.append( fits.Column(name=names[i]+'_H3' , format='D', array=extra['h3'][:,i]  ))
            if (extra is not None) and ('h4' in extra.keys()):
                cols.append( fits.Column(name=names[i]+'_H4' , format='D', array=extra['h4'][:,i]  ))

        dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
        dataHDU.name = 'EMLDATA_DATA'
    
        # Create HDU list and write to file
        HDUList = fits.HDUList([priHDU, dataHDU])
        HDUList.writeto(outfits_ppxf, overwrite=True)
    
        pipeline.prettyOutput_Done("Writing: "+outfits_ppxf.split('/')[-1])
        logging.info("Wrote: "+outfits_ppxf)
    
    
        # ========================
        # SAVE BESTFIT
        outfits_ppxf = outdir+rootname+'_ppxf-bestfit-emlines.fits'
        pipeline.prettyOutput_Running("Writing: "+outfits_ppxf.split('/')[-1])
    
        # Primary HDU
        priHDU = fits.PrimaryHDU()
    
        # Table HDU with PPXF bestfit
        cols = []
        cols.append( fits.Column(name='BIN_ID',  format='J',    array=ubins                    ))
        cols.append( fits.Column(name='BESTFIT', format=str(bestfit.shape[1])+'D', array=bestfit      ))
        cols.append( fits.Column(name='GAS_BESTFIT', format=str(gas_bestfit.shape[1])+'D', array=gas_bestfit      ))
        # cols.append( fits.Column(name='MASK', format='D', array=mask_for_original_spectra      ))
        dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
        dataHDU.name = 'FIT'
    
        # Create HDU list and write to file
        HDUList = fits.HDUList([priHDU, dataHDU])
        HDUList.writeto(outfits_ppxf, overwrite=True)
    
        pipeline.prettyOutput_Done("Writing: "+outfits_ppxf.split('/')[-1])
        logging.info("Wrote: "+outfits_ppxf)
    

        

# maybe change name to run module stellar kinematics
def runModule_PPXF_emlines(configs, logLam, log_spec, log_error, LSF,bin_id=None, 
    ppxf_results=None, only_lines=False):
   
    #print("")
    print("\033[0;37m"+" - - - - - Running Emission Lines Fitting - - - - - "+"\033[0;39m")
    logging.info(" - - - Running Emission Lines Fitting - - - ")

    #--> some bookkeeping
    # if there is only one spectrum to fit, make sure to reformat it 
    if log_spec.ndim==1:
        log_error= np.expand_dims(log_error, axis=1)
        log_spec= np.expand_dims(log_spec, axis=1)
    nbins    = log_spec.shape[1]
    ubins    = np.arange(0, nbins)
    npix_in     = log_spec.shape[0]
    n_spaxels_per_bin = np.zeros(nbins)
    if bin_id is None:
        bin_id = ubins
    # number of spaxels per bin
    for i in range(nbins):
        windx = (bin_id ==i)
        n_spaxels_per_bin[i]=np.sum(windx)
    velscale_ratio = 1
    # check if wavelength is in vacuum
    if 'WAV_VACUUM' in configs:
        wav_in_vacuum = configs['WAV_VACUUM']
    else:   
        wav_in_vacuum = False
    
    # for now the number of gas moments is fixed to 2 (i.e. v and sigma, no h3 and h4 etc for gas)
    if 'GAS_MOMENTS' not in configs:
        configs['GAS_MOMENTS']=2
   
    # regenerate the LSF interpolation function for simplicity, NOTE that I am using rest-frame wavelength
    LSF_InterpolationFunction  = interp1d(np.exp(logLam)/(1+configs['REDSHIFT']/cvel), LSF/(1+configs['REDSHIFT']/cvel),
         'linear', fill_value = 'extrapolate')
    # the wav range of the data (observed)
    LamRange = (np.exp(logLam[0]), np.exp(logLam[-1]))
    if 'MDEG_EMS' in configs:
        emi_mpol_deg = configs['MDEG_EMS']
    else:
        emi_mpol_deg = 8
    # define the velocity scale in kms
    velscale = (logLam[1]-logLam[0])*cvel
    #do not use additive polynomials whehn determining emission line fluxes!!
    adeg_lines=-1
    
    #--> generate the stellar templates
    templates_info = util_templates.prepare_sp_templates\
        (configs, velscale, velscale_ratio, LSF_InterpolationFunction, LamRange, wav_in_vacuum)
    star_templates,lamRange_spmod,logLam_template,min_wav_fit,max_wav_fit, nstpl = \
        templates_info['templates'], templates_info['WavRange'],templates_info['logLam'],\
        templates_info['min_wav_to_fit'],templates_info['max_wav_to_fit'],templates_info['templates'].shape[1]
    if only_lines==True:
        # the correct way of only fitting emission lines would have been to 
        # completely remove the stellar tempaltes from the components list
        # but it is simpler to just set them all to zero and keep the 
        star_templates=star_templates*0.0
        # it's usually not good to fit without polynomials if you get rid of the stellar templates
        adeg_lines=3

    # --> generate the gas templates
    emldb=table.Table.read(configs['EMI_FILE'] , format='ascii')
    if wav_in_vacuum:
        emldb['lambda'] = air_to_vac(emldb['lambda'])
    eml_fwhm_angstr = LSF_InterpolationFunction(emldb['lambda'])
    # note that while the stellar templates are expanded in wavelength to cover +/- 150 Angstrom around the observed spectra (buffer)
    # emission line tempaltes are only generated for lines whose central wavelength lies within the min and max rest-frame waveelngth of the data
    gas_templates, gas_names, line_wave, eml_tying = \
        util_templates.generate_emission_lines_templates(emldb, LamRange, configs, logLam_template, eml_fwhm_angstr)
    ngastpl = gas_templates.shape[1]

    # --> stack vertically stellar and gas templates
    templates = np.column_stack([star_templates, gas_templates])

    # --> cut the spectra to the wavelength range of the templates
    logLam_cut, log_spec_cut, log_error_cut, npix, mask_for_original_spectra,wav_cov_templates =\
        util_templates.cut_spectra_to_match_templates(logLam, log_spec, log_error, max_wav_fit,min_wav_fit)

    # --> Define goodpixels
    wav_mask_ppxf, _ = util_templates.get_Mask\
            ('EMSLINES', configs['EMI_FILE'], configs['REDSHIFT'], velscale, logLam_cut, 
            log_error_cut, wav_in_vacuum)

    # merge with the spectral coverage mask
    mask_for_original_spectra [wav_cov_templates] =wav_mask_ppxf
    # get the per-pixel mask by combining the wavelength mask with the info on the Err array
    mask_pixels = util_templates.get_pixel_mask(wav_mask_ppxf, log_error_cut)
    # finally transform to boolean for input to ppxf
    mask_pixels = mask_pixels.astype(bool)

    #--> define the systemic velocity due to template and data starting wavelength offset
    # NOTE: this is only correct if velscale ==1!!
    offset = ((logLam_template[0] - logLam_cut[0]) + np.log(1 +\
         configs['REDSHIFT']/cvel ) )* cvel
    
    # if you provide ppxf_results from a previous fit, you can now fix the 
    # kinematics to that of that previous fit
    if ppxf_results is not None:
        stellar_kinematics = np.copy(ppxf_results)
        nstmom =ppxf_results.shape[1]
        fixed_st_kin=True # if you want to test what happens you can set this to false, then 
        #code will use ppxf_results just a starting guess, but not fix the kinematics
        
    #otherwise the stellar_kinematics array just cointains reasonable starting guesses
    # but we allow ppxf to fit for stellar kinematics 
    else:
        nstmom =2
        stellar_kinematics =np.array([0, 100]*nbins).reshape(nbins, 2)
        fixed_st_kin=False
        
   
    tpl_comp, moments, start, bounds, tied, gas_comp =\
        util_templates.define_emission_line_input_for_ppxf(configs, nstpl, ngastpl, eml_tying,emldb,
        stellar_kinematics[0,:], fixed_st_kin=fixed_st_kin)
    
    n_gas_comp = len(np.unique(tpl_comp[gas_comp]))
    n_gas_templates = len(tpl_comp[gas_comp])

    # Array to store results of ppxf
    gas_kinematics         = np.zeros((nbins, n_gas_comp, configs['GAS_MOMENTS']))+np.nan 
    kinematics_all_err     = np.zeros((nbins, n_gas_comp, configs['GAS_MOMENTS']))  +np.nan             
    chi2                   = np.zeros((nbins))         
    gas_flux              = np.zeros((nbins,n_gas_templates))
    gas_flux_error         = np.zeros((nbins,n_gas_templates)) 
    bestfit                = np.zeros((nbins,npix))
    gas_bestfit            = np.zeros((nbins,npix))
    stkin                  = np.zeros((nbins,nstmom))
    stkin_err              = np.zeros((nbins,nstmom))
    
    # ====================
    # Run PPXF
    start_time = time.time()

    if configs['PARALLEL'] == True:
        pipeline.prettyOutput_Running("Running PPXF for emission lines analysis in parallel mode")
        logging.info("Running PPXF for emission lines analysis in parallel mode")

        # Create Queues
        inQueue  = Queue()
        outQueue = Queue()
    
        # Create worker processes
        ps = [Process(target=workerPPXF, args=(inQueue, outQueue))
                for _ in range(configs['NCPU'])]
    
        # Start worker processes
        for p in ps: p.start()
    
        # Fill the queue
        for i in range(nbins):
            # this changes the stellar kinematics starting guess for each bin
            start2 = copy.deepcopy(start)
            start2[0]=stellar_kinematics[i, :]
            inQueue.put( ( templates, log_spec_cut[:,i], log_error_cut[:,i], velscale,
                mask_pixels[:,i], adeg_lines, emi_mpol_deg, offset, velscale_ratio, 
                tpl_comp, moments, start2, bounds, tied, gas_comp,gas_names,
                nbins, i) )
    
        # now get the results with indices
        ppxf_tmp = [outQueue.get() for _ in range(nbins)]
    
        # send stop signal to stop iteration
        for _ in range(configs['NCPU']): inQueue.put('STOP')

        # stop processes
        for p in ps: p.join()
    
        # Get output
        index = np.zeros(nbins)

        # i, sol, kin_err, chi2, gas_flux, gas_flux_err, gas_names, bestfit, gas_bestfit
        for i in range(0, nbins):
            index[i]                                    = ppxf_tmp[i][0]
            gas_kinematics[i,:, :]                      = ppxf_tmp[i][1]
            kinematics_all_err[i,:, :]                  = ppxf_tmp[i][2]
            chi2[i]                                     = ppxf_tmp[i][3]
            gas_flux[i,:]                               = ppxf_tmp[i][4]
            gas_flux_error[i,:]                         = ppxf_tmp[i][5]
            # gas_names[i,:]                              = ppxf_tmp[i][6]
            bestfit[i,:]                                = ppxf_tmp[i][7]
            gas_bestfit[i,:]                            = ppxf_tmp[i][8]
            stkin[i,:]                                  = ppxf_tmp[i][9]
            stkin_err[i, :]                             = ppxf_tmp[i][10]

        # Sort output
        argidx = np.argsort( index )
        gas_kinematics         = gas_kinematics[argidx,:, :]  
        kinematics_all_err     = kinematics_all_err[argidx,:, :]               
        chi2                   = chi2[argidx]           
        gas_flux               = gas_flux[argidx,:] 
        gas_flux_error         = gas_flux_error[argidx,:] 
        # gas_names              = gas_names[argidx,:] 
        bestfit                = bestfit[argidx,:] 
        gas_bestfit            = gas_bestfit[argidx,:] 
        stkin                  =stkin[argidx, :]
        stkin_err              =stkin_err[argidx, :]

        pipeline.prettyOutput_Done("Running PPXF in parallel mode", progressbar=True)
  

    elif configs['PARALLEL'] == False:
        pipeline.prettyOutput_Running("Running PPXF in serial mode")
        logging.info("Running PPXF in serial mode")
        for i in range(0, nbins):
            start[0]=stellar_kinematics[i, :]
        
            gas_kinematics[i,:, :], kinematics_all_err[i,:, :],\
                chi2[i], gas_flux[i,:],gas_flux_error[i,:], _ ,\
                bestfit[i,:], gas_bestfit[i,:] , stkin[i,:], stkin_err[i,:]  = \
                run_ppxf(templates, log_spec_cut[:,i], log_error_cut[:,i], velscale,
                mask_pixels[:,i], adeg_lines, emi_mpol_deg, offset, velscale_ratio, 
                tpl_comp, moments, start, bounds, tied, gas_comp,gas_names,
                nbins, i)
        
        pipeline.prettyOutput_Done("Running PPXF in serial mode", progressbar=True)
    
    print("             Running PPXF on %s spectra took %.2fs" % (nbins, time.time() - start_time))
    #print("")
    logging.info("Running PPXF on %s spectra took %.2fs using %i cores" % (nbins, time.time() - start_time, configs['NCPU']))

    # Check if there was a problem with a spectra: NOT DONE

    # add back the part of the spectrum that was truncated because of lack of templates
    bestfit_1 = np.zeros((nbins,npix_in))
    gas_bestfit_1 = np.zeros((nbins,npix_in))
    bestfit_1[:, wav_cov_templates]=bestfit
    gas_bestfit_1[:, wav_cov_templates]=gas_bestfit
    
    # tidy up the ppXF output so it matches the order to the original line-list
    linesfitted, fluxes_final, fluxes_err_final, vel_final,vel_err_final, \
        sigma_final,sigma_err_final, extra= util_templates.tidy_up_fluxes_and_kinematics(gas_kinematics, 
        kinematics_all_err,gas_flux, gas_flux_error,emldb, eml_tying, configs['GAS_MOMENTS'])

    # get fluxes in the correct units, see Westfall et al eq 16
    gas_flux_in_units = fluxes_final*(velscale/constants.c.to('km/s').value)*\
        linesfitted['lambda']*(1+configs['REDSHIFT']/constants.c.to('km/s').value )
    gas_err_flux_in_units = fluxes_err_final*(velscale/constants.c.to('km/s').value)*\
        linesfitted['lambda']*(1+configs['REDSHIFT']/constants.c.to('km/s').value )
    # divide by the number of spaxels per bin to make the flux per spaxel
    for i in range(gas_flux_in_units.shape[0]):
        gas_flux_in_units[i, :]= gas_flux_in_units[i, :]/n_spaxels_per_bin[i]
        gas_err_flux_in_units[i, :]= gas_err_flux_in_units[i, :]/n_spaxels_per_bin[i]

    # add back the template LSF
    eml_fwhm_angstr = LSF_InterpolationFunction(linesfitted['lambda'])
    templates_sigma = eml_fwhm_angstr/\
        linesfitted['lambda']*constants.c.to('km/s').value/2.355

   # templates_sigma = np.zeros(sigma_final.shape)+templates_sigma
    sigma_final_measured  = (sigma_final**2 + templates_sigma**2)**(0.5)

    # save results to file
    save_ppxf_emlines(configs['ROOTNAME'], configs['OUTDIR'], linesfitted, 
        gas_flux_in_units, gas_err_flux_in_units,vel_final, vel_err_final,
        sigma_final_measured, sigma_err_final, chi2, templates_sigma, bestfit_1, gas_bestfit_1, stkin, ubins, extra)


    print("\033[0;37m"+" - - - - - Emission Lines Fitting done! - - - - -"+"\033[0;39m")
   #print("")
    logging.info(" - - - Emission Lines Fitting done - - - \n")
   
    
        
