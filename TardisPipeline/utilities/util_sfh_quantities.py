from astropy.table import Table
import numpy as np
from astropy import units as u
from astropy.io import fits


def compute_sfh_relevant_quantities(configs):
    masses_density, masses_density_err, ages_mw, ages_mw_err, zs_mw, zs_mw_err, ages_lw, ages_lw_err, zs_lw, zs_lw_err = compute_sfh_averages_MC(
        configs)
    masses_density_noMC, ages_mw_noMC, zs_mw_noMC, ages_lw_noMC, zs_lw_noMC = compute_sfh_averages_noMC(configs)
    # Compute errors from MC, nominal quantities from NoMC
    return masses_density_noMC, masses_density_err, ages_mw_noMC, ages_mw_err, zs_mw_noMC, zs_mw_err, ages_lw_noMC, ages_lw_err, zs_lw_noMC, zs_lw_err


def compute_sfh_light_mass_frac(configs, age_top=None, age_low=None, z_top=None, z_low=None):
    frac_light = compute_sfh_fraction_noMC(configs, age_top=age_top, age_low=age_low, z_top=z_top, z_low=z_low,
                                           weights_type='light')
    frac_mass = compute_sfh_fraction_noMC(configs, age_top=age_top, age_low=age_low, z_top=z_top, z_low=z_low,
                                          weights_type='mass')
    frac_light_mc, frac_light_err_mc = compute_sfh_fraction_MC(configs, age_top=age_top, age_low=age_low, z_top=z_top,
                                                               z_low=z_low, weights_type='light')
    frac_mass_mc, frac_mass_err_mc = compute_sfh_fraction_MC(configs, age_top=age_top, age_low=age_low, z_top=z_top,
                                                             z_low=z_low, weights_type='mass')
    return frac_light, frac_light_err_mc, frac_mass, frac_mass_err_mc


def compute_sfh_averages_noMC(configs):
    outdir = configs['OUTDIR']
    rootname = configs['ROOTNAME']
    table_binning = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_table_SFH.fits', )
    bin_id = table_binning[1].data['BIN_ID']
    tab_templates = Table.read(outdir + rootname + '_templates_SFH_info.fits')
    ppxf_sfh_results = Table.read(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf_SFH.fits')
    masses_density, ages_mw, zs_mw, ages_lw, zs_lw = get_quantities_from_table(ppxf_sfh_results, tab_templates, bin_id)

    return masses_density, ages_mw, zs_mw, ages_lw, zs_lw


def compute_sfh_averages_MC(configs):
    outdir = configs['OUTDIR']
    rootname = configs['ROOTNAME']
    table_mc = Table.read(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf_mc_results-SFH.fits')
    table_binning = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_table_SFH.fits', )
    bin_id = table_binning[1].data['BIN_ID']
    tab_templates = Table.read(outdir + rootname + '_templates_SFH_info.fits')
    nbins = len(np.unique(bin_id))
    masses_density = np.zeros(nbins)
    masses_density_err = np.zeros(nbins)
    ages_mw = np.zeros(nbins)
    ages_mw_err = np.zeros(nbins)
    zs_mw = np.zeros(nbins)
    zs_mw_err = np.zeros(nbins)
    ages_lw = np.zeros(nbins)
    ages_lw_err = np.zeros(nbins)
    zs_lw = np.zeros(nbins)
    zs_lw_err = np.zeros(nbins)
    for i in range(nbins):
        table_mc_i = table_mc[np.where(table_mc['BIN_ID'] == i)]
        masses_density_mc, ages_mw_mc, zs_mw_mc, ages_lw_mc, zs_lw_mc = get_quantities_from_table(table_mc_i,
                                                                                                  tab_templates, bin_id)
        masses_density[i] = np.mean(masses_density_mc)
        masses_density_err[i] = np.std(masses_density_mc)
        ages_mw[i] = np.mean(ages_mw_mc)
        ages_mw_err[i] = np.std(ages_mw_mc)
        zs_mw[i] = np.mean(zs_mw_mc)
        zs_mw_err[i] = np.std(zs_mw_mc)
        ages_lw[i] = np.mean(ages_lw_mc)
        ages_lw_err[i] = np.std(ages_lw_mc)
        zs_lw[i] = np.mean(zs_lw_mc)
        zs_lw_err[i] = np.std(zs_lw_mc)

    return masses_density, masses_density_err, ages_mw, ages_mw_err, zs_mw, zs_mw_err, ages_lw, ages_lw_err, zs_lw, zs_lw_err


def compute_sfh_fraction_noMC(configs, age_top=None, age_low=None, z_top=None, z_low=None, weights_type='light'):
    outdir = configs['OUTDIR']
    rootname = configs['ROOTNAME']
    tab_templates = Table.read(outdir + rootname + '_templates_SFH_info.fits')
    ppxf_sfh_results = Table.read(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf_SFH.fits')
    fractions = get_fractions_from_table(ppxf_sfh_results, tab_templates, age_top=age_top, age_low=age_low, z_top=z_top,
                                         z_low=z_low, weights_type=weights_type)
    return fractions


def compute_sfh_fraction_MC(configs, age_top=None, age_low=None, z_top=None, z_low=None, weights_type='light'):
    outdir = configs['OUTDIR']
    rootname = configs['ROOTNAME']
    table_mc = Table.read(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf_mc_results-SFH.fits')
    table_binning = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_table_SFH.fits', )
    bin_id = table_binning[1].data['BIN_ID']
    tab_templates = Table.read(outdir + rootname + '_templates_SFH_info.fits')
    nbins = len(np.unique(bin_id))
    fractions = np.zeros(nbins)
    fractions_err = np.zeros(nbins)

    for i in range(nbins):
        table_mc_i = table_mc[np.where(table_mc['BIN_ID'] == i)]
        fractions_mc = get_fractions_from_table(table_mc_i, tab_templates, age_top=age_top, age_low=age_low,
                                                z_top=z_top,
                                                z_low=z_low, weights_type=weights_type)
        fractions[i] = np.mean(fractions_mc)
        fractions_err[i] = np.std(fractions_mc)

    return fractions, fractions_err


def get_fraction_age_metallicity_range(tab_templates, weights, age_top=None, age_low=None, z_top=None, z_low=None,
                                       weights_type='light'):
    if weights_type not in ['light', 'mass']:
        raise ValueError('Please use weights_type "light" or "mass" only')
    age_top, age_low, z_top, z_low = define_limits(age_top, age_low, z_top, z_low, tab_templates)
    cond = get_age_z_bins(age_top, age_low, z_top, z_low, tab_templates)
    if weights_type == 'light':
        weights = weights * tab_templates['M*'].data / tab_templates['M*/Lv'].data
    weights = weights / sum(weights)
    return np.sum(weights[cond])


def get_age_z_bins(age_top, age_low, z_top, z_low, tab_templates):
    cond = np.where(np.logical_and(np.logical_and(tab_templates['Age'] >= age_low, tab_templates['Age'] <= age_top),
                                   np.logical_and(tab_templates['Z'] >= z_low, tab_templates['Z'] <= z_top)))
    return cond


def define_limits(age_top, age_low, z_top, z_low, tab_templates):
    age_top = np.max(tab_templates['Age']) if age_top is None else age_top
    age_low = np.min(tab_templates['Age']) if age_low is None else age_low
    z_top = np.max(tab_templates['Z']) if z_top is None else z_top
    z_low = np.min(tab_templates['Z']) if z_low is None else z_low
    return age_top, age_low, z_top, z_low


def get_weights_ppxf_output_table(ppxf_results_table, ntemplates):
    weights = np.zeros(ntemplates)
    for j in range(ntemplates):
        weights[j] = ppxf_results_table['w_' + str(j)]
    return weights


def get_quantities_from_table(ppxf_results_table, tab_templates, bin_id):
    n = len(ppxf_results_table)
    masses_density = np.zeros(n)
    ages_mw = np.zeros(n)
    zs_mw = np.zeros(n)
    ages_lw = np.zeros(n)
    zs_lw = np.zeros(n)
    ntemplates = len(tab_templates)
    LogAge_templates = np.log10(tab_templates['Age'].data * 1E9)
    for i in range(n):
        bin_id_i = ppxf_results_table['BIN_ID'][i]
        n_spaxels = len(np.where(bin_id == bin_id_i)[0])
        weights = get_weights_ppxf_output_table(ppxf_results_table[i], ntemplates)
        masses_density[i] = np.sum(weights * tab_templates['M*'].data) / n_spaxels
        ages_mw[i] = np.sum(weights * LogAge_templates) / np.sum(weights)
        zs_mw[i] = np.sum(weights * tab_templates['Z'].data) / np.sum(weights)
        weights_l = weights * tab_templates['M*'].data / tab_templates['M*/Lv'].data
        ages_lw[i] = np.sum(weights_l * LogAge_templates) / np.sum(weights_l)
        zs_lw[i] = np.sum(weights_l * tab_templates['Z'].data) / np.sum(weights_l)
    return masses_density, ages_mw, zs_mw, ages_lw, zs_lw


def get_fractions_from_table(ppxf_results_table, tab_templates, age_top=None, age_low=None, z_top=None, z_low=None,
                             weights_type='light'):
    n = len(ppxf_results_table)
    ntemplates = len(tab_templates)
    fractions = np.zeros(n)
    for i in range(n):
        weights = get_weights_ppxf_output_table(ppxf_results_table[i], ntemplates)
        fractions[i] = get_fraction_age_metallicity_range(tab_templates, weights, age_top=age_top, age_low=age_low,
                                                          z_top=z_top, z_low=z_low,
                                                          weights_type=weights_type)
    return fractions
