from astropy.io import fits
import numpy as np
from astropy import table

import glob
import os
import logging
import pdb
from scipy import ndimage

from  TardisPipeline.utilities import util as pipeline
from ppxf.ppxf_util import log_rebin, gaussian_filter1d


def apply_snr_threshold_fb(snr, min_snr):
    pipeline.prettyOutput_Running("Selecting those spaxels with SNR above some threshold")
    logging.info("Selecting those spaxels with SNR above some threshold")
    
    idx_inside  = np.where( snr >= min_snr )[0]
    idx_outside = np.where( snr < min_snr )[0]

    if len(idx_inside) == 0 and len(idx_outside) == 0:
        idx_inside = np.arange( len(snr) )
        idx_outside = np.array([], dtype=np.int64)
    
    pipeline.prettyOutput_Done("Selecting those spaxels with SNR above some threshold")
    logging.info("Selected spaxels: "+str(len(idx_inside))+" inside and "+str(len(idx_outside))+" outside of the Voronoi region. ")

    sp_class_sn={}
    sp_class_sn['INSIDE']=idx_inside
    sp_class_sn['OUTSIDE']=idx_outside

    return(sp_class_sn)


def log_rebinning(configs, cube):

    if cube['wave_type']=='lin':
        # Do log-rebinning for spectra
        pipeline.prettyOutput_Running("Log-rebinning the spectra")
        log_spec, logLam = run_logrebinning\
                (cube['spec'], cube['velscale'], len(cube['x']), cube['wave'], configs )
        pipeline.prettyOutput_Done("Log-rebinning the spectra", progressbar=True)
        logging.info("Log-rebinned the spectra")

        # Do log-rebinning for error spectra
        pipeline.prettyOutput_Running("Log-rebinning the error spectra")
        log_error, _ = run_logrebinning\
                (cube['error'], cube['velscale'], len(cube['x']), cube['wave'], configs )

        # REMEMBER noise =9999. means that the data needs to be masked
        # however in order to mask the correct amount of that after log rebinning one needs to
        # dilate the mask by one pixel to avoid partially masked pixels
        w = np.where(log_error==9999.)
        mask = np.copy(log_error)*0.0
        mask[w]=1
        mask2 = np.copy(mask)
        for kk in range(mask.shape[1]):
            nnum = np.where(mask[:, kk]>=1)[0]
            all_mask = np.unique( [nnum, nnum-1, nnum+1] )
            all_mask = all_mask[ (all_mask>=0) & (all_mask< mask.shape[0])]
            mask2[all_mask, kk]=1
        
        log_error[mask2==1]=9999.

        pipeline.prettyOutput_Done("Log-rebinning the error spectra", progressbar=True)
        logging.info("Log-rebinned the error spectra")
    
    elif cube['wave_type']=='log':
        log_spec = np.copy(cube['spec'])
        log_error = np.copy(cube['error'])
        # remember that logLam defined by Cappellari's logrebin procedure if the natural log
        # of the wavelength
        logLam = np.log(cube['wave'])
    
     # Save all spectra
    saveAllSpectra(configs['ROOTNAME'], configs['OUTDIR'], log_spec, log_error, cube['velscale'], logLam)

    cube['log_spec']=log_spec
    cube['log_error']=log_error
    cube['logLam']=logLam

    return(cube)


def run_logrebinning( bin_data, velscale, nbins, wave, configs={} ):

    # Setup arrays
    lamRange = np.array([np.amin(wave),np.amax(wave)])
    sspNew, logLam, _ = log_rebin(lamRange, bin_data[:,0], velscale=velscale)
    log_bin_data = np.zeros([len(logLam),nbins])

    # Do log-rebinning 
    for i in range(0, nbins):
        log_bin_data[:,i] = corefunc_logrebin(lamRange, bin_data[:,i], velscale, len(logLam), i, nbins)

    return(log_bin_data, logLam)


def corefunc_logrebin(lamRange, bin_data, velscale, npix, iterate, nbins):
    try:
        sspNew, logLam, _ = log_rebin(lamRange, bin_data, velscale=velscale)
        pipeline.printProgress(iterate+1, nbins, barLength = 50)
        return(sspNew)

    except:
        out = np.zeros(npix); out[:] = np.nan
        return(out)


def saveAllSpectra(rootname, outdir, log_spec, log_error, velscale, logLam):

    outfits_spectra  = outdir+rootname+'_AllSpectra.fits'
    pipeline.prettyOutput_Running("Writing: "+outfits_spectra.split('/')[-1])

    # Primary HDU
    priHDU = fits.PrimaryHDU()

    # Table HDU for spectra
    cols = []
    cols.append( fits.Column(name='SPEC',   format=str(len(log_spec))+'D', array=log_spec.T  ))
    cols.append( fits.Column(name='ESPEC',  format=str(len(log_spec))+'D', array=log_error.T ))
    dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    dataHDU.name = 'SPECTRA'

    # Table HDU for LOGLAM
    cols = []
    cols.append( fits.Column(name='LOGLAM', format='D', array=logLam ))
    loglamHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    loglamHDU.name = 'LOGLAM'
    
    # Create HDU List and save to file
    HDUList = fits.HDUList([priHDU, dataHDU, loglamHDU])
    HDUList.writeto(outfits_spectra, overwrite=True)

    # Set header keywords
    fits.setval(outfits_spectra,'VELSCALE', value=velscale)
    fits.setval(outfits_spectra,'CRPIX1',   value=1.0)
    fits.setval(outfits_spectra,'CRVAL1',   value=logLam[0])
    fits.setval(outfits_spectra,'CDELT1',   value=logLam[1]-logLam[0])

    pipeline.prettyOutput_Done("Writing: "+outfits_spectra.split('/')[-1])
    logging.info("Wrote: "+outfits_spectra)


def saveAllTemplates(rootname, outdir, log_spec, log_error, velscale, logLam):

    outfits_spectra  = outdir+rootname+'_Log_Templ.fits'
    pipeline.prettyOutput_Running("Writing: "+outfits_spectra.split('/')[-1])

    # Primary HDU
    priHDU = fits.PrimaryHDU()

    # Table HDU for spectra
    cols = []
    cols.append( fits.Column(name='SPEC',   format=str(len(log_spec))+'D', array=log_spec.T  ))
    dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    dataHDU.name = 'SPECTRA'

    # Table HDU for LOGLAM
    cols = []
    cols.append( fits.Column(name='LOGLAM', format='D', array=logLam ))
    loglamHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    loglamHDU.name = 'LOGLAM'

    # Table HDU for LOGLAM_RANGE
    cols = []
    cols.append( fits.Column(name='LOGLAM_RANGE', format='D', array=log_error))
    loglam_rangeHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    loglam_rangeHDU.name = 'LOGLAM_RANGE'
    
    # Create HDU List and save to file
    HDUList = fits.HDUList([priHDU, dataHDU, loglamHDU, loglam_rangeHDU])
    HDUList.writeto(outfits_spectra, overwrite=True)

    # Set header keywords
    fits.setval(outfits_spectra,'VELSCALE', value=velscale)
    fits.setval(outfits_spectra,'CRPIX1',   value=1.0)
    fits.setval(outfits_spectra,'CRVAL1',   value=logLam[0])
    fits.setval(outfits_spectra,'CDELT1',   value=logLam[1]-logLam[0])

    pipeline.prettyOutput_Done("Writing: "+outfits_spectra.split('/')[-1])
    logging.info("Wrote: "+outfits_spectra)


def loadAllSpectra(rootname, outdir, cube):
    hdu = fits.open(outdir+rootname+'_AllSpectra.fits')
    log_spec  = np.array( hdu[1].data.SPEC.T  )
    log_error = np.array( hdu[1].data.ESPEC.T )
    logLam    = np.array( hdu[2].data.LOGLAM  )

    
    cube['log_spec']=log_spec
    cube['log_error']=log_error
    cube['logLam']=logLam
    return(cube)

def load_PPXF_result(configs):
    fits_ppxf = configs['OUTDIR']+configs['ROOTNAME']+'_ppxf.fits'
    hdulist = fits.open(fits_ppxf)
    velocity = hdulist[1].data['V']
    sigma = hdulist[1].data['SIGMA']

    if 'H3' in hdulist[1].data.names and 'H4' in hdulist[1].data.names:
        ppxf_result = np.zeros((len(velocity), 4))
        ppxf_result[:,0]=velocity
        ppxf_result[:,1]=sigma
        ppxf_result[:,2]=hdulist[1].data['H3']
        ppxf_result[:,3]= hdulist[1].data['H4']
    elif 'H3' in hdulist[1].data.names and 'H4' not in hdulist[1].data.names:
        ppxf_result = np.zeros((len(velocity), 3))
        ppxf_result[:,0]=velocity
        ppxf_result[:,1]=sigma
        ppxf_result[:,2]=hdulist[1].data['H3']
    else:
        ppxf_result = np.zeros((len(velocity), 2))
        ppxf_result[:,0]=velocity
        ppxf_result[:,1]=sigma
    
    return(ppxf_result)

def load_WCS(configs):
    out = fits.open(configs['OUTDIR']+configs['ROOTNAME']+'_wcs.fits')
    wcs = out[1].data
    wcs={'CD1_1': [float(wcs['CD1_1'])], 'CD2_2': [float(wcs['CD2_2'] )], \
        'NAXIS1': [int(wcs['NAXIS1'] )], 'NAXIS2': [int(wcs['NAXIS2'] )], \
        'CRVAL1': [float(wcs['CRVAL1']) ], 'CRVAL2': [float(wcs['CRVAL2'])], \
        'CUNIT1':[str(wcs['CUNIT1'][0]) ],'CUNIT2':[str(wcs['CUNIT2'][0]) ],
        'CRPIX1': [float(wcs['CRPIX1']) ], 'CRPIX2': [float(wcs['CRPIX2']) ], 'FUNIT':[float(wcs['FUNIT']) ] }
    return(wcs)

def save_WCS(configs, wcs, cube):

    #cube = {'x': x, 'y': y, 'wave': wave, 'spec': spec, 'error': espec, 'snr': snr, 'snr_sfh': snr_sfh, \
    #        'signal': signal, 'noise': noise, 'velscale': velscale, 'pixelsize': pixelsize,
    #        'LSF_InterpolationFunction': LSF_InterpolationFunction, 'wave_type': wave_type}

    # Primary HDU
    priHDU = fits.PrimaryHDU()

    cols = []
    cols.append(fits.Column(name='ID',        format='J',   array=np.arange(len(cube['x'])) ))
    cols.append(fits.Column(name='X',         format='D',   array=cube['x']               ))
    cols.append(fits.Column(name='Y',         format='D',   array=cube['y']                 ))
    cols.append(fits.Column(name='FLUX',      format='D',   array=cube['signal']            ))
    cols.append(fits.Column(name='SNR',       format='D',   array=cube['snr']               ))
     
    tbhdu = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    hdr = tbhdu.header
    for key in wcs.keys():
        hdr.append( (key, wcs[key][0]) )
    tbhdu.header['PIXELSIZE'] = cube['pixelsize']
    tbhdu.header['VELSCALE'] = cube['velscale']
    tbhdu.header['WAVE_TYPE'] = cube['wave_type']
    tbhdu.name = 'GEOMETRY'

    wcs1 = table.Table(wcs)
    wcs_hdu = fits.BinTableHDU(wcs1)
    wcs_hdu.name= 'WCS'

    outfits_wcs = configs['OUTDIR']+configs['ROOTNAME']+'_wcs.fits'
    HDUList = fits.HDUList([priHDU, wcs_hdu, tbhdu ])
    HDUList.writeto(outfits_wcs, overwrite=True)

    logging.info("Wrote geometry table: "+outfits_wcs)

    
    