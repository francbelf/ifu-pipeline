from   astropy.io import fits, ascii
import numpy      as np
from astropy import table
from astropy import constants

import glob
import os
import logging
import pdb
import importlib

from  TardisPipeline.utilities import util as pipeline
from  TardisPipeline.utilities import util_lineprofiles
from astropy.table import Column

try:
    # Try to use local version in sitePackages
    from sitePackages.ppxf.ppxf_util import log_rebin, gaussian_filter1d, air_to_vac
except: 
    # Then use system installed version instead
    from ppxf.ppxf_util import log_rebin, gaussian_filter1d,air_to_vac

cvel = constants.c.to('km/s').value

def prepare_sp_templates(configs, velscale, velscale_ratio, LSF_InterpolationFunction, LamRange, 
    wav_in_vacuum=False, norm_templ='LIGHT'):

    pipeline.prettyOutput_Running("Preparing the stellar population templates")

    # SSP model library
    sp_models = glob.glob(configs['SSP_LIB']+'*.fits')
    
    # Read data
    hdu_spmod      = fits.open(sp_models[0])
    ssp_data       = hdu_spmod[0].data
    ssp_head       = hdu_spmod[0].header

    # I do not allow for AWAV-LOG ?
    if ssp_head['CTYPE1']=='AWAV': 
        wave = ssp_head['CRVAL1']+np.arange(ssp_head['NAXIS1'])*ssp_head['CD1_1']   
    else:
        message='Templates do not have the required CTYPE1 == AWAV'
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)

    if wav_in_vacuum:
        wave_int = air_to_vac(wave)
        # interpolate
        wave = np.linspace(wave_int[0], wave_int[-1], len(wave_int))
        ssp_data = np.interp(wave , wave_int, ssp_data)

        message = "The templates are defined in air but the data is in vacuum wavelength. I am converting the templates wavelength into vacuum "
        print(pipeline.prettyOutput_WarningPrefix() + message)
        logging.warning(message)
    
    lamRange_spmod = (wave[0], wave[-1])

    # Determine length of templates in observed wavelength range
    template_overhead = np.zeros(2)
    if LamRange[0] - lamRange_spmod[0] > 150.:
        template_overhead[0] = 150.
        min_wav_fit = LamRange[0]
    elif LamRange[0]- lamRange_spmod[0] > 5.: 
        template_overhead[0] = LamRange[0]  - lamRange_spmod[0] - 5
        min_wav_fit = LamRange[0]
    else:
        template_overhead[0] = 0
        min_wav_fit = lamRange_spmod[0] +5 
    if lamRange_spmod[1] - LamRange[1] > 150.:
        template_overhead[1] = 150.
        max_wav_fit = LamRange[1]
    elif lamRange_spmod[1] - LamRange[1]  > 5. :
        template_overhead[1] = lamRange_spmod[1] - LamRange[1]  - 5
        max_wav_fit = LamRange[1]
    else:
        template_overhead[1] = 0
        max_wav_fit = lamRange_spmod[1]  - 5 

    # Create new lamRange
    constr = np.array([ LamRange[0] - template_overhead[0], LamRange[1]  + template_overhead[1] ])

    idx_lam = np.where( np.logical_and(wave > constr[0], wave < constr[1] ) )[0]
    lamRange_spmod = np.array([ wave[idx_lam[0]], wave[idx_lam[-1]] ])
    # Shorten data to size of new lamRange
    ssp_data = ssp_data[idx_lam]

    # Convolve templates to same resolution as data
    if 'FWHM' in ssp_head:
        fwhm_templ = float(ssp_head['FWHM'])+np.zeros(len(wave[idx_lam]))
        CONVOLVE_TEMPLATES_STKIN=1
    elif 'FWHMF' in ssp_head:
        LSF_file= ascii.read(configs['SSP_LIB']+ssp_head['FWHMF'])
        fwhm_templ = np.interp(wave[idx_lam],  LSF_file['col1'], LSF_file['col2'])
        CONVOLVE_TEMPLATES_STKIN=1
    else:
        fwhm_templ = np.nan
        CONVOLVE_TEMPLATES_STKIN=0
        message = "The FWHM of the templates is unknown, no correction applied."
        print(pipeline.prettyOutput_WarningPrefix()+message)
        logging.warning(message)

    # all LSF are FHWH in angstrom
    if CONVOLVE_TEMPLATES_STKIN ==1:
        FWHM_data= LSF_InterpolationFunction(wave[idx_lam])
        FWHM_dif = np.zeros(len(FWHM_data))

        w_positive = (FWHM_data > fwhm_templ)

        FWHM_dif[w_positive] = np.sqrt( FWHM_data[w_positive]**2 - fwhm_templ[w_positive]**2 )
        sigma = FWHM_dif/2.355/ssp_head['CD1_1']

 
    # Create an array to store the templates
    sspNew, logLam_spmod, _ = log_rebin(lamRange_spmod, ssp_data, velscale=velscale/velscale_ratio)
    templates = np.empty((sspNew.size, len(sp_models)))
    
   
    # Load templates, convolve and log-rebin them
    for j, file in enumerate(sp_models):
        hdu      = fits.open(file)
        if wav_in_vacuum: 
            ssp_data = np.interp(wave , wave_int, hdu[0].data)
        else:
            ssp_data = hdu[0].data
        
        ssp_data = ssp_data[idx_lam]
        
        #convolve only if necessary
        if CONVOLVE_TEMPLATES_STKIN==1:
            sp_data = gaussian_filter1d(ssp_data, sigma)
        else:
            sp_data = ssp_data

        templates[:, j], logLam_spmod, _ = log_rebin(lamRange_spmod, sp_data, velscale=velscale/velscale_ratio)
    
    templates = normalise_templates(templates, norm_templ)

    # save all in a dictionary
    templates_info={'templates':templates, 'WavRange':[lamRange_spmod[0],lamRange_spmod[1]],'logLam':logLam_spmod,
        'min_wav_to_fit':min_wav_fit , 'max_wav_to_fit':max_wav_fit }

    pipeline.prettyOutput_Done("Preparing the stellar population templates")
    logging.info("Prepared the stellar population templates")


    return( templates_info)


def prepare_sp_templates_SFH(configs, velscale, velscale_ratio=1, LSF_InterpolationFunction=None, 
            LamRange=[3600, 10000], wav_in_vacuum=False, convolve_templates=True):
    pipeline.prettyOutput_Running("Preparing the stellar population templates")
    cvel = 299792.458
    # SSP model library
    sp_models = glob.glob(configs['SSP_LIB_SFH'] + '*.fits')

    # Read data
    hdu_spmod = fits.open(sp_models[0])
    ssp_data = hdu_spmod[0].data
    ssp_head = hdu_spmod[0].header

    # allow for AWAV-LOG
    if ssp_head['CTYPE1'] == 'AWAV':
        wave = ssp_head['CRVAL1'] + np.arange(ssp_head['NAXIS1']) * ssp_head['CD1_1']
    else:
        message='Templates do not have the required CTYPE1 == AWAV'
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)

    if wav_in_vacuum:
        wave_int = air_to_vac(wave)
        # interpolate
        wave = np.linspace(wave_int[0], wave_int[-1], len(wave_int))
        ssp_data = np.interp(wave , wave_int, ssp_data)

        message = "The templates are defined in air but the data is in vacuum wavelength. I am converting the templates wavelength into vacuum "
        print(pipeline.prettyOutput_WarningPrefix() + message)
        logging.warning(message)

    lamRange_spmod = (wave[0], wave[-1])

    # Determine length of templates in observed wavelength range
    template_overhead = np.zeros(2)
    if LamRange[0] - lamRange_spmod[0] > 150.:
        template_overhead[0] = 150.
        min_wav_fit = LamRange[0]
    elif LamRange[0] - lamRange_spmod[0] > 5.:
        template_overhead[0] = LamRange[0] - lamRange_spmod[0] - 5
        min_wav_fit = LamRange[0]
    else:
        template_overhead[0] = 0
        min_wav_fit = lamRange_spmod[0] + 5
    if lamRange_spmod[1] - LamRange[1] > 150.:
        template_overhead[1] = 150.
        max_wav_fit = LamRange[1]
    elif lamRange_spmod[1] - LamRange[1] > 5.:
        template_overhead[1] = lamRange_spmod[1] - LamRange[1] - 5
        max_wav_fit = LamRange[1]
    else:
        template_overhead[1] = 0
        max_wav_fit = lamRange_spmod[1] - 5

    # Create new lamRange
    constr = np.array([LamRange[0] - template_overhead[0], LamRange[1] + template_overhead[1]])

    idx_lam = np.where(np.logical_and(wave > constr[0], wave < constr[1]))[0]
    lamRange_spmod = np.array([wave[idx_lam[0]], wave[idx_lam[-1]]])
    # Shorten data to size of new lamRange
    ssp_data = ssp_data[idx_lam]

    # Convolve templates to same resolution as data
    if convolve_templates is False:
        CONVOLVE_TEMPLATES_STKIN = 0
    else:
        if 'FWHM' in ssp_head:
            fwhm_templ = float(ssp_head['FWHM']) + np.zeros(len(wave[idx_lam]))
            CONVOLVE_TEMPLATES_STKIN = 1
        elif 'FWHMF' in ssp_head:
            LSF_file = ascii.read(configs['SSP_LIB_SFH'] + ssp_head['FWHMF'])
            fwhm_templ = np.interp(wave[idx_lam], LSF_file['col1'], LSF_file['col2'])
            CONVOLVE_TEMPLATES_STKIN = 1
        else:
            message = "The FWHM of the templates is unknown, no correction applied."
            fwhm_templ = np.nan
            CONVOLVE_TEMPLATES_STKIN = 0
            print(pipeline.prettyOutput_WarningPrefix() + message)
            logging.warning(message)

    # if you require convolution calculate the kernel
    if CONVOLVE_TEMPLATES_STKIN ==1:
        # all LSF are FHWH in angstrom
        FWHM_data = LSF_InterpolationFunction(wave[idx_lam])
        FWHM_dif = np.zeros(len(FWHM_data))

        w_positive = (FWHM_data > fwhm_templ)

        FWHM_dif[w_positive] = np.sqrt(FWHM_data[w_positive] ** 2 - fwhm_templ[w_positive] ** 2)
        sigma = FWHM_dif / 2.355 / ssp_head['CD1_1']

    # Create an array to store the templates
    sspNew, logLam_spmod, _ = log_rebin(lamRange_spmod, ssp_data, velscale=velscale / velscale_ratio)
    templates = np.empty((sspNew.size, len(sp_models)))

    # Load templates, convolve and log-rebin them
    age_templates = []
    z_templates = []
    mstar_templates = []
    m2l_templates = []

    for j, file in enumerate(sp_models):
        hdu = fits.open(file)
        if wav_in_vacuum: 
            ssp_data = np.interp(wave , wave_int, hdu[0].data)
        else:
            ssp_data = hdu[0].data
        
        ssp_data = ssp_data[idx_lam]
        try:
            age = float(hdu[0].header['Age'])
            z = float(hdu[0].header['Z'])
        except:
            message='Templates do not have the required Age and Z header keywords'
            print(pipeline.prettyOutput_FailedPrefix() + message)
            logging.error(message)

        age_templates.append(age)
        z_templates.append(z)

        # use the templates into file to read the mass of its template (e.g. eMILES is normalised to 1 Msun at birth)
        # and the M/LV
        mass,M2L = get_template_mass(configs,age,z)
        mstar_templates.append(mass)
        m2l_templates.append(M2L)

        # convolve only if necessary
        if CONVOLVE_TEMPLATES_STKIN == 1:
            sp_data = gaussian_filter1d(ssp_data, sigma)
        else:
            sp_data = ssp_data

        templates[:, j], logLam_spmod, _ = log_rebin(lamRange_spmod, sp_data, velscale=velscale / velscale_ratio)

    median_templates = np.median(templates)
    templates = templates / np.median(templates)
    n_templates = len(sp_models)
    id_templates = np.linspace(0,n_templates-1,n_templates).astype(int)
    # save all in a dictionary
    templates_info = {'templates': templates, 'WavRange': [lamRange_spmod[0], lamRange_spmod[1]],
                      'logLam': logLam_spmod,
                      'min_wav_to_fit': min_wav_fit, 'max_wav_to_fit': max_wav_fit,
                      'median_templates': median_templates}

    tab_templates = table.Table()
    tab_templates.add_column(Column(data=id_templates,name='ID', dtype=int))
    tab_templates.add_column(Column(data=age_templates, name='Age', dtype=float))
    tab_templates.add_column(Column(data=z_templates, name='Z', dtype=float))
    tab_templates.add_column(Column(data=mstar_templates, name='M*', dtype=float))
    tab_templates.add_column(Column(data=m2l_templates, name='M*/Lv', dtype=float))

    outdir = configs['OUTDIR']
    rootname = configs['ROOTNAME']

    tab_templates.write(outdir+rootname+'_templates_SFH_info.fits', overwrite = True)
    pipeline.prettyOutput_Done("Preparing the stellar population templates")
    logging.info("Prepared the stellar population templates")

    return (templates_info)

def get_template_mass(configs,age,z):
    tab = table.Table.read(configs['SSP_LIB_SFH']+'templates_info.txt', format = 'ascii' )
    tab_z = tab[np.where(tab['Z']==z)]
    tab_age = tab_z[np.where(tab_z['Age'] == age)]
    if len(tab_age) != 1:
        message = 'Age: '+str(age) +', Z: '+str(z)+' Not found in table '+configs['SSP_LIB_SFH']+'templates_info.txt'
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        return np.nan,np.nan
    else:
        return tab_age['M(*+remn)'].data[0], tab_age['M(*+remn)/Lv'].data[0]




def normalise_templates(templates, NORM_TEMP):

    # Normalise templates in such a way to get mass-weighted results
    if NORM_TEMP == 'MASS':
        templates = templates / np.mean( templates )

    # Normalise templates in such a way to get light-weighted results
    if NORM_TEMP == 'LIGHT':
        for i in range( templates.shape[1] ):
            templates[:,i] = templates[:,i] / np.mean(templates[:,i], axis=0)

    return( templates )

def _find_primary(emldb, i, primary=False):
        """
        Return the index of the line to which this one is tied.
        
        The result may be that this line is tied to one that is also
        tied to a second line.  If that's the case, the ``primary``
        keyword can be use to trace the parameter tying back to the
        independent line.

        Arg:
            i (int): The index of the line in the database.
            primary (bool): (**Optional**) Trace the line tying all the
                way back to the independent (primary) line.

        Return:
            int: The index of the line to which this one is tied.

        Raises:
            ValueError: Raised if the primary option is selected and the
                line does not trace back to a primary line.  This
                represents a poorly constructed emission-line database
                and should be avoided!
        """
        db_rows = np.arange(len(emldb))
        indx = db_rows[emldb['index'] == int(emldb['mode'][i][1:])][0]
        if not primary:
            return indx
        max_iter = 100
        j = 0
        while emldb['mode'][indx] != 'f' and j < max_iter:
            indx = db_rows[emldb['index'] == int(emldb['mode'][indx][1:])][0]
            j+=1
        if j == max_iter:
            raise ValueError('Line {0} (index={1}) does not trace back to a primary line!'.format(
                                i, emldb['index'][i]))
        return indx


def generate_emission_lines_templates(emldb,LamRange,configs, logLam, eml_fwhm_angstr):
    
    
    wave = np.exp(logLam)
    
    # ignore lines that fall outside the wavelength range covered by the data (in rest-frame)
    # note that the stellar templates extend further becauxe of the 150 km/s buffer on either side of the data
    extra_ignore = (emldb['lambda'] > LamRange[1]/(1+ configs['REDSHIFT']/cvel)   ) | (emldb['lambda']< LamRange[0]/(1+ configs['REDSHIFT']/cvel) )
    emldb['action'][extra_ignore] = 'i'
    
    # Get the list of lines to ignore
    ignore_line = emldb['action'] != 'f'
    
    # The total number of templates to construct is the number of
    # lines in the database minus the number of lines with mode=aN
    tied_all = np.array([m[0] == 'a' for m in emldb['mode']])
    nlinesdb=len(emldb)
    ntpl = nlinesdb - np.sum(ignore_line) - np.sum(tied_all)

    # Initialize the components
    comp = np.zeros(ntpl, dtype=int)-1
    vgrp = np.zeros(ntpl, dtype=int)-1
    sgrp = np.zeros(ntpl, dtype=int)-1
    
    # All the primary lines go into individual templates, kinematic
    # components, velocity groups, and sigma groups
    tpli = np.zeros(len(emldb), dtype=int)-1
    primary_line = (emldb['mode'] == 'f') & np.invert(ignore_line)

    nprimary = np.sum(primary_line)
    tpli[primary_line] = np.arange(nprimary)
    comp[:nprimary] = np.arange(nprimary)
    vgrp[:nprimary] = np.arange(nprimary)
    sgrp[:nprimary] = np.arange(nprimary)
    
    # some lines are not primary nor are being ignored
    # this means they are tied in some way
    finished = primary_line | ignore_line
    while np.sum(finished) != nlinesdb:
        # Find the indices of lines that are tied to finished lines
        start_sum = np.sum(finished)
        
        for i in range(nlinesdb):
            
            if finished[i]:
                continue
            indx = _find_primary(emldb, i)
            if not finished[indx]:
                continue
    
            finished[i] = True
    
            # Mode=a: Line is part of an existing template
            if emldb['mode'][i][0] == 'a':
                tpli[i] = tpli[indx]
            # Mode=k: Line is part of a different template but an
            # existing kinematic component
            if emldb['mode'][i][0] == 'k':
                tpli[i] = np.amax(tpli)+1
                comp[tpli[i]] = comp[tpli[indx]]
                vgrp[tpli[i]] = vgrp[tpli[indx]]
                sgrp[tpli[i]] = sgrp[tpli[indx]]
            # Mode=v: Line is part of a different template and
            # kinematic component with an untied sigma, but tied to
            # an existing velocity group
            if emldb['mode'][i][0] == 'v':
                tpli[i] = np.amax(tpli)+1
                comp[tpli[i]] = np.amax(comp)+1
                sgrp[tpli[i]] = np.amax(sgrp)+1
                vgrp[tpli[i]] = vgrp[tpli[indx]]
            # Mode=s: Line is part of a different template and
            # kinematic component with an untied velocity, but tied
            # to an existing sigma group
            if emldb['mode'][i][0] == 's':
               tpli[i] = np.amax(tpli)+1
               comp[tpli[i]] = np.amax(comp)+1
               vgrp[tpli[i]] = np.amax(vgrp)+1
               sgrp[tpli[i]] = sgrp[tpli[indx]]
    
        # If the loop ends up with the same number of parsed lines
        # that it started with, there must be an error in the
        # construction of the input database.
        if start_sum == np.sum(finished):
            raise ValueError('Unable to parse the input database.  Check tying parameters.')
    
    # Debug:
    if np.any(comp < 0) or np.any(vgrp < 0) or np.any(sgrp < 0):
        raise ValueError('Templates without an assigned component.  Check the input database.')
        
    #put everything in a dictionary
    eml_tying = {'tpli':tpli, 'comp':comp, 'vgrp':vgrp, 'sgrp':sgrp }
    
    _dw = np.diff(logLam)[0]
    _restwave  = np.log(emldb['lambda'])
    
    # Rest wavelength in pixel units
    _restwave = (_restwave - logLam[0])/_dw
    
    # Flux to pixel units; less accurate when spectrum is
    # logarithmically binned
    dl = emldb['lambda']*(np.exp(_dw/2)-np.exp(-_dw/2)) 
    _flux = emldb['A_i']

    # Dispersion in pixel units
    _sigma = eml_fwhm_angstr/dl/2.355

    # Construct the templates
    pix = np.arange(wave.size)
    flux = np.zeros((ntpl,wave.size), dtype=float)
    gas_names = []
    line_wave=[]
    for i in range(ntpl):
        # Find all the lines associated with this template:
        wtemp=tpli == i
        index = np.arange(nlinesdb)[wtemp]
        gas_names.append(emldb['name'][wtemp][0])
        line_wave.append(emldb['lambda'][wtemp][0])
        # Add each line to the template
        for j in index:
            # Declare an instance of the desired profile
            profile = util_lineprofiles.FFTGaussianLSF()
            # Use the first three moments of the line to set the
            # parameters
            p = profile.parameters_from_moments(_flux[j], _restwave[j], _sigma[j])
            # Add the line to the flux in this template
            flux[i,:] += profile(pix, p)
            
    
    gas_templates, gas_names, line_wave = flux, np.array(gas_names), \
        np.array(line_wave)
    return(gas_templates.T, gas_names, line_wave, eml_tying)
    
def cut_spectra_to_match_templates(logLam, log_spec, log_err, max_wav_fit,min_wav_fit):
    
    # mask the part of the spectrum which is not covered by the templates
    # otherwise you trip the lem(templates) < len(spectra) exception in ppxf
    wav_cov_templates = (np.exp(logLam) < max_wav_fit) & (np.exp(logLam) > min_wav_fit)
    mask_for_original_spectra = np.zeros(len(logLam))
    logLam2, log_spec2, log_error2 = logLam[wav_cov_templates], \
        log_spec[wav_cov_templates],log_err[wav_cov_templates]
    #number of wavelength channels
    npix     = log_spec2.shape[0]
    mask_for_original_spectra[~wav_cov_templates] = 2
    
    #output is new wavelength, spectra, errors, new number of wavelength pixels,
    # mask array for the original spectrum with the CUT REGIONS set to 2!! (NOTE NON STANDARD USE)
    # wav_cov_templates is the overlap region between old and new wav range
    return(logLam2, log_spec2, log_error2, npix, mask_for_original_spectra,wav_cov_templates)


def get_Mask(mode, emi_file, redshift, velscale, logLam_galaxy,logErr, 
             sky_file = None, wav_in_vacuum=False, masking_width=None):

    emldb=table.Table.read(emi_file, format='ascii')
    if wav_in_vacuum:
        emldb['lambda'] = air_to_vac(emldb['lambda'])

    npix = len(logLam_galaxy)
    # Create goodpixels and final emission line setup
    if masking_width is None:
        masking_width = 400.0 # km/s, the width is from -masking_width to + masking_width around the redshifted position of line
    #else mask a specific amount fixed by the instruments
        
    # this part of the mask is the same for all spectra
    if mode =='ST_KIN':
        mask_out= mask_emission_lines\
            (npix, redshift, emldb, velscale, logLam_galaxy[0], (logLam_galaxy[1]-logLam_galaxy[0]), masking_width, mode)
    elif mode =='EMSLINES':
        mask_out= mask_emission_lines\
            (npix, redshift, emldb, velscale, logLam_galaxy[0], (logLam_galaxy[1]-logLam_galaxy[0]), masking_width ,mode)
    elif mode == 'SFH':
        mask_out= mask_emission_lines\
            (npix, redshift, emldb, velscale, logLam_galaxy[0], (logLam_galaxy[1]-logLam_galaxy[0]), masking_width ,mode, sky_file =  sky_file)
    return(mask_out)

def get_pixel_mask(wav_mask, logErr):

    # make the mask the same shape as the flux and error spectra

    mask_pixel = np.array([ np.zeros(logErr.shape[1])+bb for bb in wav_mask])

    # mask regions of the spectrum with noise = 9999.
    ww = (logErr >= 9999.)
    #this mask follows the ppxf convention: i.e. 0 is a masked pixel and 1 is used
    mask_pixel[ww]=0.
    return (mask_pixel)


#################################################################
def mask_emission_lines(npix,Vsys,emldb, velscale,l0_gal,lstep_gal, sigm, mode, sky_file = None):

    goodpixels = np.arange(0,npix)  
    tmppixels  = np.copy(goodpixels)
    mask = np.ones(npix)
    # looping over the listed emission-lines and mask those tagged with an
    # 'm' for mask. Mask sky lines at rest-frame wavelength
    if sigm==0:
        emldb['action']='i'

    if mode == 'ST_KIN':
        for i in np.arange(0, len(emldb)):
            if ( emldb['action'][i]== 'm') | (emldb['action'][i]== 'f'):
                if (emldb['name'][i]!= 'sky'):
                    meml_cpix = np.ceil((np.log(emldb['lambda'][i])-l0_gal)/lstep_gal+Vsys/velscale)

                # sky lines are at rest-frame
                if (emldb['name'][i]== 'sky'):
                    meml_cpix = np.ceil((np.log(emldb['lambda'][i])-l0_gal)/lstep_gal) 

                # set the width of the mask (\pm Width) in pixels using either
                # 3 times the sigma of each line in the emission-line setup 
                # or the provided sigma value 
                if (sigm is not None):
                    msigma = sigm/velscale
                if (sigm is None):
                    msigma = 400./velscale
                meml_bpix = meml_cpix - msigma
                meml_rpix = meml_cpix + msigma
                w = np.where((goodpixels >= meml_bpix) & (goodpixels <= meml_rpix)) 
                if (np.size(w) != 0):
                    tmppixels[w] = -1 
                elif(np.size(w) ==0):
                    emldb['action'][i] = 'i' 
    if mode == 'EMSLINES':
        for i in np.arange(0, len(emldb)):
            if ( emldb['action'][i]== 'm'):
                if (emldb['name'][i]!= 'sky'):
                    meml_cpix = np.ceil((np.log(emldb['lambda'][i])-l0_gal)/lstep_gal+Vsys/velscale)

                # sky lines are at rest-frame
                if (emldb['name'][i]== 'sky'):
                    meml_cpix = np.ceil((np.log(emldb['lambda'][i])-l0_gal)/lstep_gal) 

                # set the width of the mask (\pm Width) in pixels using either
                # 3 times the sigma of each line in the emission-line setup 
                # or the provided sigma value 
                if (sigm is not None):
                    msigma = sigm/velscale
                if (sigm is None):
                    msigma = 400./velscale
                meml_bpix = meml_cpix - msigma
                meml_rpix = meml_cpix + msigma
                w = np.where((goodpixels >= meml_bpix) & (goodpixels <= meml_rpix)) 
                if (np.size(w) != 0):
                    tmppixels[w] = -1 
                elif(np.size(w) ==0):
                    emldb['action'][i] = 'i'
    if mode == 'SFH':
        for i in np.arange(0, len(emldb)):
            if ( emldb['action'][i]== 'm') | (emldb['action'][i]== 'f'):     #EMISSION LINES
                if (emldb['name'][i]!= 'sky'):
                    meml_cpix = np.ceil((np.log(emldb['lambda'][i])-l0_gal)/lstep_gal+Vsys/velscale) #No rest frame

                if (sigm is not None):
                    msigma = sigm/velscale
                if (sigm is None):
                    msigma = 400./velscale
                meml_bpix = meml_cpix - msigma
                meml_rpix = meml_cpix + msigma
                w = np.where((goodpixels >= meml_bpix) & (goodpixels <= meml_rpix))
                if (np.size(w) != 0):
                    tmppixels[w] = -1
                elif(np.size(w) ==0):
                    emldb['action'][i] = 'i'
        if sky_file is not None:
            sky_ranges = table.Table.read(sky_file, format = 'ascii')
            for i in range(len(sky_ranges)):
                meml_bpix = np.ceil((np.log(sky_ranges['lambda_ini'][i])-l0_gal)/lstep_gal)
                meml_rpix = np.ceil((np.log(sky_ranges['lambda_end'][i]) - l0_gal) / lstep_gal)   #Rest frame
                w = np.where((goodpixels >= meml_bpix) & (goodpixels <= meml_rpix))
                if (np.size(w) != 0):
                    tmppixels[w] = -1



    w = np.where(tmppixels != -1)
    goodpixels = goodpixels[w]

    w = (tmppixels != -1)
    mask[~w] = 0

    return mask, goodpixels

#################################################################
def define_emission_line_input_for_ppxf(configs, nstpl, ngastpl, eml_tying,emldb,
                                        stellar_kinematics, fixed_st_kin=False):
    tpl_comp = np.append(np.zeros(nstpl, dtype=int), eml_tying['comp']+1)
    
    #total number of templates
    n_templ= len(tpl_comp)
    #total number of (kinematic) components
    n_comp = len(np.unique(tpl_comp))
    # select gas components
    gas_comp = tpl_comp>0
    # two moments per kinematics component
    moments = np.ones(n_comp, dtype=int)+1
    moments[0] = configs['MOM']
    moments[1:] = configs['GAS_MOMENTS']
    
    #the stellar kinematics bounds need to have the right shape depending on the number 
    # of stellar components
    if moments[0]==2:
         bounds_kin_stars = [[-499, 499], [1., 499] ]
    elif moments[0]==3:  
         bounds_kin_stars = [[-499, 499], [1., 499], [-0.3, 0.3] ]
    elif moments[0]==4:  
         bounds_kin_stars = [[-499, 499], [1., 499], [-0.3, 0.3], [0.001, 0.3] ]
         
         
    if moments[1]==2:
         bounds_kin_gas = [[-499, 499], [1., 499] ]
    elif moments[1]==3:  
         bounds_kin_gas = [[-499, 499], [1., 499], [-0.3, 0.3] ]
    elif moments[1]==4:  
         bounds_kin_gas = [[-499, 499], [1., 499], [-0.3, 0.3], [0.001, 0.3] ]
         
    # total number of moments     
    n_tot_moments = np.sum(moments)
    # the only thing to change to fix the stellar kinematics is to do
    if fixed_st_kin==True:
        moments[0]=-moments[0]
    # two kinematic moments per components

    v_g = [np.array(emldb['V_g'])[eml_tying['tpli']==i][0] for i in np.arange(ngastpl)]
    sigma_g = [np.array(emldb['sig_g'])[eml_tying['tpli']==i][0] for i in np.arange(ngastpl)]
    start = []
    bounds=[]
    for i in range(n_comp):
        if i==0:
            start.append(stellar_kinematics)
            bounds.append( bounds_kin_stars )
        else:
            if moments[1]==2:
                start_gas = [v_g[i-1], sigma_g[i-1] ]
            if moments[1]==3:
                start_gas = [v_g[i-1], sigma_g[i-1], 0]
            if moments[1]==4:
                start_gas = [v_g[i-1], sigma_g[i-1] , 0, 0]
                
            start.append(start_gas  )
            bounds.append(bounds_kin_gas)

    #      Parse the velocity and sigma groups into tied parameters
    tied_flat = np.empty(n_tot_moments, dtype=object)
    tpl_index = np.arange(n_templ)
    
    tpl_vgrp = np.append(np.zeros(nstpl, dtype=int), eml_tying['vgrp']+1)
    tpl_sgrp = np.append(np.zeros(nstpl, dtype=int), eml_tying['sgrp']+1)
    
    
    for i in range(n_comp):
        # Do not allow tying to fixed components?
        if moments[i] < 0:
            continue
        # Velocity group of this component
        indx = np.unique(tpl_comp[tpl_index[tpl_vgrp == i]])
        if len(indx) > 1:
            parn = [ 0 + np.sum(np.absolute(moments[:j])) for j in indx ]
            tied_flat[parn[1:]] = 'p[{0}]'.format(parn[0])
        
        # Sigma group of this component
        indx = np.unique(tpl_comp[tpl_index[tpl_sgrp == i]])
        if len(indx) > 1:
            parn = [ 1 + np.sum(np.absolute(moments[:j])) for j in indx ]
            tied_flat[parn[1:]] = 'p[{0}]'.format(parn[0])
    
    tied_flat[[t is None for t in tied_flat ]] = ''
    
    # reshape the tied array so it matches the shape of start 
    tied = []
    track = 0
    for i in range(n_comp):  
        tied.append(list( tied_flat[track:track+np.absolute(moments[i])]) )
        track = track+np.absolute(moments[i])
        
    return(tpl_comp, moments, start, bounds, tied, gas_comp)

def tidy_up_fluxes_and_kinematics(gas_kinematics, kinematics_all_err,gas_flux,
                                  gas_flux_error, emldb, eml_tying, gas_moments):
    
    w = (emldb['action']=='f')
    linesfitted = emldb[w]
    tpli_fitted = eml_tying['tpli'][w]
    nbins =gas_kinematics.shape[0]
    ngascomp =  len(np.unique(eml_tying['comp']))
    ngastpl = len(np.unique(tpli_fitted))
    
    fluxes_final = np.zeros( (nbins, len(tpli_fitted)) )
    fluxes_err_final = np.zeros((nbins, len(tpli_fitted)))
    vel_final = np.zeros((nbins, len(tpli_fitted)))
    vel_err_final = np.zeros((nbins, len(tpli_fitted)))  
    sigma_final = np.zeros((nbins, len(tpli_fitted)))
    sigma_err_final = np.zeros((nbins, len(tpli_fitted)))  
    if gas_moments >=3:
        h3_final = np.zeros((nbins, len(tpli_fitted)))
    if gas_moments ==4:
        h4_final = np.zeros((nbins, len(tpli_fitted)))
    
    for j in range(ngastpl):
        #which line(s) correspond to this template
        templates_x= np.arange(len(tpli_fitted))[tpli_fitted==j]
        #component number of this template
        component_x = eml_tying['comp'][j]
        # if more than one line in this template...
        if len(templates_x)>1:
            total_flux_template = np.sum(linesfitted['A_i'][templates_x])
            stronger_line = templates_x[linesfitted['A_i'][templates_x] ==1.][0]
            weaker_lines = templates_x[linesfitted['A_i'][templates_x] !=1.]
            fluxes_final[:,stronger_line] = gas_flux[:, j]/total_flux_template
            fluxes_err_final[:,stronger_line] = gas_flux_error[:, j]/total_flux_template
        

            for kk in range(len(weaker_lines)):
                fluxes_final[:, weaker_lines[kk]] = \
                    gas_flux[:, j]/total_flux_template *\
                    linesfitted['A_i'][weaker_lines[kk]]
                fluxes_err_final[:, weaker_lines[kk]] = \
                    gas_flux_error[:, j]/total_flux_template *\
                    linesfitted['A_i'][weaker_lines[kk]]
         
            
        else:
            fluxes_final[:, templates_x[0]] = gas_flux[:, j]
            fluxes_err_final[:, templates_x[0]] = gas_flux_error[:, j]

        
    
    for j in range(ngastpl):
        #which line(s) correspond to this template
        # linesfitted[templates_x] will give you the info on this line(s)
        templates_x= np.arange(len(tpli_fitted))[tpli_fitted==j]
        #component number of this template
        component_x = eml_tying['comp'][j]
        # now list all the components that can have
        # sigma and velocity group number of this template
        sigma_x = eml_tying['comp'] [ eml_tying['sgrp']== eml_tying['sgrp'][j]]
        v_x = eml_tying['comp'][eml_tying['vgrp'] == eml_tying['vgrp'][j]]
        
        # in case there is a component with a lower number than component_x
        # in this velocity/sigma group it means the  kinematics of that line
        # is tied to the one of that component, so replace  component_x with component_vx
        # in order to get the ERRORS of the primary (aka free) component
        # otherwise you would get zerro errors because line is tied
        component_vx = np.min(v_x)
        component_sx = np.min(sigma_x)

        #components tied which have therefore zero errors
        # THESE LINES causes ENDLESS problems because of the ==0 conditions
        # tied_vel  = np.arange(ngascomp)[kinematics_all_err[0, :, 0]==0]
        # tied_sigma  = np.arange(ngascomp)[kinematics_all_err[0, :, 1]==0]
        
        # 
        # if component_x in tied_vel: 
        #     component_vx = v_x[np.array([i not in tied_vel for i in v_x])][0]
        # if component_x in tied_sigma: 
        #     component_sx = sigma_x[np.array([i not in tied_sigma for i in sigma_x])][0]
        
        for kk in range(len(templates_x)):
            vel_final[:, templates_x[kk]] = gas_kinematics[:,component_x, 0 ]
            sigma_final[:, templates_x[kk]] = gas_kinematics[:,component_x, 1 ]
        
            vel_err_final[:, templates_x[kk]] = kinematics_all_err[:,component_vx, 0 ]
            sigma_err_final[:, templates_x[kk]] = kinematics_all_err[:,component_sx, 1 ]

            extra = None
            if gas_moments >=3:
                h3_final[:, templates_x[kk]] = gas_kinematics[:,component_x, 2 ]
                extra = {}
                extra['h3']=h3_final
            if gas_moments ==4:
                h4_final[:, templates_x[kk]] = gas_kinematics[:,component_x, 3 ]
                extra['h4']=h4_final

        
        
    return(linesfitted, fluxes_final, fluxes_err_final, vel_final,vel_err_final, \
           sigma_final,sigma_err_final, extra)