import sys
from   astropy.io   import fits
import numpy        as np
import TardisPipeline as tardis_module
import glob
import os

# ==============================================================================
#                          P R E T T Y   O U T P U T
# ==============================================================================
def printProgress(iteration, total, prefix = '', suffix = '', decimals = 2, barLength = 80, color = 'g'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        color       - Optional  : color identifier (Str)
    """
    if   color == 'y': color = '\033[43m'
    elif color == 'k': color = '\033[40m'
    elif color == 'r': color = '\033[41m'
    elif color == 'g': color = '\033[42m'
    elif color == 'b': color = '\033[44m'
    elif color == 'm': color = '\033[45m'
    elif color == 'c': color = '\033[46m'

    filledLength    = int(round(barLength * iteration / float(total)))
    percents        = round(100.00 * (iteration / float(total)), decimals)
    bar             = color + ' '*filledLength + '\033[49m' + ' '*(barLength - filledLength -1)
    sys.stdout.write('\r%s |%s| %s%s %s\r' % (prefix, bar, percents, '%', suffix)),
    sys.stdout.flush()

def prettyOutput_Running(outputlabel):
    sys.stdout.write("\r [ "+'\033[0;37m'+"RUNNING "+'\033[0;39m'+"] "+outputlabel)
    sys.stdout.flush(); print("")

def prettyOutput_Done(outputlabel, progressbar=False):
    if progressbar == True:
        sys.stdout.write("\033[K")
    sys.stdout.write("\033[F"); sys.stdout.write("\033[K")
    sys.stdout.write("\r\r [ "+'\033[0;32m'+"DONE    "+'\033[0;39m'+"] "+outputlabel)
    sys.stdout.flush(); print("")

def prettyOutput_Warning(outputlabel, progressbar=False):
    if progressbar == True:
        sys.stdout.write("\033[K")
    sys.stdout.write("\033[F"); sys.stdout.write("\033[K")
    sys.stdout.write("\r\r [ "+'\033[0;33m'+"WARNING "+'\033[0;39m'+"] "+outputlabel)
    sys.stdout.flush(); print("")

def prettyOutput_Failed(outputlabel, progressbar=False):
    if progressbar == True:
        sys.stdout.write("\033[K")
    sys.stdout.write("\033[F"); sys.stdout.write("\033[K")
    sys.stdout.write("\r\r [ "+'\033[0;31m'+"FAILED  "+'\033[0;39m'+"] "+outputlabel)
    sys.stdout.flush(); print("")

def prettyOutput_DonePrefix():
    return(" [ "+'\033[0;32m'+"DONE    "+'\033[0;39m'+"] ")

def prettyOutput_WarningPrefix():
    return(" [ "+'\033[0;33m'+"WARNING "+'\033[0;39m'+"] ")

def prettyOutput_FailedPrefix():
    return(" [ "+'\033[0;31m'+"FAILED  "+'\033[0;39m'+"] ")

def printTemplates():
    """
    Prints a list of the stellar templates available
    """
    codedir = os.path.dirname(os.path.realpath(tardis_module.__file__))
    output = glob.glob(codedir+'/Templates/spectralTemplates/*/')
    templates = [out.split('/')[-2] for out in output]
    print(templates)

def printStellarPopTemplates():
    """
    Prints a list of the templates available from stellar population analysis
    """
    codedir = os.path.dirname(os.path.realpath(tardis_module.__file__))
    output = glob.glob(codedir+'/Templates/spectralTemplates/*/*templates_info.txt')
    templates = [out.split('/')[-2] for out in output]
    print(templates)

def printIFUs():
    """
    Prints a list of the available IFUs
    """
    codedir = os.path.dirname(os.path.realpath(tardis_module.__file__))
    output = glob.glob(codedir+'/readData/*py')
    # remove the __init__ file
    output=output[1:]
    ifu = [out.split('/')[-1] for out in output]
    ifu = [out.split('.')[0] for out in ifu]
    print(ifu)