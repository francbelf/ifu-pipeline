import numpy    as np
from astropy.io import fits
from multiprocessing import Queue, Process
from scipy.interpolate import interp1d
from astropy import constants
from astropy import units as u
import copy
from astropy import table
import pdb

import time
import logging

from TardisPipeline.utilities import util as pipeline
from TardisPipeline.utilities import util_prepare
from TardisPipeline.utilities import util_templates

try:
    # Try to use local version in sitePackages
    from sitePackages.ppxf.ppxf import ppxf
except:
    # Then use system installed version instead
    from ppxf.ppxf import ppxf
import extinction


def workerPPXF(inQueue, outQueue):
    for templates, templates_cont_substracted, bin_data, noise, velscale, mask_ppxf, \
        adeg, mdeg, offset, velscale_ratio, moments, EBV_init, start, nbins, i, lam, regul, fixed, nsims in iter(
        inQueue.get, 'STOP'):
        velocity, sigma, bestfit, reddening, weights, weights_err, weights_mc, ext_curve, mpoly, apoly, chi2 = \
            run_ppxf(templates, templates_cont_substracted, bin_data, noise, velscale, mask_ppxf, \
                     adeg, mdeg, offset, velscale_ratio, moments, EBV_init, start, nbins, i, lam, regul, fixed, nsims)

        outQueue.put(
            (i, mask_ppxf, velocity, sigma, bestfit, reddening, weights, weights_err, weights_mc, ext_curve, mpoly, apoly, chi2))


def run_ppxf(templates, templates_cont_substracted, bin_data, noise, velscale, mask_ppxf, \
             adeg, mdeg, offset, velscale_ratio, moments, EBV_init, start, \
             nbins, i, lam, regul, fixed, nsims):
    pipeline.printProgress(i, nbins, barLength=50)
    

    try:
        median_data = np.median(bin_data)
        if median_data <=0:
            raise Exception
        galaxy_norm = bin_data / median_data
        noise_norm = noise / median_data
        

        # Call PPXF, 1st run using an extinction law, no polynomials
        pp = ppxf(templates, galaxy_norm, noise_norm, velscale, lam=lam, mask=mask_ppxf,
                  degree=-1, mdegree=-1, vsyst=offset, velscale_ratio=velscale_ratio,
                  moments=moments, start=start, plot=False, reddening=EBV_init, 
                  regul=regul, quiet=True, fixed=fixed)
        

        # Deredden spectrum
        EBV = pp.reddening
        Rv = 4.05
        Av = EBV * Rv
        galaxy_norm = extinction.remove(extinction.calzetti00(lam, Av, Rv), galaxy_norm)
        noise_norm = extinction.remove(extinction.calzetti00(lam, Av, Rv), noise_norm)
        ext_curve = extinction.apply(extinction.calzetti00(lam, Av, Rv), np.ones_like(galaxy_norm))

        # re-run pPXF on the dereddened spectrum using multiplicative polynomial
        start_poly = pp.sol
        fixed_poly = [True] * len(pp.sol)

        if not (templates == templates_cont_substracted).all(): #If templates and templates_cont_substracted are different, normalize the continuum of the observed spectra
            galaxy_norm_copy = np.where(~mask_ppxf, np.nan, galaxy_norm)
            continuum = normalize_spec_continuum(lam * u.AA, galaxy_norm_copy * u.Lsun, criteria=3,
                                                 nsig_low=2, nsig_up=8)
        else:
            continuum = np.ones_like(galaxy_norm)

        galaxy_norm = galaxy_norm / continuum
        noise_norm = noise_norm / continuum


        pp = ppxf(templates_cont_substracted, galaxy_norm, noise_norm, velscale, lam=lam, mask=mask_ppxf,
                  degree=adeg, mdegree=mdeg, vsyst=offset, velscale_ratio=velscale_ratio,
                  moments=moments, start=start_poly, plot=False, regul=regul, quiet=True, fixed=fixed_poly)

        weights_renorm = pp.weights * median_data
        weights_err = np.zeros_like(weights_renorm)
        weights_mc = np.array([weights_renorm])
        reddening_mean = EBV
        chi2 = pp.chi2
        mpoly = pp.mpoly
        if adeg > -1:
            apoly = pp.apoly
        else:
            apoly = np.zeros_like(ext_curve)

        # if no MCMC simulations required
        if nsims <= 1:
            return (pp.sol[0], pp.sol[1], pp.bestfit* median_data * continuum, reddening_mean, weights_renorm, weights_err, weights_mc, ext_curve, mpoly, apoly, chi2)

        # MC simulations with polynomial only.
        else:
            start_mc = pp.sol
            fixed_mc = [True] * len(pp.sol)
            bestfit = pp.bestfit
            weights_mc = np.zeros((nsims, len(templates[0])))
            for j in range(nsims):
                added_noise = np.random.normal(loc=0, scale=noise_norm)
                galaxy_norm_noisy = galaxy_norm + added_noise
                pp = ppxf(templates_cont_substracted, galaxy_norm_noisy, noise_norm, velscale, lam=lam, mask=mask_ppxf,
                          degree=adeg, mdegree=mdeg, vsyst=offset, velscale_ratio=velscale_ratio,
                        moments=moments, start=start_mc, plot=False, regul=0., quiet=True,
                        fixed=fixed_mc)
                weights_mc[j, :] = pp.weights

            weights_mc = weights_mc * median_data

            weights_err = np.std(weights_mc, axis=0)

            return (start_mc[0], start_mc[1], bestfit* median_data* continuum, reddening_mean, weights_renorm, weights_err, weights_mc, ext_curve, mpoly, apoly, chi2)
    except:
        return(np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan)

def save_ppxf_SFH(rootname, outdir, velocity, sigma, bestfit, reddening, weights, weights_err, weights_mc, ext_curve,
                  masks, mpoly, apoly, chi2, npix, ubins):
    # ========================
    # SAVE RESULTS
    outfits_ppxf = outdir + rootname + '_ppxf_SFH.fits'
    pipeline.prettyOutput_Running("Writing: " + outfits_ppxf.split('/')[-1])

    # Primary HDU
    priHDU = fits.PrimaryHDU()

    # Table HDU with PPXF output data
    cols = []
    cols.append(fits.Column(name='BIN_ID', format='J', array=ubins))
    cols.append(fits.Column(name='V_STARS', format='D', array=velocity))
    cols.append(fits.Column(name='SIGMA_STARS', format='D', array=sigma))
    cols.append(fits.Column(name='REDDENING', format='D', array=reddening))
    cols.append(fits.Column(name='CHI2_SSP', format='D', array=chi2))
    weights_T = weights.T
    weights_err_T = weights_err.T
    for i in range(len(weights_T)):
        name = 'w_' + str(i)
        cols.append((fits.Column(name=name, format='D', array=weights_T[i])))
    for i in range(len(weights_T)):
        name = 'ew_' + str(i)
        cols.append((fits.Column(name=name, format='D', array=weights_err_T[i])))

    dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    dataHDU.name = 'SFH_DATA'

    # Create HDU list and write to file
    HDUList = fits.HDUList([priHDU, dataHDU])
    HDUList.writeto(outfits_ppxf, overwrite=True)

    pipeline.prettyOutput_Done("Writing: " + outfits_ppxf.split('/')[-1])
    logging.info("Wrote: " + outfits_ppxf)
    # ========================
    # SAVE MC RESULTS
    outfits_ppxf = outdir + rootname + '_ppxf_mc_results-SFH.fits'
    pipeline.prettyOutput_Running("Writing: " + outfits_ppxf.split('/')[-1])

    # Primary HDU
    priHDU = fits.PrimaryHDU()

    nweights = weights.shape[1]
    nsims = weights_mc.shape[1]
    data = [[] for k in range(nweights + 1)]  # Store nweights + 1 columns (weights + bin_id)
    for i in range(len(ubins)):
        weights_mc_i = weights_mc[i]
        data[0].extend([ubins[i]] * nsims)

        for j in range(len(weights_mc_i.T)):
            weights_mc_ij = weights_mc_i.T[j]
            data[j + 1].extend(weights_mc_ij)
    data = np.array(data)
    cols = []
    cols.append(fits.Column(name='BIN_ID', format='J', array=data[0]))
    for i in range(1, len(data)):
        name = 'w_' + str(i - 1)
        cols.append(fits.Column(name=name, format='D', array=data[i]))
    dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    dataHDU.name = 'MC_RESULTS'

    # Create HDU list and write to file
    HDUList = fits.HDUList([priHDU, dataHDU])
    HDUList.writeto(outfits_ppxf, overwrite=True)

    pipeline.prettyOutput_Done("Writing: " + outfits_ppxf.split('/')[-1])
    logging.info("Wrote: " + outfits_ppxf)

    # ========================
    # SAVE BESTFIT
    outfits_ppxf = outdir + rootname + '_ppxf-bestfit-SFH.fits'
    pipeline.prettyOutput_Running("Writing: " + outfits_ppxf.split('/')[-1])

    # Primary HDU
    priHDU = fits.PrimaryHDU()

    # Table HDU with PPXF bestfit
    cols = []
    cols.append(fits.Column(name='BIN_ID', format='J', array=ubins))
    cols.append(fits.Column(name='BESTFIT', format=str(bestfit.shape[1]) + 'D', array=bestfit))
    cols.append(fits.Column(name='EXT_CURVE', format=str(ext_curve.shape[1]) + 'D', array=ext_curve))
    cols.append(fits.Column(name='MPOLY', format=str(mpoly.shape[1]) + 'D', array=mpoly))
    cols.append(fits.Column(name='APOLY', format=str(apoly.shape[1]) + 'D', array=apoly))
    cols.append(fits.Column(name='MASK', format=str(masks.shape[1]) + 'D', array=masks))
    dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    dataHDU.name = 'FIT'

    # Create HDU list and write to file
    HDUList = fits.HDUList([priHDU, dataHDU])
    HDUList.writeto(outfits_ppxf, overwrite=True)

    pipeline.prettyOutput_Done("Writing: " + outfits_ppxf.split('/')[-1])
    logging.info("Wrote: " + outfits_ppxf)

def normalize_spec_continuum(wave, flux, criteria = 3, nsig_low = 2, nsig_up = 10):
    #criteria = 1: Do the clipping based on the std of the residuals -> Iterate removing pixels with residuals < nsig_low*sig or >nsig_up*sig
    #criteria = 2: Do the clipping removing pixels in which the normalized flux is lower than a given threshold (0.98)
    #criteria = 3: Do 2, and also the >nsig_up*sig. So, cut downwards  using normalized spectrum, and cut upwards using the residuals
    from astropy import units as u
    from specutils.spectra import Spectrum1D, SpectralRegion
    from specutils.fitting import fit_generic_continuum
    from astropy.modeling import models

    flux_copy = copy.deepcopy(flux)

    for ii in range(50):
        spectrum = Spectrum1D(flux=flux_copy, spectral_axis=wave)
        cont_fitter = fit_generic_continuum(spectrum, model=models.Polynomial1D(degree=5),
                                            exclude_regions=[SpectralRegion(6720 * u.AA, 6735 * u.AA),
                                                             SpectralRegion(4760 * u.AA, 4770 * u.AA),
                                                             SpectralRegion(4800 * u.AA, 4900 * u.AA),
                                                             SpectralRegion(5405 * u.AA, 5420 * u.AA),
                                                             SpectralRegion(5870 * u.AA, 5885 * u.AA),
                                                             SpectralRegion(6540 * u.AA, 6600 * u.AA)])
        continuum_fitted = cont_fitter(wave)

        residual = flux_copy - continuum_fitted
        sigma_res = np.nanstd(residual)

        normed_flux = flux_copy.value / continuum_fitted.value

        if criteria == 1:
            flux_copy = np.where(np.logical_or(residual < - nsig_low * sigma_res, residual > nsig_up * sigma_res), np.nan, flux_copy)
            if len( np.where(np.logical_or(residual < - nsig_low * sigma_res, residual > nsig_up * sigma_res))[0]) == 0:
                #print('convergence at ' + str(ii))
                break

        elif criteria == 2:
            flux_copy = np.where(normed_flux <= 0.98, np.nan, flux_copy)

            if len(np.where(normed_flux <= 0.98)[0]) == 0:
                #print('convergence at ' + str(ii))
                break
            #if ii == 49:
                #print('Not converged!')
        elif criteria == 3:
            flux_copy = np.where(np.logical_or(normed_flux <= 0.98, residual > nsig_up * sigma_res),
                                 np.nan, flux_copy)
            if len(np.where(np.logical_or(normed_flux <= 0.98, residual > nsig_up * sigma_res))[0]) == 0:
                #print('convergence at ' + str(ii))
                break

        else:
            raise ValueError('Not Valid critera option!')

    return continuum_fitted.value


# maybe change name to run module stellar kinematics
def runModule_PPXF_stellarpops(configs, logLam, log_spec, log_error, LSF, bin_id=None, ppxf_results=None):
    
    print("\033[0;37m" + " - - - - - Running Stellar Population Analysis - - - - - " + "\033[0;39m")
    logging.info(" - - - Running Stellar Population Analysis - - - ")

    #--> some bookkeeping
    if log_spec.ndim==1:
        log_error= np.expand_dims(log_error, axis=1)
        log_spec= np.expand_dims(log_spec, axis=1)
    nbins = log_spec.shape[1]
    ubins = np.arange(0, nbins)
    npix_in = log_spec.shape[0]
    n_spaxels_per_bin = np.zeros(nbins)
    # number of spaxels per bin
    for i in range(nbins):
        windx = (bin_id == i)
        n_spaxels_per_bin[i] = np.sum(windx)
    velscale_ratio = 1
    # check if wavelength is in vacuum
    if 'WAV_VACUUM' in configs:
        wav_in_vacuum = configs['WAV_VACUUM']
    else:   
        wav_in_vacuum = False
    if bin_id is None:
        bin_id = ubins
    velscale = (logLam[1]-logLam[0])*constants.c.to('km/s').value
    # no regularisation
    configs['REGUL']=0

    # regenerate the LSF interpolation function for simplicity
    LSF_InterpolationFunction = interp1d(np.exp(logLam) / (1 + configs['REDSHIFT'] / constants.c.to('km/s').value), LSF,
                                         'linear', fill_value='extrapolate')
    # the wav range of the data (observed)
    LamRange = (np.exp(logLam[0]), np.exp(logLam[-1]))

    # --> generate the stellar templates
    templates_info = util_templates.prepare_sp_templates_SFH \
        (configs, velscale, velscale_ratio, LSF_InterpolationFunction, LamRange,  wav_in_vacuum)
    star_templates, lamRange_spmod, logLam_template, min_wav_fit, max_wav_fit, ntemplates = \
        templates_info['templates'], templates_info['WavRange'], templates_info['logLam'], \
        templates_info['min_wav_to_fit'], templates_info['max_wav_to_fit'], templates_info['templates'].shape[1]
    templates = np.column_stack([star_templates])
    median_templates = templates_info['median_templates']

    # --> cut the spectra to the wavelength range of the templates
    logLam_cut, log_spec_cut, log_error_cut, npix, mask_for_original_spectra, wav_cov_templates = \
        util_templates.cut_spectra_to_match_templates(logLam, log_spec, log_error, max_wav_fit, min_wav_fit)

    lam = np.exp(logLam_cut)
    nsims = 1 if configs['MC_PPXF_SFH'] == 0 else configs['MC_PPXF_SFH']

    # --> Define goodpixels
    skyranges_file = configs['SKY_LINES_RANGES']
    wav_mask_ppxf, _  = util_templates.get_Mask('SFH', configs['EMI_FILE'], configs['REDSHIFT'], velscale, 
         logLam_cut, log_error_cut, sky_file=skyranges_file, wav_in_vacuum=wav_in_vacuum, masking_width=configs['MASK_WIDTH'])
   
    # merge with the spectral coverage mask
    mask_for_original_spectra [wav_cov_templates] =wav_mask_ppxf
    # get the per-pixel mask by combining the wavelength mask with the info on the Err array
    mask_pixels = util_templates.get_pixel_mask(wav_mask_ppxf, log_error_cut)
    # finally transform to boolean for input to ppxf
    mask_pixels = mask_pixels.astype(bool)

    if 'REMOVE_CONT' in configs:
        substract_cont=bool(configs['REMOVE_CONT'])
    else:
        substract_cont=False
    if substract_cont:
        templates_cont_substracted = np.zeros_like(templates)
        for jj in range(ntemplates):
            flux_temp = np.where(templates[:, jj] == 0, np.nan, templates[:, jj])
            flux_temp[np.where(~np.isnan(flux_temp))[0][:5]] = np.nan
            flux_temp[np.where(~np.isnan(flux_temp))[0][-5:]] = np.nan
            # continuum_1 = normalize_spec_continuum(np.e ** templates_info['logLam'] * u.AA, flux_temp * u.Lsun, criteria = 1, nsig_low = 2, nsig_up = 10)
            # continuum_2 = normalize_spec_continuum(np.e ** templates_info['logLam'] * u.AA, flux_temp * u.Lsun, criteria=2)
            continuum_3 = normalize_spec_continuum(np.e ** templates_info['logLam'] * u.AA, flux_temp * u.Lsun,
                                                   criteria=3,
                                                   nsig_low=2, nsig_up=10)

            templates_cont_substracted[:, jj] = templates[:, jj] / continuum_3
    else:
        templates_cont_substracted = copy.deepcopy(templates)



    # --> define the systemic velocity due to template and data starting wavelength offset
    # NOTE: this is only correct if velscale ==1!!
    offset = ((logLam_template[0] - logLam_cut[0]) + np.log(1 + \
        configs['REDSHIFT'] / constants.c.to('km/s').value)) * constants.c.to('km/s').value

    EBV_init = 0.1
    mdeg = configs['MDEG_SFH']
    adeg = configs['ADEG_SFH']
    fixed = [ppxf_results is not None] * configs['MOM']

    if ppxf_results is not None:
        logging.info('SFH: Stellar kinematics are FIXED to the results of PPXF (ST_KIN)')

    # --> generate the arrays necessary to run ppxf with emission line and the desired
    # components, bounds, starting points and tied paramters
    # for now the number of gas moments is fixed to 2 (i.e. v and sigma, no h3 and h4 etc for gas)
    # configs['GAS_MOMENTS'] = 2
    # these are the guess stellar kinematics, just placeholder for now
    # stellar_kinematics = np.copy(ppxf_results[0, :])
    # nstmom = ppxf_results.shape[1]

    # tpl_comp, moments, start, bounds, tied, gas_comp = \
    #    util_templates.define_emission_line_input_for_ppxf(configs, nstpl, ngastpl, eml_tying, emldb,
    #                                                       stellar_kinematics, fixed_st_kin=True)

    # n_gas_comp = len(np.unique(tpl_comp[gas_comp]))
    # n_gas_templates = len(tpl_comp[gas_comp])

    # Array to store results of ppxf
    # gas_kinematics = np.zeros((nbins, n_gas_comp, configs['GAS_MOMENTS'])) + np.nan
    # kinematics_all_err = np.zeros((nbins, n_gas_comp, configs['GAS_MOMENTS'])) + np.nan
    velocity = np.zeros((nbins))
    sigma = np.zeros((nbins))
    reddening = np.zeros((nbins))
    chi2 = np.zeros((nbins))
    bestfit = np.zeros((nbins, npix))
    weights = np.zeros((nbins, ntemplates))
    weights_err = np.zeros((nbins, ntemplates))
    weights_mc = np.zeros((nbins, nsims, ntemplates))
    ext_curve = np.zeros((nbins, npix))
    masks = np.ones((nbins, npix))

    mpoly = np.zeros((nbins, npix))
    apoly = np.zeros((nbins, npix))

    # ====================
    # Run PPXF
    start_time = time.time()

    if configs['PARALLEL'] == True:
        pipeline.prettyOutput_Running("Running PPXF for SFH analysis in parallel mode")
        logging.info("Running PPXF for SFH  analysis in parallel mode")

        # Create Queues
        inQueue = Queue()
        outQueue = Queue()

        # Create worker processes
        ps = [Process(target=workerPPXF, args=(inQueue, outQueue))
              for _ in range(configs['NCPU'])]

        # Start worker processes
        for p in ps: p.start()

        # Fill the queue
        for i in range(nbins):
            # this changes the stellar kinematics starting guess for each bin
            if ppxf_results is not None:
                start = ppxf_results[i, 0:configs['MOM']]
            else:
                start = [0, 100]
            inQueue.put((templates, templates_cont_substracted, log_spec_cut[:, i], log_error_cut[:, i], velscale,
                         mask_pixels[:, i], adeg, mdeg, offset, velscale_ratio,
                         configs['MOM'], EBV_init, start, nbins, i, lam, configs['REGUL'], fixed, nsims))

        # now get the results with indices
        ppxf_tmp = [outQueue.get() for _ in range(nbins)]

        # send stop signal to stop iteration
        for _ in range(configs['NCPU']): inQueue.put('STOP')

        # stop processes
        for p in ps: p.join()

        # Get output
        index = np.zeros(nbins)

        # i, velocity, sigma, bestfit, reddening, weights
        for i in range(0, nbins):
            index[i] = ppxf_tmp[i][0]
            masks[i] = ppxf_tmp[i][1]
            velocity[i] = ppxf_tmp[i][2]
            sigma[i] = ppxf_tmp[i][3]
            bestfit[i, :] = ppxf_tmp[i][4]
            reddening[i] = ppxf_tmp[i][5]
            weights[i, :] = ppxf_tmp[i][6]
            weights_err[i, :] = ppxf_tmp[i][7]
            weights_mc[i, :, :] = ppxf_tmp[i][8]
            ext_curve[i, :] = ppxf_tmp[i][9]
            mpoly[i, :] = ppxf_tmp[i][10]
            apoly[i, :] = ppxf_tmp[i][11]
            chi2[i] = ppxf_tmp[i][12]

        # Sort output
        argidx = np.argsort(index)
        velocity = velocity[argidx]
        sigma = sigma[argidx]
        bestfit = bestfit[argidx, :]
        reddening = reddening[argidx]
        weights = weights[argidx, :]
        weights_err = weights_err[argidx, :]
        weights_mc = weights_mc[argidx, :, :]
        ext_curve = ext_curve[argidx, :]
        masks = masks[argidx, :]

        mpoly = mpoly[argidx, :]
        apoly = apoly[argidx, :]
        chi2 = chi2[argidx]

        pipeline.prettyOutput_Done("Running PPXF in parallel mode", progressbar=True)


    elif configs['PARALLEL'] == False:
        pipeline.prettyOutput_Running("Running PPXF in serial mode")
        logging.info("Running PPXF in serial mode")
        for i in range(0, nbins):
            if ppxf_results is not None:
                start = ppxf_results[i, 0:configs['MOM']]
            else:
                start = [0, 100]

            velocity[i], sigma[i], bestfit[i, :], reddening[i], weights[i, :], weights_err[i, :], \
                weights_mc[i, :,:], ext_curve[i,:], mpoly[i,:], apoly[i,:], chi2[i] = run_ppxf(
                templates, templates_cont_substracted, log_spec_cut[:, i],
                log_error_cut[:, i], velscale,
                mask_pixels[:, i], adeg, mdeg,
                offset, velscale_ratio,
                configs['MOM'], EBV_init,
                start, nbins, i, lam, configs['REGUL'], fixed, nsims)
            masks[i]=mask_pixels[:, i]

        pipeline.prettyOutput_Done("Running PPXF in serial mode", progressbar=True)

    print("             Running PPXF on %s spectra took %.2fs" % (nbins, time.time() - start_time))
    #print("")
    logging.info(
        "Running PPXF on %s spectra took %.2fs using %i cores" % (nbins, time.time() - start_time, configs['NCPU']))

    # this step is necessary since the util_tempaltes function returns the templates 
    #dividied my median(termplates), which is useful for intpu to ppxf
    weights = weights / median_templates
    weights_err = weights_err / median_templates
    weights_mc = weights_mc / median_templates
   
   
    if 'SPECTRUM_SIZE' in configs:
        spaxel_size = configs['SPECTRUM_SIZE']
    else:
        wcs = util_prepare.load_WCS(configs)
        spaxel_size = abs(wcs['CD1_1'][0]) * 3600.  # spaxel size in arcsec

    try:
        wcs = util_prepare.load_WCS(configs)
        flux_unit = wcs['FUNIT'][0]  # factor to convert flux in units of erg/s/cm2/AA
         # Put everything in units of L/(Lsun*AA)
        Lsun = u.solLum.to(u.erg / u.s)  # Solar luminosity in erg/s
        pc_to_cm = ((1 * u.pc).to(u.cm)).value  # from pc to cm
        spaxel_angle = spaxel_size * u.arcsec
        spaxel_size_rad = spaxel_angle.to(u.rad).value
        conversion_factor = flux_unit * 4 * np.pi * (pc_to_cm ** 2) / spaxel_size_rad ** 2 / Lsun
        weights = weights * conversion_factor
        weights_err = weights_err * conversion_factor
        weights_mc = weights_mc * conversion_factor
        print('calculating in flux units')
    except:
        print('no rescaling')
        pass

    # add back the part of the spectrum that was truncated because of lack of templates
    bestfit_1 = np.zeros((nbins, npix_in))
    bestfit_1[:, wav_cov_templates] = bestfit

    ext_curve_1 = np.zeros((nbins, npix_in))
    ext_curve_1[:, wav_cov_templates] = ext_curve

    masks_1 = np.ones((nbins, npix_in))
    w0 = masks ==0
    w1= masks ==1
    masks[w0]=1
    masks[w1]=0
    masks_1[:, wav_cov_templates] = masks

    mpoly_1 = np.zeros((nbins, npix_in))
    mpoly_1[:, wav_cov_templates] = mpoly

    apoly_1 = np.zeros((nbins, npix_in))
    apoly_1[:, wav_cov_templates] = apoly

    # save results to file

    save_ppxf_SFH(configs['ROOTNAME'], configs['OUTDIR'], velocity, sigma, bestfit_1, reddening, weights, weights_err,
                  weights_mc, ext_curve_1, masks_1, mpoly_1, apoly_1, chi2, npix,ubins)


    print("\033[0;37m" + " - - - - - Stellar Population Analysis done! - - - - -" + "\033[0;39m")
    #print("")
    logging.info(" - - - Stellar Population Analysis done! - - - \n")
