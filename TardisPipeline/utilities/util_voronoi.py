from pyexpat.errors import XML_ERROR_NOT_SUSPENDED
from   astropy.io             import fits
import numpy                  as np
import scipy.spatial.distance as dist

import logging
import importlib
from  TardisPipeline.utilities import util as pipeline
import pdb

# my version of Voronoi 2D binning avoids getting stuck in the loop
try:
    from TardisPipeline.sitePackages.voronoi_2d_binning import voronoi_2d_binning
except:
    from vorbin.voronoi_2d_binning import voronoi_2d_binning
    logging.info("Using the original Cappellari Voronoi binning procedure, beware this may cause the binning to fail")



def define_voronoi_bins(configs, tasks, x, y, signal, noise, pixelsize, snr,spx_class_sn, wcs, mode):

    # Generate the Voronoi bins
    pipeline.prettyOutput_Running("Defining the Voronoi bins")
    logging.info("Defining the Voronoi bins")
    if mode not in ['kinematics','sfh']:
        raise ValueError('mode MUST be either "kinematics" or "sfh"')
    if mode == 'kinematics':
        targetSN = configs['TARGET_SNR']
    elif mode == 'sfh':
        targetSN = configs['TARGET_SNR_SFH']

    # from the instrument specific file read the S/N conversion function
    instrument = importlib.import_module('.readData.'+configs['IFU'], package='TardisPipeline')
    sn_func = instrument.sn_func

    ##### This is just to be sure that even in a case when all pixels have enought S/N, the pipeline is not going to crash
    if len(np.where(signal[spx_class_sn['INSIDE']]/ noise[spx_class_sn['INSIDE']] < targetSN)[0]) > 0:
        binNum, xNode, yNode, _, _, sn, nPixels, _ = voronoi_2d_binning(x[spx_class_sn['INSIDE']], 
            y[spx_class_sn['INSIDE']],signal[spx_class_sn['INSIDE']], 
            noise[spx_class_sn['INSIDE']], targetSN, plot=0, quiet=1, 
            pixelsize=pixelsize, sn_func = sn_func)
    else:
        binNum = np.arange(0,len(x[spx_class_sn['INSIDE']]), 1)
        xNode = x[spx_class_sn['INSIDE']]
        yNode = y[spx_class_sn['INSIDE']]
        sn = signal[spx_class_sn['INSIDE']].data/ noise[spx_class_sn['INSIDE']].data
        nPixels =  np.ones_like(sn)


    pipeline.prettyOutput_Done("Defining the Voronoi bins")
    print("             "+str(np.max(binNum)+1)+" voronoi bins generated!")
    logging.info(str(np.max(binNum)+1)+" Voronoi bins generated!")

    # Find the nearest Voronoi bin for the pixels outside the Voronoi region
    #binNum_outside = find_nearest_voronoibin( x, y, spx_class_sn['OUTSIDE'], xNode, yNode )

    # Generate extended binNum-list: 
    #   Positive binNum indicate the Voronoi bin of the spaxel (for spaxels inside the Voronoi region)
    #   Negative binNum indicate the nearest Voronoi bin of the spaxel (for spaxels outside of the Voronoi region)
    binNum_long = np.zeros( len(x) ); binNum_long[:] = np.nan
    binNum_long[spx_class_sn['INSIDE']]  = binNum
    binNum_long[spx_class_sn['OUTSIDE']] = -1 #* binNum_outside

    # Save bintable: data for *ALL* spectra inside and outside of the Voronoi region!
    save_table(configs['ROOTNAME'], configs['OUTDIR'], x, y, signal, snr, binNum_long, \
        xNode, yNode, sn, nPixels, pixelsize, wcs)

    return(binNum,binNum_long)


def define_bins_from_file(configs, x, y, signal, noise,pixelsize, wcs, path_bin_map):

    # from the instrument specific file read the S/N conversion function
    instrument = importlib.import_module('.readData.'+configs['IFU'], package='TardisPipeline')
    sn_func = instrument.sn_func

    binMap = read_binning_map(path_bin_map, wcs)

    binNum_long = match_map_with_spaxel_list(binMap, x, y, pixelsize)
    binNum = binNum_long[binNum_long >= 0]
    # this is the total number of bins (remember bin numbers start from 0)
    ubins = np.int(np.max(binNum)+1)

    # all these quantities are at the bin level
    xNode = np.zeros( ubins )
    yNode = np.zeros( ubins )
    snBin= np.zeros( ubins )
    nPixels = np.zeros( ubins)
    for i in range(ubins):
        idx = (binNum_long==i)
        xNode[i] =np.mean(x[idx])
        yNode[i]=np.mean(y[idx])
        nPixels[i] =idx.sum()
        snBin[i] =sn_func(idx, signal, noise)

    save_table(configs['ROOTNAME'], configs['OUTDIR'], x, y, signal, signal/noise, binNum_long, \
        xNode, yNode, snBin, nPixels, pixelsize, wcs)

    return(binNum,binNum_long)


def find_nearest_voronoibin(x, y, idx_outside, xNode, yNode):
    x = x[idx_outside]
    y = y[idx_outside]
    pix_coords = np.concatenate( (x.reshape((len(x),1)),         y.reshape((len(y),1))),         axis=1 )
    bin_coords = np.concatenate( (xNode.reshape((len(xNode),1)), yNode.reshape((len(yNode),1))), axis=1 )

    dists = dist.cdist( pix_coords, bin_coords, 'euclidean' ) 
    closest = np.argmin( dists, axis=1 )

    return(closest)


def save_table(rootname, outdir, x, y, signal, snr, binNum_new, 
    xNode, yNode, snBin, nPixels, pixelsize=None, wcs=None, flag='stars'):
    
    
    ubins =np.unique(binNum_new[binNum_new>=0])
    if flag =='stars':
        outfits_table = outdir+rootname+'_table.fits'
    elif flag=='ems':
        outfits_table = outdir+rootname+'_table_ems.fits'
    pipeline.prettyOutput_Running("Writing: "+outfits_table)

    # Expand data to spaxel level
    xNode_new = np.zeros( len(x) )
    yNode_new = np.zeros( len(x) )
    sn_new = np.zeros( len(x) )
    nPixels_new = np.zeros( len(x) )
    for i in range( len(ubins) ):
        idx = np.where( ubins[i] == np.abs(binNum_new) )[0]
        xNode_new[idx] = xNode[i]
        yNode_new[idx] = yNode[i]
        sn_new[idx] = snBin[i]
        nPixels_new[idx] = nPixels[i]

    cols = []
    cols.append(fits.Column(name='ID',        format='J',   array=np.arange(len(x)) ))
    cols.append(fits.Column(name='BIN_ID',    format='J',   array=binNum_new        ))
    cols.append(fits.Column(name='X',         format='D',   array=x                 ))
    cols.append(fits.Column(name='Y',         format='D',   array=y                 ))
    cols.append(fits.Column(name='FLUX',      format='D',   array=signal            ))
    cols.append(fits.Column(name='SNR',       format='D',   array=snr               ))
    cols.append(fits.Column(name='XBIN',      format='D',   array=xNode_new         ))
    cols.append(fits.Column(name='YBIN',      format='D',   array=yNode_new         ))
    cols.append(fits.Column(name='SNRBIN',    format='D',   array=sn_new            ))
    cols.append(fits.Column(name='NSPAX',     format='J',   array=nPixels_new       ))
    tbhdu = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    hdr = tbhdu.header
    if wcs is not None:
        for key in wcs.keys():
            hdr.append( (key, wcs[key][0]) )
    if pixelsize is not None:
        tbhdu.header['PIXELSIZE'] = pixelsize
    tbhdu.writeto(outfits_table, overwrite=True)

    pipeline.prettyOutput_Done("Writing: "+outfits_table)
    logging.info("Wrote Voronoi table: "+outfits_table)


def apply_bins(configs, tasks, binNum, spec, espec, velscale, wave, LSF_interpol_funct, lin_log_flag):
 
    # Apply bins
    pipeline.prettyOutput_Running("Applying the Voronoi bins to "+lin_log_flag+"-data")
    # FB need to add processing of LSF 
    # from the instrument specific file read the S/N conversion function
    instrument = importlib.import_module('.readData.'+configs['IFU'], package='TardisPipeline')
    sn_func = instrument.sn_func

    bin_data, bin_error, bin_flux = sum_spectra_in_bins( binNum, spec, espec , sn_func)
    pipeline.prettyOutput_Done("Applying the Voronoi bins to "+lin_log_flag+"-data", progressbar=True)
    logging.info("Applied Voronoi bins to "+lin_log_flag+"-data")
        
    binned_cube={}
    binned_cube["log_spec"]=bin_data
    binned_cube["log_error"]=bin_error
    binned_cube["logLam"]=wave
    binned_cube["velscale"]=velscale
    binned_cube["LSF"] = LSF_interpol_funct(np.exp(wave))
    save_binspectra(configs['ROOTNAME'], configs['OUTDIR'], bin_data, bin_error, velscale, 
        wave, binned_cube["LSF"], lin_log_flag)
    
    return(binned_cube)



def sum_spectra_in_bins( binNum, spec, error , sn_func):
    ubins     = np.unique(binNum)
    nbins     = len(ubins)
    npix      = spec.shape[0]
    bin_data  = np.zeros([npix,nbins])
    bin_error = np.zeros([npix,nbins])
    bin_flux  = np.zeros(nbins)

    for i in range(nbins):
        k = np.where( binNum == ubins[i] )[0]
        valbin = len(k)
        if valbin == 1:
           av_spec     = spec[:,k]
           av_err_spec = error[:,k]
        else:
            # turn input spectra and error into masked arrays for ease of manipulation
            spec2 = np.ma.MaskedArray(spec[:,k], mask = (error[:,k] ==9999.))
            err2 = np.ma.MaskedArray(error[:,k], mask = (error[:,k] ==9999.))

            # get the average flux, taking the mask into account and summing over the spectra
            avg, sumweights = np.ma.average(spec2, axis=1, returned=True)
            # sum the error in quadrature
            err_avg = (np.ma.sum(err2**2, axis=1) )**0.5

            # find the faction of valid spectra contributing to each pixel
            good_weights = sumweights/valbin
            # the the summed spectra from the average
            avg = avg*valbin
            # correct the error for the missing pixels
            err_avg  = err_avg/ sumweights*valbin
            # correct the error for the potential effect of spatial covariance
            # in case of no covariance scaling =1
            scaling = 1/(sn_func(np.array(range(0, valbin)), np.ones(valbin), np.ones(valbin))/valbin**0.5)
            err_avg = err_avg*scaling

            av_spec = avg.data
            mask_pix = (err_avg.mask==True) | (good_weights<0.7)
            av_err_spec = err_avg.data
            av_err_spec[mask_pix] = 9999.
        
    
        bin_data[:,i]  = np.ravel(av_spec)
        bin_error[:,i] = np.ravel(av_err_spec)
        # take the average only of the non-masked pixels 
        wgood = (bin_error[:,i]!=9999.)
        bin_flux[i]    = np.mean(av_spec[wgood])
        pipeline.printProgress(i+1, nbins, barLength = 50)

    return(bin_data, bin_error, bin_flux)


def save_binspectra(rootname, outdir, log_spec, log_error, velscale, logLam, lsf, flag):
    if flag == 'log':
        outfits_spectra  = outdir+rootname+'_VorSpectra.fits'
    elif flag == 'lin':
        outfits_spectra  = outdir+rootname+'_VorSpectra_linear.fits'
    elif flag == 'ems':
        outfits_spectra  = outdir+rootname+'_VorSpectra_ems.fits'

    pipeline.prettyOutput_Running("Writing: "+outfits_spectra)
    npix = len(log_spec)

    # Create primary HDU
    priHDU = fits.PrimaryHDU()

    # Table HDU for spectra
    cols = []
    cols.append( fits.Column(name='SPEC',  format=str(npix)+'D', array=log_spec.T  ))
    cols.append( fits.Column(name='ESPEC', format=str(npix)+'D', array=log_error.T ))
    dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    dataHDU.name = 'VOR_SPECTRA'

    # Table HDU for LOGLAM
    cols = []
    cols.append( fits.Column(name='LOGLAM', format='D', array=logLam ))
    cols.append( fits.Column(name='LSF', format='D', array=lsf ))
    loglamHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
    loglamHDU.name = 'LOGLAM'

    # Create HDU list and save to file
    HDUList = fits.HDUList([priHDU, dataHDU, loglamHDU])
    HDUList.writeto(outfits_spectra, overwrite=True)

    # Set header values
    fits.setval(outfits_spectra,'VELSCALE',value=velscale)
    fits.setval(outfits_spectra,'CRPIX1',  value=1.0)
    fits.setval(outfits_spectra,'CRVAL1',  value=logLam[0])
    fits.setval(outfits_spectra,'CDELT1',  value=logLam[1]-logLam[0])

    pipeline.prettyOutput_Done("Writing: "+outfits_spectra)
    logging.info("Wrote: "+outfits_spectra)

def load_binspectra(configs, flag='log', SFH = False):
    outdir = configs['OUTDIR']
    rootname = configs['ROOTNAME']
    sfh = '_SFH' if SFH else ''
    if flag == 'log':
        hdu      = fits.open(outdir+rootname+'_VorSpectra'+sfh+'.fits')
    elif flag == 'lin':
        hdu      = fits.open(outdir+rootname+'_VorSpectra'+sfh+'_linear.fits')
    elif flag == 'ems':
        hdu      = fits.open(outdir+rootname+'_VorSpectra_'+sfh+'ems.fits')
    
    log_spec = np.array( hdu['VOR_SPECTRA'].data.SPEC.T )
    log_error = np.array( hdu['VOR_SPECTRA'].data.ESPEC.T )
    logLam   = np.array( hdu['LOGLAM'].data.LOGLAM )
    lsf   = np.array( hdu['LOGLAM'].data.LSF )
    velscale = float(hdu[0].header['VELSCALE'])

    binned_cube={}
    binned_cube["log_spec"]=log_spec
    binned_cube["log_error"]=log_error
    binned_cube["logLam"]=logLam
    binned_cube["velscale"]=velscale
    binned_cube["LSF"] = lsf

    return(binned_cube)

def load_binning_table(configs, flag='stars'):
    if flag == 'stars':
        binNum = np.array( fits.open(configs['OUTDIR']+configs['ROOTNAME']+'_table.fits')[1].data.BIN_ID )
    if flag == 'ems':
        binNum = np.array( fits.open(configs['OUTDIR']+configs['ROOTNAME']+'_table_ems.fits')[1].data.BIN_ID )

    binNum = binNum[np.where(binNum >= 0)[0]]

    return(binNum)

def read_binning_map(path_bin_map, wcs):
    BinMap = fits.open(path_bin_map)[0].data
    assert len(np.unique(BinMap))-1 == np.max(BinMap)+1 , \
        'Binning map is not constituted by a set of bins 0 .. N'
    npixels_x = int(wcs['NAXIS1'][0])
    npixels_y = int(wcs['NAXIS2'][0])
    assert (BinMap.shape[1] ==npixels_x) & (BinMap.shape[0] ==npixels_y), \
        'Binning map provided does not match the dimensions of the datacube'
    return(BinMap)

def match_map_with_spaxel_list(BinMap, x, y,pixelsize):

    i_ = np.array(np.round(y / pixelsize), dtype=np.int)
    j_ = np.array(np.round(x / pixelsize), dtype=np.int)  
    BinMap_long = BinMap[i_, j_]

    uniq_bins = np.unique(BinMap_long[BinMap_long>=0])
    nbins_surviving = len(uniq_bins)
    nbins_max= np.max(BinMap_long)+1
    
    # note that in this step it is possible that not all the bins have survived
    if nbins_surviving !=nbins_max:
        logging.info("Some of the Bins in the provided BinMap did not survive because they correspond to area with no/bad spectra")
        
        for ii, bin in enumerate(uniq_bins):
            ww = BinMap_long==bin
            BinMap_long[ww]=ii

    return(BinMap_long)
