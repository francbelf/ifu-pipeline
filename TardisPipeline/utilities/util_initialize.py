import numpy as np
from astropy.io import fits, ascii
import os
import sys
import shutil
import multiprocessing
import logging
import pdb
import configparser
from astropy import constants
import logging
import time

from TardisPipeline.utilities import util as pipeline
import TardisPipeline as tardis_module


def readConfig(path='Config.ini'):
    '''
    Read the configuration file using configparser

    Parametres:
        path (str): path to Config.ini

    Returns:
        configs (dict): configs dictionary
    '''
    try:
        custom = configparser.ConfigParser()
        custom.optionxform = lambda option: option
        custom.read(path)
        configs = dict(custom['Config']) 
        return(configs)
    
    except:
        message = "Config file could not be found"
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
    

def readGalaxies(configs):
    '''
    Read the galaxy list

    Parametres:
        path (str): path to galaxy_list

    Returns:
        galaxy (Table.table): table of galaxy, sys_vel, 
    '''
    try:
        galaxy = ascii.read(configs['GALAXY_LIST'])
        
        ncols = len(galaxy.colnames)
        galaxy['col1'].name='NAME'
        galaxy['col2'].name='REDSHIFT'
        if configs['SYS_VEL']!='1':
            galaxy['REDSHIFT']*=constants.c.to('km/s').value
        if (ncols ==3):
             galaxy['col3'].name='EBmV'
        else:
            galaxy['EBmV']=0
        return(galaxy)
    except:
        message = "galaxy list file could not be read"
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
    

def setupConfig(galnumber, path='Config', verbose=True):
    '''
    Set up the config file in full and the directory structure for the run on one galaxy

    Parameters:
        path (str): path to Config.ini file
        galnumber (int): index of the galaxy to fit

    Returns:
        configs (dict): config dictionary
        tasks (dict): tasks dictionary
    '''
    # read the configuration file for this run
    configs = readConfig(path)
    # read the list of galaxies
    galaxies = readGalaxies(configs)
    # consider one galaxy
    galaxy = galaxies[galnumber]

    #-------- DEFINE KEY DIRECTORIES --------
    configs['CODEDIR'] = os.path.dirname(os.path.realpath(tardis_module.__file__))
    configs['ROOTNAME'] = galaxy['NAME']
    configs['EBmV'] = galaxy['EBmV']
    configs['REDSHIFT'] = galaxy['REDSHIFT']
    # run_name = <galaxy>_<run_ID>
    configs['RUN_NAME'] = configs['ROOTNAME']+'_'+str(configs['RUN_ID'])
    # this is the path of the input .fits datacubes
    configs['DATAFILE'] = configs['DATADIR'] + configs['ROOTNAME'] + '/' + configs['ROOTNAME'] + '.fits'
    # path of the output directory for this run
    configs['OUTDIR'] = configs['DATADIR']  + configs['ROOTNAME'] + '/' + configs['RUN_NAME'] + '/'
    # generate output directory
    if os.path.isdir(configs['OUTDIR']) == False:
        os.mkdir(configs['OUTDIR'])

    #-------- SETUP LOG FILE --------
    setupLog(configs)

    #-------- CONFIGURATION FILES --------
    configs['NCPU'] = int(configs['NCPU'])
    if configs['NCPU'] > 1:
        configs['PARALLEL'] = True
    else:
        configs['PARALLEL'] = False
    configs['DEBUG'] = bool(int(configs['DEBUG']))

    #-------- READ DATA --------
    configs['LMIN'] = float(configs['LMIN'])
    configs['LMAX'] = float(configs['LMAX'])

    #-------- TASKS --------
    configs['BINNING'] = int(configs['BINNING'])
    configs['EMS_LINES'] = int(configs['EMS_LINES'])
    configs['SFH'] = int(configs['SFH'])

    #-------- BINNING PARAMETERS --------   
    configs['SN_WAV'] = [float(configs['SN_WAV'].split(',')[0]), float(configs['SN_WAV'].split(',')[1])]
    configs['TARGET_SNR'] = float(configs['TARGET_SNR'])
    configs['MIN_SNR'] = float(configs['MIN_SNR'])
    if (configs['BINNING']==1):
        configs['PATH_BIN_MAP'] = None
    elif (configs['BINNING']==2):
        if 'PATH_BIN_MAP' not in configs.keys() :
            configs['PATH_BIN_MAP'] = configs['DATADIR'] + configs['ROOTNAME'] + '/' + configs['ROOTNAME'] + '_binning_map.fits'
        elif configs['PATH_BIN_MAP'] is None:
            configs['PATH_BIN_MAP'] = configs['DATADIR'] + configs['ROOTNAME'] + '/' + configs['ROOTNAME'] + '_binning_map.fits'
        else:
            configs['PATH_BIN_MAP'] = configs['PATH_BIN_MAP']+ configs['ROOTNAME'] + '_binning_map.fits'


    #-------- STELLAR KINEMATICS PARAMETERS --------
    if ('SSP_LIB' in configs.keys()) & (configs['SSP_LIB']!='None'):
        configs['SSP_LIB'] = configs['CODEDIR'] + '/Templates/spectralTemplates/' + \
         configs['SSP_LIB'] + '/'
    else:
        configs['SSP_LIB'] = configs['CODEDIR'] + '/Templates/spectralTemplates/eMILES-noyoung/'
    if ('MOM' in configs.keys()) & (configs['MOM']!='None'):
        configs['MOM'] = int(configs['MOM'])
    else: 
        configs['MOM'] = 2
    if ('ADEG' in configs.keys()) & (configs['ADEG']!='None'):
        configs['ADEG'] = int(configs['ADEG'])
    else: 
        configs['ADEG'] = 8
    if ('MDEG' in configs.keys()) & (configs['MDEG']!='None'):
        configs['MDEG'] = int(configs['MDEG'])
    else: 
        configs['MDEG'] = 0
    if 'MC_PPXF' in configs.keys():
        configs['MC_PPXF'] = int(configs['MC_PPXF'])
    else: 
        configs['MC_PPXF'] = 0
    if 'MC_PPXF_SFH' in configs.keys():
        configs['MC_PPXF_SFH'] = int(configs['MC_PPXF_SFH'])
    else: 
        configs['MC_PPXF_SFH'] = 20
    
    #-------- EMISSION LINE FITTING PARAMETERS --------
    if ('SSP_LIB_EMS' in configs.keys()) & (configs['SSP_LIB_EMS']!='None'):
        configs['SSP_LIB_EMS'] = configs['CODEDIR'] + '/Templates/spectralTemplates/' + \
         configs['SSP_LIB_EMS'] + '/'
    else:
        configs['SSP_LIB_EMS'] = configs['SSP_LIB'] 
    
    configs['EBmV'] = float(configs['EBmV'])
   
    if ('EMI_FILE' in configs.keys()) & (configs['EMI_FILE']!='None'):
        configs['EMI_FILE'] = configs['CODEDIR'] + '/Templates/configurationTemplates/' + \
         configs['EMI_FILE'] 
    else:
        configs['EMI_FILE'] = configs['CODEDIR'] + '/Templates/configurationTemplates/emission_lines.setup'

    #-------- STELLAR POPULATION ANALYSIS PARAMETERS --------
    if ('SSP_LIB_SFH' in configs.keys()) & (configs['SSP_LIB_SFH']!='None'):
        configs['SSP_LIB_SFH'] = configs['CODEDIR'] + '/Templates/spectralTemplates/' + \
         configs['SSP_LIB_SFH'] + '/'
    else:
        configs['SSP_LIB_SFH'] = configs['CODEDIR'] + '/Templates/spectralTemplates/eMILES-Ch-stpops/'

    if ('SKY_LINES_RANGES' in configs.keys()) & (configs['SKY_LINES_RANGES']!='None'):
        configs['SKY_LINES_RANGES'] = configs['CODEDIR'] + '/Templates/configurationTemplates/' + \
         configs['SKY_LINES_RANGES'] 
    else:
        configs['SKY_LINES_RANGES'] = configs['CODEDIR'] + '/Templates/configurationTemplates/sky_lines_ranges.setup'

    if 'MDEG_SFH' in configs.keys():
        configs['MDEG_SFH'] = int(configs['MDEG_SFH'])
    else:
        configs['MDEG_SFH'] = 0

    if 'ADEG_SFH' in configs.keys():
        configs['ADEG_SFH'] = int(configs['ADEG_SFH'])
    else:
        configs['ADEG_SFH'] = -1
    
    if 'MC_PPXF_SFH' in configs.keys():
        configs['MC_PPXF_SFH'] = int(configs['MC_PPXF_SFH'])
    else:
        configs['MC_PPXF_SFH'] = 20
    
    # copy the configuration file used to the output directory
    shutil.copyfile(path, configs['OUTDIR'] + 'Config')

    tasks = {}
    tasks['SKIP_GALAXY'] = False

    # ----- Check that all configuration and input files are in place
    #  check that input fits datacube exists
    if os.path.isfile(configs['DATAFILE']) == False:
        message = configs['DATAFILE'] + ' does not exist. This is a fatal error! Galaxy will be skipped.'
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        tasks['SKIP_GALAXY'] = True
    # no emission-line file in the output directory
    if os.path.isfile(configs['OUTDIR'] + 'emission_lines.setup') == False:
        shutil.copyfile(configs['EMI_FILE'], configs['OUTDIR'] + 'emission_lines.setup')

    if configs['BINNING'] not in [1, 2]:
        message = "BINNING has to be either 1 (Voronoi) or 2 (From File). Using Voronoi."
        print(pipeline.prettyOutput_WarningPrefix() + message)
        logging.warning(message)
        configs['BINNING'] = 1
    if configs['EMS_LINES'] not in [0, 1, 2]:
        message = "EMS_LINES has to be either 0 (Off), 1 (BIN level) or 2 (SPAXEL level). Emission line fitter will be disabled. Continue."
        print(pipeline.prettyOutput_WarningPrefix() + message)
        logging.warning(message)
        configs['EMS_LINES'] = 0
    if configs['SFH'] not in [0, 1]:
        message = "SFH has to be either 0 (No) or 1 (Yes). SFH will be disabled. Continue."
        print(pipeline.prettyOutput_WarningPrefix() + message)
        logging.warning(message)
        configs['SFH'] = 0

    #  check that the binning map exists
    if configs['BINNING']==2:
        if os.path.isfile(configs['PATH_BIN_MAP']) == False:
            message = configs['PATH_BIN_MAP'] + ' does not exist. Change binning strategy'
            print(pipeline.prettyOutput_FailedPrefix() + message)
            logging.error(message)
            tasks['SKIP_GALAXY'] = True


    if configs['NCPU'] > multiprocessing.cpu_count():
        message = "The chosen number of CPU's seems to be higher than the number of cores in the system! Continue."
        print(pipeline.prettyOutput_WarningPrefix() + message)
        logging.warning(message)
    # is this still necessary ? hardcoded in the ppxf wrapper??
    if configs['MOM'] > 4:
        message = "Maximum number of LSVD moments is fixed at 4, continuming with MOM =4."
        print(pipeline.prettyOutput_WarningPrefix() + message)
        configs['MOM'] = int(4)
        logging.warning(message)

    if os.path.isfile(configs['CODEDIR'] + '/readData/' + configs['IFU'] + '.py') == False:
        message = "No read-in routine at 'readData/" + configs[
            'IFU'] + ".py' found. "+configs['IFU']+" is not a supported IFU. This is a fatal error! Galaxy will be skipped."
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        tasks['SKIP_GALAXY'] = True

    if configs['LMIN'] > configs['LMAX']:
        message = "The given minimum wavelength LMIN is larger than the maximum wavelength LMAX. I will swap them! Continue."
        print(pipeline.prettyOutput_WarningPrefix() + message)
        logging.warning(message)
        lmin = configs['LMAX']
        lmax = configs['LMIN']
        configs['LMIN'] = lmin
        configs['LMAX'] = lmax

    if os.path.isdir(configs['SSP_LIB']) == False:
        message = "No spectral template library found in " + configs[
            'SSP_LIB'] + ". This is a fatal error!"
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        tasks['SKIP_GALAXY'] = True
    if os.path.isdir(configs['SSP_LIB_EMS']) == False:
        message = "No spectral template library found in " + configs[
            'SSP_LIB_EMS'] + ". This is a fatal error!"
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        tasks['SKIP_GALAXY'] = True
    if os.path.isdir(configs['SSP_LIB_SFH']) == False:
        message = "No spectral template library found in " + configs[
            'SSP_LIB_SFH'] + ". This is a fatal error!"
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        tasks['SKIP_GALAXY'] = True
    if os.path.isfile(configs['SSP_LIB_SFH']+'templates_info.txt') == False:
        message = "Templates for stellar population fitting needs a templates_info.txt file . This is a fatal error!"
        print(pipeline.prettyOutput_FailedPrefix() + message)
        logging.error(message)
        tasks['SKIP_GALAXY'] = True

    # Print configurations on screen
    if verbose==True:
        printConfigs_Configs(configs)

    return (configs, tasks)


def determine_tasks(configs, tasks, verbose=True):
   
    # LOG-REBIN SPECTRA
    if os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_AllSpectra.fits') == True:
        tasks['LOG_REBIN_SPECTRA'] = False
    else:
        tasks['LOG_REBIN_SPECTRA'] = True

    # DEFINE BINS
    if os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_table.fits') == True:
        tasks['DEFINE_BINS'] = 0
    elif configs['BINNING']==1:
        tasks['DEFINE_BINS'] = 1
    elif configs['BINNING']==2:
        tasks['DEFINE_BINS'] = 2

    # APPLY  BINS 
    if os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_VorSpectra.fits') == True:
        tasks['APPLY_BINS'] = False
    else:
        tasks['APPLY_BINS'] = True

    # stellar kinematics
    if os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf.fits') == True:
        tasks['PPXF_ST_KIN'] = False
    else:
        tasks['PPXF_ST_KIN'] = True

    # emission lines
    if os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_emlines.fits') == True:
        tasks['EMS_LINES'] = 0  # False
    elif os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_emlines.fits') == False and configs[
        'EMS_LINES'] == 1:
        tasks['EMS_LINES'] = 1  # BIN level
    elif os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_emlines.fits') == False and configs[
        'EMS_LINES'] == 2:
        tasks['EMS_LINES'] = 2  # SPAXEL level
    elif os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_emlines.fits') == False and configs[
        'EMS_LINES'] == 3:
        tasks['EMS_LINES'] = 3  # Ems BIN level
        if os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_table_ems.fits') == True and \
                os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_VorSpectra_ems.fits') == True:
            tasks['REVORONOI_LINES'] = 0
        else:
            tasks['REVORONOI_LINES'] = 1
    else:
        tasks['EMS_LINES'] = 0  # False

    # SFH                            
    # #Check if SFH is required, if the SN and ranges are different to those of st_kine and if the files already exists
    if configs['SFH'] == 1:  # SFH requested
        tasks['SFH'] = True
        # these only exists in case one day you may want to define a different binning scheme for stellar pops
        # currently not implemented
        tasks['APPLY_BINS_SFH'] = False
        tasks['DEFINE_BINS_SFH'] = False

        if os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf_SFH.fits') == True:
            tasks['PPXF_SFH'] = False
        else:
            tasks['PPXF_SFH'] = True

    else:  # SFH not requested
        tasks['SFH'] = False
        tasks['APPLY_BINS_SFH'] = False
        tasks['DEFINE_BINS_SFH'] = False
        tasks['PPXF_SFH'] = False

    # produce final maps
    if os.path.isfile(configs['OUTDIR'] + configs['ROOTNAME'] + '_MAPS.fits') == False:
        tasks['MAPS'] = True
    else:
        tasks['MAPS'] = False

    # IN CASE EVERYTHING IS ALREADY DONE
    if tasks['PPXF_ST_KIN'] == False and tasks['EMS_LINES'] == 0 and \
            tasks['SFH'] == False and \
            tasks['MAPS'] == False:
        message = "There is nothing to do for " + str(configs['RUN_NAME']) + ". Galaxy will be skipped!"
        print(pipeline.prettyOutput_WarningPrefix() + message)
        logging.warning(message)
        tasks['SKIP_GALAXY'] = True
    if verbose==True:
        printConfigs_Tasks(configs, tasks)

    return (tasks)


def printConfigs_Configs(configs):
    os.system('clear')
    # print("")
    # print("\033[0;37m" + "************************************************************" + "\033[0;39m")
    # print("\033[0;37m" + "*                   THE AWESOME PIPELINE                   *" + "\033[0;39m")
    # print("\033[0;37m" + "************************************************************" + "\033[0;39m")
    # print("")
    print("\033[0;37m" + "Pipeline runs with the following settings:" + "\033[0;39m")
    print(    "   * Datadir:           " + str(configs['DATADIR']))
    print(    "   * Run Name:          " + str(configs['RUN_NAME']))
    if bool(int(configs['PARALLEL'])) == True:
        print("   * NCPU:               " + str(int(configs['NCPU'])))
    print("")
    print("   * Wavelength Range:   " + str(configs['LMIN']) + " - " + str(configs['LMAX']) + "/AA")
    print("   * Redshift:     " + str(configs['REDSHIFT']) + " km/s")
    print("   * Target SNR (ST_KIN):" + str(configs['TARGET_SNR']))
    print("   * Min. SNR:           " + str(configs['MIN_SNR']))
    print("   * Wav. range for S/N:  " + str(configs['SN_WAV']) + " /AA")
    print("")
    print("\033[0;37m" + "Template Libraries:" + "\033[0;39m")
    print("   * SSP library (ST_KIN):" + str(configs['SSP_LIB']))
    print("  * SSP library (EMS):        " + str(configs['SSP_LIB_EMS']))
    print("  * SSP library (SFH):        " + str(configs['SSP_LIB_SFH']))
    print("")
    print("\033[0;37m" + "PPXF settings:" + "\033[0;39m")
    print("   * Gauss-Hermite Mom.: " + str(configs['MOM']))
    print("   * Add. polyn. Degree (ST_KIN): " + str(configs['ADEG']))
    print("   * Mul. polyn. Degree (ST_KIN): " + str(configs['MDEG']))
    print("   * Num. of MC Sims. (ST_KIN):   " + str(configs['MC_PPXF']))
    print("")
    print("\033[0;37m" + "EMS settings:" + "\033[0;39m")
    print("   * Galactic foreground E(B-V):             " + str(configs['EBmV']))
    print("")
    print("\033[0;37m" + "PPXF_SFH settings:" + "\033[0;39m")
    print("   * Mul. polyn. Degree (SFH) :    " + str(configs['MDEG_SFH']))
    print("   * Add. polyn. Degree (SFH) :    " + str(configs['ADEG_SFH']))
    print("   * Num. of MC Sims. (SFH):   " + str(configs['MC_PPXF_SFH']))
    print("")
    print("\033[0;37m" + "************************************************************" + "\033[0;39m")
    print("")


def printConfigs_Tasks(configs, tasks):
    # print("")
    print("\033[0;37m" + "Recipes to run:" + "\033[0;39m")

    if tasks['DEFINE_BINS'] == 0:
        print("   * BINNING:            False")
    elif tasks['DEFINE_BINS'] == 1:
        print("   * BINNING:            VORONOI")
    elif tasks['DEFINE_BINS'] == 2:
        print("   * BINNING:            FROM BINNING MAP")


    print(    "   * STELLAR KINEMATICS:               " + str(tasks['PPXF_ST_KIN']))
    if tasks['EMS_LINES'] == 0:
        print("   * EMISSING LINES FITTING:           False")
    elif tasks['EMS_LINES'] == 1:
        print("   * EMISSING LINES FITTING:           BIN level")
    elif tasks['EMS_LINES'] == 2:
        print("   * EMISSING LINES FITTING:           SPAXEL level")
    elif tasks['EMS_LINES'] == 3:
        print("   * EMISSING LINES FITTING:           EMS BIN level")
    print(    "   * STELLAR POPULATION ANALYSIS:      " + str(tasks['PPXF_SFH']))
    # print("")
    print("\033[0;37m" + "************************************************************" + "\033[0;39m")
    # print("")
    print("")
    if configs['DEBUG'] == True:
        print(pipeline.prettyOutput_WarningPrefix() + "RUNNING IN DEBUG MODE!")
        print("             Remember to clean output directory afterwards!")
        print("")
        print("")

def setupLog(configs):
    # Setup logfile
    for handler in logging.root.handlers[:]: logging.root.removeHandler(handler)
    logging.basicConfig(filename= configs['OUTDIR']+'LOGFILE',
                        level   = logging.INFO,
                        format  = '%(asctime)s - %(levelname)-8s - %(module)s: %(message)s',
                        datefmt = '%m/%d/%y %H:%M:%S' )
    logging.Formatter.converter = time.gmtime    
    logging.info("\n\n# ================================================\n"\
            +"#   STARTUP OF PIPELINE\n# "+time.strftime("  %d-%b-%Y %H:%M:%S %Z", time.gmtime())+\
            "\n# ================================================")
