#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 14 18:09:27 2019

@author: fbelfior
"""

from curses import KEY_BREAK
import matplotlib.pyplot as plt
from astropy.io import fits
import numpy as np
from astropy import table
from scipy import interpolate
import logging
import copy
import TardisPipeline
from TardisPipeline.utilities import util as pipeline
from TardisPipeline.utilities import util_sfh_quantities
from astropy.table import Table


def mapify_all(configs, tasks, wcs):
    # read WCS
    # wcs = table.Table.read(configs['OUTDIR']+configs['ROOTNAME']+'_wcs.fits')
    npixels_x = int(wcs['NAXIS1'][0])
    npixels_y = int(wcs['NAXIS2'][0])
    pixelsize = float(abs(wcs['CD1_1'][0])) * 3600

    # read the voronoi bin table output to save
    hdu_voronoi = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_table.fits')
    vorbin_data = hdu_voronoi[1].data
    idxMap = np.where(vorbin_data.NSPAX >= 0)[0]

    i_ = np.array(np.round((vorbin_data.Y[idxMap]) / pixelsize), dtype=int)
    j_ = np.array(np.round((vorbin_data.X[idxMap]) / pixelsize), dtype=int)
    # Primary HDU
    priHDU = fits.PrimaryHDU()

    hdr0 = fits.Header()

    # add the WCS keywords to header
    hdr0['CD1_1'] = float(wcs['CD1_1'][0])
    hdr0['CD2_2'] = float(wcs['CD2_2'][0])
    hdr0['CRVAL1'] = float(wcs['CRVAL1'][0])
    hdr0['CRVAL2'] = float(wcs['CRVAL2'][0])
    hdr0['CRPIX1'] = float(wcs['CRPIX1'][0])
    hdr0['CRPIX2'] = float(wcs['CRPIX2'][0])
    hdr0['CUNIT1'] = str(wcs['CUNIT1'][0])
    hdr0['CUNIT2'] = str(wcs['CUNIT2'][0])
    hdr0['CTYPE1'] = 'RA---TAN'
    hdr0['CTYPE2'] = 'DEC--TAN'
    hdr0['AUTHOR'] = 'Francesco Belfiore <francesco.belfiore@inaf.it>, Ismael Pessa'

    cols = []
    cols.append(priHDU)

    # Write output for the Voronoi bin output
    fields = ['ID', 'FLUX', 'SNR', 'SNRBIN', 'BIN_ID']

    for kk in fields:
        val = vorbin_data[kk][idxMap]
        if kk == 'BIN_ID':
            image = np.full((npixels_y, npixels_x), -1.)
        else:
            image = np.full((npixels_y, npixels_x), np.nan)
        image[i_, j_] = val

        if kk == 'FLUX':
            hdr0['BUNIT'] = str(wcs['FUNIT'][0]) + ' erg/s/cm2/AA/spaxel'

        cols.append(fits.ImageHDU(data=image, name=kk, header=hdr0))

        if kk == 'FLUX':
            del hdr0['BUNIT']

            # now only consider values that have bins
    idxMap = np.where((vorbin_data.NSPAX >= 0) & (vorbin_data.BIN_ID >= 0))[0]

    i_ = np.array(np.round((vorbin_data.Y[idxMap]) / pixelsize), dtype=int)
    j_ = np.array(np.round((vorbin_data.X[idxMap]) / pixelsize), dtype=int)

    # Write output for the ppxf stellar kinematics output
    hdu_ppxf_stkin = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf.fits')
    ppxf_stkin_data = hdu_ppxf_stkin[1].data

    if configs['MC_PPXF']==0:
        fields = ['V', 'FORM_ERR_V', 'SIGMA', 'FORM_ERR_SIGMA']
        if 'H3' in ppxf_stkin_data.names:
            fields = fields + ['H3', 'FORM_ERR_H3']
        if 'H4' in ppxf_stkin_data.names:
            fields = fields + ['H4', 'FORM_ERR_H4']

        vorbin_id = vorbin_data.BIN_ID[idxMap]
        image_vor_bin = np.full((npixels_y, npixels_x), np.nan)
        image_vor_bin[i_, j_] = vorbin_id

        for kk in fields:
            quantity = np.full((npixels_y, npixels_x), np.nan)
            for i in range(len(np.unique(vorbin_id))):
                wbin = (image_vor_bin == i)
                quantity[wbin] = ppxf_stkin_data[kk][i]

            if kk in ['V', 'FORM_ERR_V', 'SIGMA', 'FORM_ERR_SIGMA']:
                hdr0['BUNIT'] = 'km/s'

            cols.append(fits.ImageHDU(data=quantity, name=kk + '_STARS', header=hdr0))

            if kk in ['V', 'FORM_ERR_V', 'SIGMA', 'FORM_ERR_SIGMA']:
                del hdr0['BUNIT']
    elif configs['MC_PPXF']>0:
        fields = ['V', 'ERR_V', 'FORM_ERR_V', 'SIGMA', 'ERR_SIGMA', 'FORM_ERR_SIGMA']
        if 'H3' in ppxf_stkin_data.names:
            fields = fields + ['H3', 'ERR_H3', 'FORM_ERR_H3']
        if 'H4' in ppxf_stkin_data.names:
            fields = fields + ['H4', 'ERR_H4', 'FORM_ERR_H4']

        vorbin_id = vorbin_data.BIN_ID[idxMap]
        image_vor_bin = np.full((npixels_y, npixels_x), np.nan)
        image_vor_bin[i_, j_] = vorbin_id

        for kk in fields:
            quantity = np.full((npixels_y, npixels_x), np.nan)
            for i in range(len(np.unique(vorbin_id))):
                wbin = (image_vor_bin == i)
                quantity[wbin] = ppxf_stkin_data[kk][i]

            if kk in ['V', 'ERR_V', 'FORM_ERR_V', 'SIGMA', 'ERR_SIGMA', 'FORM_ERR_SIGMA']:
                hdr0['BUNIT'] = 'km/s'

            cols.append(fits.ImageHDU(data=quantity, name=kk + '_STARS', header=hdr0))

            if kk in ['V', 'ERR_V', 'FORM_ERR_V', 'SIGMA', 'ERR_SIGMA', 'FORM_ERR_SIGMA']:
                del hdr0['BUNIT']

    if configs['EMS_LINES'] > 0:
        # Write output for the emission lines fitting
        hdu_ppxf_ems = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_emlines.fits')
        hdr1 = copy.copy(hdr0)
        hdr1['BUNIT'] = str(wcs['FUNIT'][0]) + ' erg/s/cm2/spaxel'
        hdr2 = copy.copy(hdr0)
        hdr2['BUNIT'] = 'km/s'

    if configs['EMS_LINES'] == 1:
        ems_data = hdu_ppxf_ems[1].data
        fields = ems_data.names
        fields.remove('V_STARS2')
        fields.remove('SIGMA_STARS2')
    

        for kk in fields:
            quantity = np.full((npixels_y, npixels_x), np.nan)
            for i in range(len(np.unique(vorbin_id))):
                wbin = (image_vor_bin == i)
                quantity[wbin] = ems_data[kk][i]

            # get the correct BUNIT in the headers of the different quantities
            if (kk == 'BIN_ID') | (kk=='CHI2_TOT'): 
                if kk == 'BIN_ID': kk = 'BIN_ID_LINES'
                hdrv1 = copy.copy(hdr1)
                print(kk)
                del hdrv1['BUNIT']
                cols.append(fits.ImageHDU(data=quantity, name=kk, header=hdrv1))
            elif (kk.split('_')[1] == 'VEL') | (kk.split('_')[1] == 'SIGMA'):
                cols.append(fits.ImageHDU(data=quantity, name=kk, header=hdr2))
            else:
                cols.append(fits.ImageHDU(data=quantity, name=kk, header=hdr1))

           


    elif configs['EMS_LINES'] == 2:
        idxMap2 = np.where((vorbin_data.NSPAX >= 0) )[0]

        ii_ = np.array(np.round((vorbin_data.Y[idxMap2]) / pixelsize), dtype=int)
        jj_ = np.array(np.round((vorbin_data.X[idxMap2]) / pixelsize), dtype=int)

        ems_data = hdu_ppxf_ems[1].data
        fields = ems_data.names
        fields.remove('V_STARS2')
        fields.remove('SIGMA_STARS2')

        bin_id_ems = ems_data.BIN_ID
        image_ems_bin = np.full((npixels_y, npixels_x), np.nan)
        image_ems_bin[ii_, jj_] = bin_id_ems

        
        for kk in fields:

            val = ems_data[kk]
            #rename for output files
            
            image = np.full((npixels_y, npixels_x), np.nan)
            image[ii_, jj_] = val
            # get the correct BUNIT in the headers of the different quantities
            if (kk == 'BIN_ID_LINES') | (kk=='CHI2_TOT'):
                if kk == 'BIN_ID': kk = 'BIN_ID_LINES'
                hdrv1 = copy.copy(hdr1)
                del hdrv1['BUNIT']
                cols.append(fits.ImageHDU(data=image, name=kk, header=hdrv1))
            elif (kk.split('_')[1] == 'VEL') | (kk.split('_')[1] == 'SIGMA'):
                cols.append(fits.ImageHDU(data=image, name=kk, header=hdr2))
            else:
                cols.append(fits.ImageHDU(data=image, name=kk, header=hdr1))

    # elif configs['EMS_LINES'] == 3:
    #     ems_data = hdu_ppxf_ems[1].data
    #     fields = ems_data.names

    #     hdu_voronoi = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_table_ems.fits')
    #     vorbin_data_ems = hdu_voronoi[1].data
    #     vorbin_id = vorbin_data_ems.BIN_ID

    #     i_ = np.array(np.round((vorbin_data_ems.Y) / pixelsize), dtype=int)
    #     j_ = np.array(np.round((vorbin_data_ems.X) / pixelsize), dtype=int)
    #     image_vor_bin_ems = np.full((npixels_y, npixels_x), np.nan)
    #     image_vor_bin_ems[i_, j_] = vorbin_id

    #     for kk in fields:
    #         quantity = np.full((npixels_y, npixels_x), np.nan)
    #         for i in range(len(np.unique(vorbin_id))):
    #             wbin = (image_vor_bin_ems == i)
    #             quantity[wbin] = ems_data[kk][i]
    #         if kk == 'BIN_ID': kk = 'BIN_ID_LINES'
    #         cols.append(fits.ImageHDU(data=quantity, name=kk, header=hdr1))

    # Write output for the ppxf SFH output
    if configs['SFH'] == 1:
        hdu_ppxf_sfh = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf_SFH.fits')
        ppxf_sfh_data = hdu_ppxf_sfh[1].data
        masses_density, mass_density_err, ages_mw, ages_mw_err, z_mw, z_mw_err, \
        ages_lw, ages_lw_err, z_lw, z_lw_err = util_sfh_quantities.compute_sfh_relevant_quantities(configs)
        reddening = ppxf_sfh_data['REDDENING']

        chi2_ssp = ppxf_sfh_data['CHI2_SSP']

        fields_names = ['STELLAR_MASS_DENSITY', 'STELLAR_MASS_DENSITY_ERR', 'AGE_MW', 'AGE_MW_ERR', 'Z_MW', 'Z_MW_ERR', \
                        'AGE_LW', 'AGE_LW_ERR', 'Z_LW', 'Z_LW_ERR', 'EBV_STARS', 'CHI2_SSP']
        unit_names = ['M_sun/pc2', 'M_sun/pc2', 'log(Age/yr)', 'log(Age/yr) dex', '[Z/H] dex', 'dex', 'log(Age/yr)',
                      'log(Age/yr) dex', '[Z/H] dex', 'dex', 'mag', '-']
        fields_data = [masses_density, mass_density_err, ages_mw, ages_mw_err, z_mw, z_mw_err, \
                       ages_lw, ages_lw_err, z_lw, z_lw_err, reddening, chi2_ssp]
        nfields = len(fields_names)
        hdr2 = copy.copy(hdr0)

        vorbin_id = vorbin_data.BIN_ID[idxMap]
        image_vor_bin = np.full((npixels_y, npixels_x), np.nan)
        image_vor_bin[i_, j_] = vorbin_id

        for j in range(nfields):
            quantity = np.full((npixels_y, npixels_x), np.nan)
            for i in range(len(np.unique(vorbin_id))):
                wbin = (image_vor_bin == i)
                quantity[wbin] = fields_data[j][i]

            hdr2['BUNIT'] = unit_names[j]
            cols.append(fits.ImageHDU(data=quantity, name=fields_names[j], header=hdr2))

    maps_file = configs['OUTDIR'] + configs['ROOTNAME'] + '_MAPS.fits'
    cols[0].header['DAPVER'] = TardisPipeline.__version__
    cols[0].header.comments["DAPVER"] = 'version of the Data Analysis pipeline'
    relevant_keywords = ['RUN_NAME','REDSHIFT','EBmV','EMS_LINES','SFH','IFU', 
        'LMIN','LMAX','SN_WAV','TARGET_SNR','MIN_SNR','MOM','ADEG','MDEG','SSP_LIB','MC_PPXF',
        'SSP_LIB_EMS',  'ADEG_SFH', 'MDEG_SFH','SSP_LIB_SFH','MC_PPXF_SFH',]
    new_names =['RUN', 'VSYS', 'GALEBV', 'EMSTYP', 'SFHTYP','IFU', 
        'LMIN', 'LMAX', 'SNWAV', 'TSNR', 'MSNR', 'KMOM1','ADEG1', 'MDEG1', 'SSPLIB1', 'MC1',
         'SSPLIB2', 'ADEG3', 'MDEG3', 'SSPLIB3', 'MC3']
    comment = ['name of object and run', 'systemic velocity', 'galactic EBV applied', 
        'emission line analysis, 0:off, 1: bins, 2:spaxels',
         'SFH analysis, 0:off, 1: bins', 'instrument class', 'minimum fitted wavelength', 
         'maximum fitted wavelength', 'wavelength interval for S/N computations', 
         'target S/N', 'minimum S/N for considering spectrum', 'number of kinematic moments (stars)',
         'Additive polynomial order (stars)', 'Multiplicative polynomial order (stars)', 
         'Stellar templated used (stars)', 'number of MCMC runs (stars)',
         'Stellar templates used (emission lines)',  'Additive polynomial order (SFH)',
         'Multiplicative polynomial order (SFH)',
         'Stellar templates used (SFH)','number of MCMC runs (SFH)']
    for ii, key in enumerate(relevant_keywords):
        if (key =='SSP_LIB') | (key =='SSP_LIB_EMS')| (key =='SSP_LIB_SFH'):
            cols[0].header[new_names[ii]] = str(configs[key].split('/')[-2])
            cols[0].header.comments[new_names[ii]] = comment[ii]
        else:
            cols[0].header[new_names[ii]] = str(configs[key])
            cols[0].header.comments[new_names[ii]] = comment[ii]
    cols[0].header.add_blank('DATA ANALYSIS PIPELINE SETTINGS:', after='EXTEND')
    
    fits.HDUList(cols).writeto(maps_file, overwrite=True)

    pipeline.prettyOutput_Done("Writing: " + maps_file)
    logging.info("Wrote: " + maps_file)

    # residual cube
    # only compute residual cube if emission lines are fitted spaxel by spaxel
    if configs['EMS_LINES'] == 2:
    
        hdu2 = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_AllSpectra.fits')
        hdu4 = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_ppxf-bestfit-emlines.fits')
        flux_spaxels = hdu2['SPECTRA'].data['SPEC'][idxMap2, :]

        
        tot_bestfit = hdu4['FIT'].data['BESTFIT']
        # this is the stellar continuum best fit alone
        gas_bestift = hdu4['FIT'].data['BESTFIT'] - hdu4['FIT'].data['GAS_BESTFIT']

        nwave = flux_spaxels.shape[1]
        cube_1pass = np.full((nwave, npixels_y, npixels_x), np.nan)
        cube_1pass[:, ii_, jj_] = (flux_spaxels - gas_bestift).T

        cube_2pass = np.full((nwave, npixels_y, npixels_x), np.nan)
        cube_2pass[:, ii_, jj_] = (flux_spaxels - tot_bestfit).T
        # %%
        hdr_cube = copy.copy(hdr0)
        # add the header keyword for the logarithmic wavelength sampling
        loglam = hdu2['LOGLAM'].data['LOGLAM'] 
        hdr_cube['CTYPE3'] = 'AWAV-LOG'
        hdr_cube['CRPIX3']=1
        hdr_cube['NAXIS3']=len(loglam)
        hdr_cube['CRVAL3']=np.exp(loglam[0])
        hdr_cube['CDELT3']=(loglam[1]-loglam[0])*np.exp(loglam[0])
        hdr_cube['CUNIT3'] = 'Angstrom'
        hdr_cube['FUNIT'] = (wcs['FUNIT'][0], ' erg/s/cm2/AA/spaxel')
        
        cols = []
        priHDU = fits.PrimaryHDU()
        cols.append(priHDU)
        cols.append(fits.ImageHDU(data=cube_1pass, name='RESIDUAL_LINES', header=hdr_cube))
        cols.append(fits.ImageHDU(data=cube_2pass, name='RESIDUAL', header=hdr_cube))

        residual_cube_file = configs['OUTDIR'] + configs['ROOTNAME'] + '_RESIDUAL_CUBE.fits'
        #need the output_verify = fix to fix the order of the header keywords after having added the relevant ones in the wrong place
        fits.HDUList(cols).writeto(residual_cube_file, overwrite=True, output_verify='fix')


def mapify_weights(configs, wcs, map_id, age_top=None, age_low=None, z_top=None, z_low=None):
    outdir = configs['OUTDIR']
    rootname = configs['ROOTNAME']
    tab_templates = Table.read(outdir + rootname + '_templates_SFH_info.fits')
    age_top, age_low, z_top, z_low = util_sfh_quantities.define_limits(age_top, age_low, z_top, z_low, tab_templates)
    npixels_x = int(wcs['NAXIS1'][0])
    npixels_y = int(wcs['NAXIS2'][0])
    pixelsize = float(abs(wcs['CD1_1'][0])) * 3600

    # read the voronoi bin table output to save
    hdu_voronoi = fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_table.fits')
    vorbin_data = hdu_voronoi[1].data
    idxMap = np.where((vorbin_data.NSPAX >= 0) & (vorbin_data.BIN_ID >= 0))[0]

    i_ = np.array(np.round((vorbin_data.Y[idxMap]) / pixelsize), dtype=int)
    j_ = np.array(np.round((vorbin_data.X[idxMap]) / pixelsize), dtype=int)
    # Primary HDU
    priHDU = fits.PrimaryHDU()
    priHDU.header['age_top'] = str(age_top)
    priHDU.header['age_low'] = str(age_low)
    priHDU.header['z_top'] = str(z_top)
    priHDU.header['z_low'] = str(z_low)

    hdr0 = fits.Header()

    # add the WCS keywords to header
    hdr0['CD1_1'] = float(wcs['CD1_1'][0])
    hdr0['CD2_2'] = float(wcs['CD2_2'][0])
    hdr0['CRVAL1'] = float(wcs['CRVAL1'][0])
    hdr0['CRVAL2'] = float(wcs['CRVAL2'][0])
    hdr0['CRPIX1'] = float(wcs['CRPIX1'][0])
    hdr0['CRPIX2'] = float(wcs['CRPIX2'][0])
    hdr0['CUNIT1'] = str(wcs['CUNIT1'][0])
    hdr0['CUNIT2'] = str(wcs['CUNIT2'][0])
    hdr0['CTYPE1'] = 'RA---TAN'
    hdr0['CTYPE2'] = 'DEC--TAN'
    hdr0['AUTHOR'] = 'Francesco Belfiore <francesco.belfiore@inaf.it>, Ismael Pessa'

    cols = []
    cols.append(priHDU)
    frac_light, frac_light_err, frac_mass, frac_mass_err = util_sfh_quantities.compute_sfh_light_mass_frac(configs,
                                                                                                           age_top=age_top,
                                                                                                           age_low=age_low,
                                                                                                           z_top=z_top,
                                                                                                           z_low=z_low)
    field_names = ['light_fraction', 'light_fraction_err', 'mass_fraction', 'mass_fraction_err']
    field_data = [frac_light, frac_light_err, frac_mass, frac_mass_err]
    nfields = len(field_names)

    vorbin_id = vorbin_data.BIN_ID[idxMap]
    image_vor_bin = np.full((npixels_y, npixels_x), np.nan)
    image_vor_bin[i_, j_] = vorbin_id

    for j in range(nfields):
        quantity = np.full((npixels_y, npixels_x), np.nan)
        for i in range(len(np.unique(vorbin_id))):
            wbin = (image_vor_bin == i)
            quantity[wbin] = field_data[j][i]
        cols.append(fits.ImageHDU(data=quantity, name=field_names[j], header=hdr0))

    maps_file = configs['OUTDIR'] + configs['ROOTNAME'] + '_' + str(map_id) + '_FRACTION_MAPS.fits'
    fits.HDUList(cols).writeto(maps_file, overwrite=True)

    pipeline.prettyOutput_Done("Writing: " + maps_file)
    logging.info("Wrote: " + maps_file)
