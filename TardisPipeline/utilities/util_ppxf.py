import numpy    as np
from astropy.io import fits
from multiprocessing import Queue, Process
from  scipy.interpolate import interp1d
from astropy import constants

import time
import logging
import pdb
import importlib
from  TardisPipeline.utilities import util as pipeline
from  TardisPipeline.utilities  import util_prepare
from  TardisPipeline.utilities  import util_templates


try:
    # Try to use local version in sitePackages
    from sitePackages.ppxf.ppxf import ppxf
except: 
    # Then use system installed version instead
    from ppxf.ppxf import ppxf

cvel = constants.c.to('km/s').value

def workerPPXF(inQueue, outQueue):
    for templates, bin_data, noise, velscale, start, mask_ppxf, nmoments,\
        adeg, mdeg, offset, velscale_ratio, nsims, nbins, i \
        in iter(inQueue.get, 'STOP'):

        sol, bestfit, optimal_template, mc_results, formal_error, chi2 = run_ppxf(templates, \
            bin_data, noise, velscale, start, mask_ppxf, nmoments,\
            adeg, mdeg, offset, velscale_ratio, nsims, nbins, i)

        outQueue.put(( i, sol, bestfit, optimal_template, mc_results, formal_error, chi2 ))


def run_ppxf( templates, log_bin_data, log_bin_error, velscale, start, mask_ppxf, nmoments, adeg, mdeg,\
        offset, velscale_ratio, nsims, nbins, i):

    pipeline.printProgress( i, nbins, barLength=50 )

    try:

        # Call PPXF
        pp = ppxf(templates, log_bin_data, log_bin_error, velscale, start, mask=mask_ppxf, plot=False, \
                  quiet=True, moments=nmoments, degree=adeg, mdegree=mdeg, velscale_ratio=velscale_ratio, vsyst=offset)
        

        # Make the unconvolved optimal STELLAR template
        normalized_weights = pp.weights / np.sum( pp.weights )
        optimal_template   = np.zeros( templates.shape[0] )
        for j in range(0, templates.shape[1]):
            optimal_template = optimal_template + templates[:,j]*normalized_weights[j]
        
        # Correct the formal errors assuming that the fit is good
        formal_error = pp.error * np.sqrt(pp.chi2)
        chi2 = pp.chi2
    
        # Do MC-Simulations
        sol_MC     = np.zeros((nsims,nmoments)); sol_MC[:,:] = np.nan
        mc_results = np.zeros(nmoments);         mc_results  = np.nan
    
        # # Use for MC simulations optimal template only if OPT_TEMP is True
        # if OPT_TEMP in [1,2]: templates = optimal_template
    
        for o in range(0, nsims):
            # Add noise to bestfit: 
            #   - Draw random numbers from normal distribution with mean of 0 and sigma of 1 (np.random.normal(0,1,npix)
            #   - standard deviation( (galaxy spectrum - bestfit)[goodpix] )
            noisy_bestfit = pp.bestfit  +  np.random.normal(0, 1, len(log_bin_data)) * np.std( log_bin_data[mask_ppxf] - pp.bestfit[mask_ppxf] )
    
            mc = ppxf(templates, noisy_bestfit, log_bin_error, velscale, start, mask=mask_ppxf, plot=False, \
                    quiet=True, moments=nmoments, degree=adeg, mdegree=mdeg, velscale_ratio=velscale_ratio, vsyst=offset, bias=0.0)
            sol_MC[o,:] = mc.sol[:]
     
        if nsims != 0:
            mc_results = np.nanstd( sol_MC, axis=0 )
     
        return(pp.sol[:], pp.bestfit, optimal_template, mc_results, formal_error, chi2)
    except:
#        pdb.set_trace()
        logger=logging.getLogger()
        logger.exception("Fatal error in fitting galaxy")
        #raise Exception('Problem in i = '+str(i))
        return( np.nan, np.nan, np.nan, np.nan, np.nan, np.nan )



def save_ppxf(rootname, outdir, ppxf_result, mc_results, formal_error, chi2, \
              ppxf_bestfit, mask_for_original_spectra, optimal_template, logLam_template, npix, ubins):
   
        # ========================
        # SAVE RESULTS
        outfits_ppxf = outdir+rootname+'_ppxf.fits'
        pipeline.prettyOutput_Running("Writing: "+outfits_ppxf.split('/')[-1])
    
        # Primary HDU
        priHDU = fits.PrimaryHDU()
    
        # Table HDU with PPXF output data
        cols = []
        cols.append( fits.Column(name='BIN_ID',         format='J', array=ubins             ))
        cols.append( fits.Column(name='CHI2',         format='D', array=chi2             ))
        names = np.char.array( ['V', 'SIGMA', 'H3', 'H4' ] )
        names_err = 'ERR_'+names
        names_err1 = 'FORM_ERR_'+names

        for i in range(ppxf_result.shape[1]):
            cols.append( fits.Column(name=names[i] ,             format='D', array=ppxf_result[:,i]  ))
        for i in range(mc_results.shape[1]):
            cols.append( fits.Column(name=names_err[i] ,             format='D', array=mc_results[:,i]  ))
        for i in range(formal_error.shape[1]):
            cols.append( fits.Column(name=names_err1[i] ,             format='D', array=formal_error[:,i]  ))
            
        dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
        dataHDU.name = 'PPXF_DATA'
    
        # Create HDU list and write to file
        HDUList = fits.HDUList([priHDU, dataHDU])
        HDUList.writeto(outfits_ppxf, overwrite=True)
    
        pipeline.prettyOutput_Done("Writing: "+outfits_ppxf.split('/')[-1])
        logging.info("Wrote: "+outfits_ppxf)
    
    
        # ========================
        # SAVE BESTFIT
        outfits_ppxf = outdir+rootname+'_ppxf-bestfit.fits'
        pipeline.prettyOutput_Running("Writing: "+outfits_ppxf.split('/')[-1])
    
        # Primary HDU
        priHDU = fits.PrimaryHDU()
    
        # need to make the mask array the same shape as the ppxf_bestfit array
        # this is a very inefficient way, surely this can be done better!
        # mask_t = np.zeros(ppxf_bestfit.shape)
        # for i in range(ppxf_bestfit.shape[0]):
        #     mask_t[i, :] = mask_for_original_spectra

        # Table HDU with PPXF bestfit
        cols = []
        cols.append( fits.Column(name='BIN_ID',  format='J',    array=ubins                    ))
        cols.append( fits.Column(name='BESTFIT', format=str(ppxf_bestfit.shape[1])+'D', array=ppxf_bestfit      ))
        # cols.append( fits.Column(name='MASK', format=str(ppxf_bestfit.shape[1])+'D',, array=mask_t      ))
        dataHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
        dataHDU.name = 'FIT'

        cols = []
        # until now the mask is defined according to the ppxf definition, however for output 
        # use 1=masked, 2=outside wav range, 0=ok
        w0 = (mask_for_original_spectra ==0)
        w1 = (mask_for_original_spectra ==1)
        mask_for_original_spectra[w0]=1
        mask_for_original_spectra[w1]=0

        cols.append( fits.Column(name='MASK', format='D', array=mask_for_original_spectra      ))
        maskHDU = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
        maskHDU.name = 'MASK'
    
        # Create HDU list and write to file
        HDUList = fits.HDUList([priHDU, dataHDU,maskHDU])
        HDUList.writeto(outfits_ppxf, overwrite=True)
    
        pipeline.prettyOutput_Done("Writing: "+outfits_ppxf.split('/')[-1])
        logging.info("Wrote: "+outfits_ppxf)
    


# maybe change name to run module stellar kinematics
def runModule_PPXF(configs, logLam, log_spec, log_error, LSF):
   
    print("\033[0;37m"+" - - - - - Running Stellar Kinematics Extraction - - - - - "+"\033[0;39m")
    logging.info(" - - - Running Stellar Kinematics Extraction - - - ")

    #--> some bookkeeping
    # if there is only one spectrum to fit, make sure to reformat it 
    if log_spec.ndim==1:
        log_error= np.expand_dims(log_error,  axis=1)
        log_spec= np.expand_dims(log_spec, axis=1)

    nbins    = log_spec.shape[1]
    ubins    = np.arange(0, nbins)
    npix_in     = log_spec.shape[0]
    LamRange = (np.exp(logLam[0]), np.exp(logLam[-1]) )
    nsims = configs['MC_PPXF']

    # define the velocity scale in kms
    velscale = (logLam[1]-logLam[0])*cvel

    if 'WAV_VACUUM' in configs:
        wav_in_vacuum = configs['WAV_VACUUM']
    else:   
        wav_in_vacuum = False
    velscale_ratio = 1 # velscale ratio is fixed at 1, do not change!
    redshift = configs['REDSHIFT']/cvel
    # generate the LSF interpolation function for simplicity
    LSF_InterpolationFunction  = interp1d(np.exp(logLam)/(1+redshift) ,  LSF /(1+redshift) ,\
         'linear', fill_value = 'extrapolate')
    logging.info("Using full spectral library for PPXF")
    
    #--> generate the stellar templates
    templates_info = util_templates.prepare_sp_templates\
        (configs, velscale, velscale_ratio, LSF_InterpolationFunction, LamRange, wav_in_vacuum)
    templates,lamRange_spmod,logLam_template,min_wav_fit,max_wav_fit, nstpl = \
        templates_info['templates'], templates_info['WavRange'],templates_info['logLam'],\
        templates_info['min_wav_to_fit'],templates_info['max_wav_to_fit'],templates_info['templates'].shape[1]

    # --> cut the spectra to the wavlength range of the templates
    logLam_cut, log_spec_cut, log_error_cut, npix, mask_for_original_spectra,wav_cov_templates =\
        util_templates.cut_spectra_to_match_templates(logLam, log_spec, log_error, max_wav_fit,min_wav_fit)

    # --> starting velocity and dispersion
    # offset in velocity between templatates and cut spectra
    offset = ((logLam_template[0] - logLam_cut[0]) + np.log(1 + \
        configs['REDSHIFT']/cvel ) )*cvel 
    # starting sigma is set to 100 km/s
    start  = [ 0, 100]

    # --> Define goodpixels (here using the masked array ppxf syntax)
    # note we follow here the PPXF masking convention: ie. mask = 0 means the pixel is maked! 1 means it's used in the fit
    # wav_mask_ppxf is a 1D wavelength mask (not a per-pixel mask)
    wav_mask_ppxf, _ = util_templates.get_Mask\
            ('ST_KIN', configs['EMI_FILE'], configs['REDSHIFT'], velscale, logLam_cut, log_error_cut, 
            wav_in_vacuum, masking_width=configs['MASK_WIDTH'])
           
    # merge with the spectral coverage mask
    mask_for_original_spectra [wav_cov_templates] =wav_mask_ppxf
    # get the per-pixel mask by combining the wavelength mask with the info on the Err array
    mask_pixels = util_templates.get_pixel_mask(wav_mask_ppxf, log_error_cut)
    # finally transform to boolean for input to ppxf
    mask_pixels = mask_pixels.astype(bool)
    
    

    # Array to store results of ppxf
    # 4 is the maximum number of moments
    ppxf_result        = np.zeros((nbins,configs['MOM']))
    ppxf_bestfit       = np.zeros((nbins,npix))
    optimal_template   = np.zeros((nbins,templates.shape[0]))
    mc_results         = np.zeros((nbins,configs['MOM']))
    formal_error       = np.zeros((nbins,configs['MOM']))
    chi2               = np.zeros((nbins))

    # ====================
    # Run PPXF
    start_time = time.time()
    if configs['PARALLEL'] == True:
        pipeline.prettyOutput_Running("Running PPXF in parallel mode")
        logging.info("Running PPXF in parallel mode")

        # Create Queues
        inQueue  = Queue()
        outQueue = Queue()
    
        # Create worker processes
        ps = [Process(target=workerPPXF, args=(inQueue, outQueue))
                for _ in range(configs['NCPU'])]
    
        # Start worker processes
        for p in ps: p.start()
    
        # Fill the queue
        for i in range(nbins):
            inQueue.put( ( templates, log_spec_cut[:,i], log_error_cut[:,i], velscale, start, mask_pixels[:,i],\
                            configs['MOM'], configs['ADEG'], configs['MDEG'], offset, velscale_ratio,\
                            nsims, nbins, i ) )
    
        # now get the results with indices
        ppxf_tmp = [outQueue.get() for _ in range(nbins)]
    
        # send stop signal to stop iteration
        for _ in range(configs['NCPU']): inQueue.put('STOP')

        # stop processes
        for p in ps: p.join()
    
        # Get output
        index = np.zeros(nbins)
        for i in range(0, nbins):
            index[i]                        = ppxf_tmp[i][0]
            ppxf_result[i,:configs['MOM']]  = ppxf_tmp[i][1]
            ppxf_bestfit[i,:]               = ppxf_tmp[i][2]
            optimal_template[i,:]           = ppxf_tmp[i][3]
            mc_results[i,:configs['MOM']]   = ppxf_tmp[i][4]
            formal_error[i,:configs['MOM']] = ppxf_tmp[i][5]
            chi2[i]                         = ppxf_tmp[i][6]
        # Sort output
        argidx = np.argsort( index )
        ppxf_result      = ppxf_result[argidx,:]  
        ppxf_bestfit     = ppxf_bestfit[argidx,:]               
        optimal_template = optimal_template[argidx,:]           
        mc_results       = mc_results[argidx,:] 
        formal_error     = formal_error[argidx,:] 
        chi2             = chi2[argidx] 

        pipeline.prettyOutput_Done("Running PPXF in parallel mode", progressbar=True)

    elif configs['PARALLEL'] == False:
        pipeline.prettyOutput_Running("Running PPXF in serial mode")
        logging.info("Running PPXF in serial mode")
        for i in range(0, nbins):
            ppxf_result[i,:configs['MOM']], ppxf_bestfit[i,:], optimal_template[i,:],\
                mc_results[i,:configs['MOM']], formal_error[i,:configs['MOM']], chi2[i] = run_ppxf\
                (templates, log_spec_cut[:,i], log_error_cut[:,i], velscale, start, mask_pixels[:,i],\
                configs['MOM'], configs['ADEG'], configs['MDEG'], offset, velscale_ratio,\
                nsims, nbins, i)
        
        pipeline.prettyOutput_Done("Running PPXF in serial mode", progressbar=True)
    
    print("Running PPXF on %s spectra took %.2fs" % (nbins, time.time() - start_time))
    #print("")
    logging.info("Running PPXF on %s spectra took %.2fs using %i cores" % (nbins, time.time() - start_time, configs['NCPU']))

    # Check if there was a problem with a spectra
    ppxf_probs = np.where(np.isnan(ppxf_result[:,0]) == True)[0]
    if len( ppxf_probs ) != 0:
        print(pipeline.prettyOutput_WarningPrefix()+"There was a problem in the analysis of the following spectra:")
        print("             "+str(ubins[ppxf_probs]))
        print("             The results of these spectra have been set to *np.nan*. Analysis continues!")
        logging.warning("There was a problem in the analysis of the following spectra: \n"+str(ubins[ppxf_probs])+
            " \nThe results of these spectra have been set to *np.nan*. Analysis continues!")
    else:
        logging.info("There were no problems in the analysis.")

    # add back the part of the spectrum that was truncated because of lack of templates
    ppxf_bestfit_1 = np.zeros((nbins,npix_in))
    ppxf_bestfit_1[:, wav_cov_templates]=ppxf_bestfit

    # Save stellar kinematics to file
    save_ppxf(configs['ROOTNAME'], configs['OUTDIR'], ppxf_result, mc_results, formal_error, chi2,
        ppxf_bestfit_1, mask_for_original_spectra, optimal_template, logLam_template, npix, ubins)

    print("\033[0;37m"+" - - - - - Stellar Kinematics done! - - - - -"+"\033[0;39m")
    logging.info(" - - - Stellar Kinematics Done - - - \n")
   
    return(ppxf_result)
        
