import numpy as np
from astropy.io import fits
from astropy import constants

import os
import sys
import shutil
import multiprocessing
import logging

import pdb

try:
    from TardisPipeline.sitePackages.voronoi_2d_binning import voronoi_2d_binning
except:
    from vorbin.voronoi_2d_binning import voronoi_2d_binning

from  TardisPipeline.utilities import util as pipeline
from  TardisPipeline.utilities import util_voronoi as util_voronoi

def dezonify(configs, spaxel_flux, signal,binNum):
     
    # read the best-fit stellar kinematics pPXF fit
    hdu3 = fits.open(configs['OUTDIR']+configs['ROOTNAME']+'_ppxf-bestfit.fits')
    flux_bins_fit = hdu3['FIT'].data['BESTFIT']
    bin_id =  hdu3['FIT'].data['BIN_ID']
    flux_voronoi_rescaled = np.zeros(spaxel_flux.shape )

    for i in range(len(bin_id)):
        wbin = (binNum==i)
        total_continuum_bin = np.sum(signal[wbin])
        flux_voronoi_rescaled[:, wbin] = \
            flux_bins_fit[i,:, None]*signal[wbin] /total_continuum_bin

    resid1 = spaxel_flux -flux_voronoi_rescaled
    
    
    return(resid1)


def define_guess_anr(configs, resid, error,wave, ppxf_result, binNum):
    # need the stellar kinematics for each spaxel
    stars_v = np.zeros(resid.shape[1])

    for i in range(len(np.unique(binNum))):
        wbin = (binNum ==i)
        stars_v[wbin]=ppxf_result[i, 0] 
    
    ha_wav = 6562.80*(1+ configs['REDSHIFT']/constants.c.to('km/s').value ) *\
        ( 1 + stars_v/constants.c.to('km/s').value  )
    wwav_ha = (wave[None, :] > ha_wav[:, None]-10) & (wave[None, :] < ha_wav[:, None]+10)

    amp = np.zeros(resid.shape[1])
    noise = np.zeros(resid.shape[1])

    for kk in range(resid.shape[1]):
        amp[kk] = np.max(resid[wwav_ha[kk, :], kk])
        noise[kk]=np.median(error[wwav_ha[kk, :], kk])  
        
   
    return(amp, noise)

def make_bins_ems(min_snr, target_snr, amp, noise, x_pos, y_pos, pixelsize):
    to_bin  = amp/noise < min_snr 
    binNum_ems_v = np.zeros( amp.shape)

    # Generate the Voronoi bins
    pipeline.prettyOutput_Running("Defining the Voronoi bins")
    logging.info("Defining the Voronoi bins")

    
    if to_bin.sum()>0:
        binNum_ems, xNode, yNode, _, _, sn_ems, nPixels_ems, _ = \
            voronoi_2d_binning(x_pos[to_bin], y_pos[to_bin],\
            amp[to_bin], noise[to_bin], target_snr, plot=0, quiet=0, pixelsize=pixelsize)

        binNum_ems_v[to_bin]=binNum_ems

    binNum_ems_v[~to_bin] = np.max(binNum_ems)+1+ np.arange(np.sum(~to_bin))
    xNode = np.append(xNode, x_pos[~to_bin])
    yNode = np.append(yNode, x_pos[~to_bin])
    sn_ems = np.append(sn_ems, amp[~to_bin]/noise[~to_bin])
    nPixels_ems = np.append(nPixels_ems, np.ones(np.sum(~to_bin)))


    pipeline.prettyOutput_Done("Defining the Voronoi bins")
    print("             "+str(np.max(binNum_ems_v)+1)+" voronoi bins generated!")
    logging.info(str(np.max(binNum_ems_v)+1)+" Voronoi bins generated!")


    return(binNum_ems_v, xNode, yNode,  sn_ems, nPixels_ems)
    

def emission_lines_binning_RunAll(configs, spaxel_flux, spaxel_error, wave, 
            signal, ppxf_result, binNum, x_pos, y_pos, pixelsize, velscale, lsf, wcs):

    min_snr=30
    target_snr=60

    resid  = dezonify(configs, spaxel_flux, signal,binNum)

    amp, noise = define_guess_anr(configs, resid, spaxel_error, wave,ppxf_result, binNum)
    
    binNum_ems, xNode,yNode,  sn_ems, nPixels_ems = \
        make_bins_ems(min_snr, target_snr, amp, noise, x_pos, y_pos, pixelsize)
    
    bin_data, bin_error, bin_flux= util_voronoi.sum_spectra_in_bins( binNum_ems, 
                                spaxel_flux, spaxel_error)
    
    util_voronoi.save_table(configs['ROOTNAME'], configs["OUTDIR"], x_pos, y_pos, amp, amp/noise, 
               binNum_ems, np.unique(binNum_ems), xNode, 
               yNode, sn_ems, nPixels_ems, pixelsize, wcs, flag='ems')
    
    util_voronoi.save_vorspectra(configs['ROOTNAME'], configs["OUTDIR"], bin_data, bin_error, 
                    velscale, np.log(wave), lsf, flag='ems')

    binned_cube_ems={}
    binned_cube_ems["log_spec"]=bin_data
    binned_cube_ems["log_error"]=bin_error
    binned_cube_ems["logLam"]=np.log(wave)
    binned_cube_ems["velscale"]=velscale
    binned_cube_ems["LSF"] = lsf
    
    return(binNum_ems,binned_cube_ems )

def determine_stellar_kinematics(binNum_stkin, binNum_ems, ppxf_result):
    
    ubins = np.unique(binNum_stkin)
    nmom=ppxf_result.shape[1]
    ppxf_result_spaxels = np.zeros( (len(binNum_stkin), nmom))+np.nan
    for i in range(len(ubins) ):
            windx = (binNum_stkin ==i)
            ppxf_result_spaxels[windx, :]=ppxf_result[None, i]
        
    ubins2 = np.unique(binNum_ems)
    ppxf_result_emsbins = np.zeros( (len(ubins2), nmom))+np.nan
    st_kin_fixed = np.zeros( len(ubins2))+np.nan
    for i in range(len(ubins2)):
            windx = np.where(binNum_ems ==i)[0]
            if len(np.unique(ppxf_result_spaxels[windx, 0]))==1:
                ppxf_result_emsbins[i, :]=ppxf_result_spaxels[windx[0], :]
                st_kin_fixed[i]=1
            else:
                for kk in range(nmom-1):
                    ppxf_result_emsbins[i, kk]=np.nanmedian(ppxf_result_spaxels[windx, kk])
                    
                st_kin_fixed[i]=-1
                
    ppxf_result_emsbins[np.isfinite(ppxf_result_emsbins)==0]=0.0
    return(ppxf_result_emsbins, st_kin_fixed)

def load_ems_voronoi(configs):
    binned_cube_ems =util_voronoi.load_voronoi(configs, flag='ems')
    binNum_ems = util_voronoi.load_voronoi_table(configs, flag='ems')
    return(binNum_ems, binned_cube_ems)






        
    




