#!/usr/bin/env python

import os
os.environ["MKL_NUM_THREADS"]     = "1" 
os.environ["NUMEXPR_NUM_THREADS"] = "1" 
os.environ["OMP_NUM_THREADS"]     = "1" 

import numpy as np
from   astropy.io import fits, ascii
import warnings
warnings.filterwarnings("ignore")
import importlib
import logging
import time
import datetime

import matplotlib
matplotlib.use('pdf')

from  TardisPipeline.utilities import util as pipeline
from  TardisPipeline.utilities import util_initialize as util_init
from  TardisPipeline.utilities  import util_prepare
from  TardisPipeline.utilities  import util_voronoi as util_binning
from  TardisPipeline.utilities  import util_ppxf
from  TardisPipeline.utilities  import util_ppxf_stellarpops
from  TardisPipeline.utilities  import util_ppxf_emlines
from  TardisPipeline.utilities  import util_dezonify
from  TardisPipeline.utilities  import util_mapify

import pdb

def run_pipeline(galnumber,path):
# ==============================================================================
#                                 S T A R T U P 
# ==============================================================================
    # Read config-file and setup the configs structure
    configs, tasks = util_init.setupConfig(galnumber, path)
    if tasks['SKIP_GALAXY'] == True: 
        message = " - - - GALAXY IS SKIPPED! - - - "
        print(pipeline.prettyOutput_FailedPrefix()+message)
        logging.critical(message)
        return(None)
    
    # Determine what the pipeline has to do
    tasks= util_init.determine_tasks(configs, tasks)
    if tasks['SKIP_GALAXY'] == True: 
        message = " - - - GALAXY IS SKIPPED! - - - "
        print(pipeline.prettyOutput_FailedPrefix()+message)
        logging.critical(message)
        return(None)

# ==============================================================================
#                            INPUT DATA AND REBINNING 
# ==============================================================================
  
    print("\033[0;37m"+" - - - - - Reading input data - - - - - "+"\033[0;39m")
    logging.info(" - - - Reading input data - - - ")

    #  Import the readData routine, depending on the chosen instrument

    readCUBE = importlib.import_module('.readData.'+configs['IFU'], package='TardisPipeline')
    # Read IFU cube
    cube, wcs, configs = readCUBE.read_cube(configs, tasks)

    if tasks['LOG_REBIN_SPECTRA']==True:
        
        # performs log-rebinning of all the indiviudal spaxels 
        # if already in log-wav form it just populates the log_spec etc keys of the cube dict
        cube= util_prepare.log_rebinning(configs, cube)
   
    else:
        # Load all spectra
        cube= util_prepare.loadAllSpectra(configs['ROOTNAME'], configs['OUTDIR'], cube)
        # wcs = util_prepare.load_WCS(configs)

# ==============================================================================
#                            BINNING 
# ==============================================================================
    print("\033[0;37m"+" - - - - - Running Binning Step - - - - - "+"\033[0;39m")
    logging.info(" - - - Running Binning - - - ")

    # Selecting those spaxels with SNR above some threshold
    spx_class_sn= util_prepare.apply_snr_threshold_fb(cube['snr'], configs['MIN_SNR'])

    if tasks['DEFINE_BINS'] == 1:
        # Define Voronoi bins and save table
        binNum,  binNum_long= util_binning.define_voronoi_bins\
                 (configs, tasks, cube['x'], cube['y'], cube['signal'], cube['noise'], cube['pixelsize'], \
                  cube['snr'], spx_class_sn, wcs, 'kinematics')

    elif tasks['DEFINE_BINS'] == 2:
        binNum,  binNum_long=util_binning.define_bins_from_file(configs, cube['x'], cube['y'],\
             cube['signal'], cube['noise'], cube['pixelsize'], wcs, configs['PATH_BIN_MAP'])
        spx_class_sn['INSIDE'] = (binNum_long >= 0)
    elif tasks['DEFINE_BINS'] == 0:
        # define the binNum, which is the only thing needed to do the next step!
        binNum_long = np.array( fits.open(configs['OUTDIR']+configs['ROOTNAME']+'_table.fits')[1].data.BIN_ID )
        spx_class_sn['INSIDE'] = (binNum_long >= 0)
        binNum = binNum_long[binNum_long >= 0]
        

    if tasks['APPLY_BINS'] == True:
        # sums the log-rebinned spectra which belong to the same Voronoi bin
        binned_cube = util_binning.apply_bins(configs, tasks, binNum, cube['log_spec'][:,spx_class_sn['INSIDE']], \
                 cube['log_error'][:,spx_class_sn['INSIDE']], cube['velscale'], 
                 cube['logLam'], cube['LSF_InterpolationFunction'], 'log')
    else:
        #Loads the voronoi-binned spectra and re-constitutes the binned_cube dict.
        binned_cube = util_binning.load_binspectra(configs)

    if tasks['SFH'] == True:
        if tasks['DEFINE_BINS_SFH'] == True:
            raise NotImplementedError('Not implemented')
        else:
            # copy the file and load the Voronoi bins from the stellar kinematics intermediate file
            os.system('cp '+ configs['OUTDIR'] + configs['ROOTNAME'] + '_table.fits '+configs['OUTDIR']+configs['ROOTNAME']+'_table_SFH.fits')
            binNum_SFH = np.array(fits.open(configs['OUTDIR'] + configs['ROOTNAME'] + '_table_SFH.fits')[1].data.BIN_ID)
            binNum_SFH = binNum_SFH[np.where(binNum_SFH >= 0)[0]]

        if tasks['APPLY_BINS_SFH'] == True:
            raise NotImplementedError('Not implemented')
        else:
            # copy the file and load the summed spectra from the kinematics intermediate files
            os.system('cp ' + configs['OUTDIR'] + configs['ROOTNAME'] + '_VorSpectra.fits ' + \
               configs['OUTDIR'] + configs['ROOTNAME'] + '_VorSpectra_SFH.fits')
            binned_cube_SFH = util_binning.load_binspectra(configs, SFH=True)



    print("\033[0;37m"+" - - - - - Binning done! - - - - -"+"\033[0;39m")
    logging.info(" - - - Binning Done - - - \n")

# ==============================================================================
#                             D O   A N A L Y S I S 
# ==============================================================================

    #RUN PPXF
    if tasks['PPXF_ST_KIN'] ==True:
        # compute stellar kinematics
        #print("\033[0;37m"+" - - - - - Determining stellar kinematics! - - - - -"+"\033[0;39m")
        ppxf_result = util_ppxf.runModule_PPXF(configs, binned_cube['logLam'], binned_cube['log_spec'], 
            binned_cube['log_error'], binned_cube['LSF'])
    elif tasks['PPXF_ST_KIN'] ==False:
        # load the stellar kinematics output
        print(pipeline.prettyOutput_WarningPrefix()+"Loading stellar kinematics from file! ")
        logging.warning("Loading stellar kinematics from file!\n")
        ppxf_result = util_prepare.load_PPXF_result(configs)

    if tasks['PPXF_SFH'] ==True:
        #print("\033[0;37m"+" - - - - - Determining stellar population properties! - - - - -"+"\033[0;39m")

        util_ppxf_stellarpops.runModule_PPXF_stellarpops(configs, binned_cube_SFH['logLam'], binned_cube_SFH['log_spec'],
                binned_cube_SFH['log_error'], binned_cube_SFH['LSF'], binNum, ppxf_result)
        

    elif tasks['PPXF_SFH'] ==False:
        print(pipeline.prettyOutput_WarningPrefix()+"Skipping PPXF (SFH)! ")
        logging.warning("Skipping PPXF (SFH) analysis!\n")

    
    #RUN Emission lines fitter
    if tasks['EMS_LINES'] ==1:
        print("\033[0;37m"+" - - - - - Fitting emission lines in bins - - - - -"+"\033[0;39m")
        logging.info(" - - - Fitting emission lines in bins - - - \n")
        util_ppxf_emlines.runModule_PPXF_emlines(configs, binned_cube['logLam'], binned_cube['log_spec'], 
            binned_cube['log_error'], binned_cube['LSF'], bin_id=binNum, ppxf_results=ppxf_result)
    elif tasks['EMS_LINES']==2:
        print("\033[0;37m"+" - - - - - Fitting emission lines in spaxels - - - - -"+"\033[0;39m")
        logging.info(" - - - Fitting emission lines in spaxels - - - \n")
        LSF = cube['LSF_InterpolationFunction'](np.exp(cube['logLam']))

        nbins = np.max(binNum_long)+1
        ppxf_result_spaxels = np.zeros( (len(binNum_long), ppxf_result.shape[1]))
        for i in range(int(nbins)):
            windx = (binNum_long ==i)
            ppxf_result_spaxels[windx, :]=ppxf_result[i]
        
        util_ppxf_emlines.runModule_PPXF_emlines(configs, cube['logLam'], cube['log_spec'] ,
            cube['log_error'], LSF , 
            bin_id=None, ppxf_results=ppxf_result_spaxels)
    # elif tasks['EMS_LINES']==3:   
    #     print("\033[0;37m"+" - - - - - Fitting emission lines in bins based on line emission - - - - -"+"\033[0;39m")
    #     logging.info(" - - - Fitting emission lines in Voronoi bins based on line emission - - - \n")

    #     if tasks['REVORONOI_LINES']==1:
    #         LSF = cube['LSF_InterpolationFunction'](np.exp(cube['logLam']))
    #         binNum_ems, binned_cube_ems = util_dezonify.emission_lines_binning_RunAll(configs, 
    #                 cube['log_spec'][:,spx_class_sn['INSIDE']], 
    #                 cube['log_error'][:,spx_class_sn['INSIDE']], np.exp(cube['logLam']), 
    #                 cube['signal'][spx_class_sn['INSIDE']], ppxf_result, binNum, 
    #                 cube['x'][spx_class_sn['INSIDE']], cube['y'][spx_class_sn['INSIDE']], 
    #                 cube['pixelsize'], cube['velscale'], LSF, wcs)
    #     elif tasks['REVORONOI_LINES']==0:
    #         binNum_ems, binned_cube_ems = util_dezonify.load_ems_voronoi(configs)

    #     ppxf_result_emsbins, st_kin_fixed = util_dezonify.determine_stellar_kinematics(binNum, \
    #         binNum_ems, ppxf_result)
  
    #     util_ppxf_emlines.runModule_PPXF_emlines(configs, tasks, binned_cube_ems['logLam'], \
    #         binned_cube_ems['log_spec'], 
    #         binned_cube_ems['log_error'], binned_cube_ems['LSF'] , 
    #         bin_id=binNum_ems, ppxf_results=ppxf_result_emsbins)
    

    # MAKE MAPS
    if tasks['MAPS']==True:
        print("\033[0;37m"+" - - - - - Generating maps - - - - -"+"\033[0;39m")
        logging.info(" - - - Generating maps - - - \n")
        util_mapify.mapify_all(configs, tasks, wcs)
    # EXAMPLE OF FRACTION MAPS
    # if tasks['SFH'] == True:
    #     util_mapify.mapify_weights(configs, wcs, 1, age_low = 0.03, age_top=0.15)
    #     util_mapify.mapify_weights(configs, wcs, 2, age_low = 0.25, age_top=0.6)
    #     util_mapify.mapify_weights(configs, wcs, 3, age_low = 1.0, age_top=3.0)
    #     util_mapify.mapify_weights(configs, wcs, 4, age_low = 1.75)
    #     util_mapify.mapify_weights(configs, wcs, 5, z_top = -0.35)
    #     util_mapify.mapify_weights(configs, wcs, 6, z_low = 0.06)


    print(" *************** ")
    print(" **   DONE!   ** ")
    print(" *************** ")
    logging.info(" - DONE! :)")

def run_all(path='Config.ini'):
    # read the configuration file for this run
    configs = util_init.readConfig(path)
    # read the list of galaxies
    galaxies = util_init.readGalaxies(configs)
    ngalaxies= len(galaxies)
        
    for galnumber in range(ngalaxies):
        run_pipeline(galnumber,path)
        print("\n")

# ==============================================================================
#                           M A I N   F U N C T I O N 
# ==============================================================================
if __name__ == '__main__':
    run_all()
    
