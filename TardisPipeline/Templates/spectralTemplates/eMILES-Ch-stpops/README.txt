README

eMILES models with
- Chabirer IMF (Ch)
- IMF slope of 1.3 
- Basti isochrones  (iT)
- base alpha/Fe (baseFe)

Only a subset of the full range of available ages and metallicities are included.
Ages = 0.03, 0.05, 0.08, 0.15, 0.25, 0.40, 0.60, 1.0, 1.75, 3.0, 5.0, 8.5, 13.5 Gyr
[Z/H] = -1.49, -0.96, -0.35, +0.06, +0.26, +0.4


NOTE: SSP for age< 150 Myr have zero flux at longer wavelengths so they are unsafe to use if data with lambda > 9000 is included

See details of the naming convention here
http://www.iac.es/proyecto/miles/pages/ssp-models/name-convention.php


Spectral resolution given in the included spectral resolution file, see here
http://www.iac.es/proyecto/miles/pages/spectral-energy-distributions-seds/e-miles.php

