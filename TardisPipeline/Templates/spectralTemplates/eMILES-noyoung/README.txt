README

E-MILES models with
- Chabrier IMF
- IMF slope of 1.3
- Basti isochrones 
- base a/fe 

Only a subset of the full range of available ages and metallicities are included.
Ages = 0.15, 0.25, 0.45, 0.9, 1.75, 3.5, 7.0, 14 Gyr
[Z/H] = -1.49, -0.35, +0.06, +1.3

Same as sMILES-subset but only ages > 150 Myr are included  in order to the full wavelength coverage out to 9000 \AA 
NOTE: the  younger spectral have zero flux at longer wavelengths so they are unsafe to use if data with lambda > 9000 is included

See details of the naming convention here
http://www.iac.es/proyecto/miles/pages/ssp-models/name-convention.php


Spectral resolution given in the included spectral resolution file, see here
http://www.iac.es/proyecto/miles/pages/spectral-energy-distributions-seds/e-miles.php

 
NOTE: In versions earlierthan v1.1 this folder was called eMILES-short2