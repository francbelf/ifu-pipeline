README

eMILES models with
- Chabirer IMF (Ch)
- IMF slope of 1.3 
- Padova Isochrones

With the addition of young models as described in Asa'd et al. (2017). Models stitched and compiled together by Ismael Pessa.

18 Age bins [6.3 Myr to 15.8 Gyr]
Ages = 0.0063, 0.01, 0.016, 0.025, 0.040, 0.063, 0.100, 0.150, 0.250, 0.45, 0.70, 1.3, 2.0, 3.2, 5.6, 8.9, 15.8 Gyr

4 [Z/H] bins
[Z/H] = -0.7, -0.4, 0.0, 0.22 for the 12 templates older than 63 Myr
[Z/H] = -0.7, -0.4, 0.0, 0.41 for the 5 templates younger than 63 Myr

The lowest metallicity bin ([Z/H]= -1.33) was removed from the grid.

Spectral resolution given in the included spectral resolution file, see here
http://www.iac.es/proyecto/miles/pages/spectral-energy-distributions-seds/e-miles.php

