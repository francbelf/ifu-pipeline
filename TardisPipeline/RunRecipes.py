import os
os.environ["MKL_NUM_THREADS"]     = "1" 
os.environ["NUMEXPR_NUM_THREADS"] = "1" 
os.environ["OMP_NUM_THREADS"]     = "1" 

import numpy as np
from   astropy.io import fits, ascii
import warnings
warnings.filterwarnings("ignore")
import importlib
import logging
import time
import datetime

import matplotlib
matplotlib.use('pdf')

from  TardisPipeline.utilities import util as pipeline
from  TardisPipeline.utilities import util_initialize as util_init
from  TardisPipeline.utilities  import util_prepare
from  TardisPipeline.utilities  import util_voronoi
from  TardisPipeline.utilities  import util_ppxf
from  TardisPipeline.utilities  import util_ppxf_stellarpops
from  TardisPipeline.utilities  import util_ppxf_emlines
from  TardisPipeline.utilities  import util_dezonify
from  TardisPipeline.utilities  import util_mapify

import pdb


class Tardis(object):
    def __init__(self, tagetname, config_path='.Config.ini'):
        # read the configuration file for this run
        configs = util_init.readConfig(path)
        # read the list of galaxies
        galaxies = util_init.readGalaxies(configs)
        # determine which galaxy to fit
        galnumber = np.where(galaxies['Name']==tagetname)[0]

        # processe the configuration for this galaxy
        configs, tasks = util_init.setupConfig(galnumber, config_path)
        # Determine what the pipeline has to do
        tasks= util_init.determine_tasks(configs, tasks)
        self.tasks=tasks
        self.configs=configs
        

    def run_LogRebin(self):
        
        print("\033[0;37m"+" - - - - - Reading input data! - - - - - "+"\033[0;39m")
        logging.info(" - - - Reading input data - - - ")

        #  Import the readData routine, depending on the chosen instrument

        readCUBE = importlib.import_module('.readData.'+self.configs['IFU'], package='TardisPipeline')
        # Read IFU cube
        self.cube, self.wcs, self.configs = readCUBE.read_cube(self.configs, self.tasks)

        if self.tasks['LOG_REBIN_SPECTRA']==True:
            # performs log-rebinning of all the indiviudal spaxels 
            # if already in log-wav form it just populates the log_spec etc keys of the cube dict
            self.cube= util_prepare.log_rebinning(self.configs, self.cube)
        else:
            # Load all spectra
            self.cube= util_prepare.loadAllSpectra(self.configs['ROOTNAME'], 
                self.configs['OUTDIR'], self.cube)
    
    def run_Binning(self):
        print("\033[0;37m"+" - - - - - Running Binning Step! - - - - - "+"\033[0;39m")
        logging.info(" - - - Running Binning - - - ")

        # Selecting those spaxels with SNR above some threshold
        spx_class_sn= util_prepare.apply_snr_threshold_fb(self.cube['snr'], self.configs['MIN_SNR'])

        if self.tasks['DEFINE_VORONOI_BINS'] == True:
            # Define Voronoi bins and save table
            binNum = util_voronoi.define_voronoi_bins\
                    (self.configs, self.tasks, self.cube['x'], self.cube['y'], self.cube['signal'], 
                    self.cube['noise'], self.cube['pixelsize'], \
                    self.cube['snr'], spx_class_sn, self.wcs, 'kinematics')
        else:
            # define the binNum, which is the only thing needed to do the next step!
            binNum = np.array( fits.open(self.configs['OUTDIR']+self.configs['ROOTNAME']+'_table.fits')[1].data.BIN_ID )
            spx_class_sn['INSIDE'] = (binNum >= 0)
            binNum = binNum[binNum >= 0]
            

        if self.tasks['APPLY_VORONOI_BINS'] == True:
            # sums the log-rebinned spectra which belong to the same Voronoi bin
            binned_cube = util_voronoi.apply_voronoi_bins(self.configs, self.tasks, binNum, 
                self.cube['log_spec'][:,spx_class_sn['INSIDE']], \
                self.cube['log_error'][:,spx_class_sn['INSIDE']], self.cube['velscale'], 
                self.cube['logLam'], self.cube['LSF_InterpolationFunction'], 'log')
        else:
            #Loads the voronoi-binned spectra and re-constitutes the binned_cube dict.
            binned_cube = util_voronoi.load_voronoi(self.configs)

        print("\033[0;37m"+" - - - - - Binning done! - - - - -"+"\033[0;39m")
        logging.info(" - - - Binning Done - - - \n")`