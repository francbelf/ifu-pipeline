# ======================================
#             R E A D M E
# ======================================

This repository contains the Data Analysis Pipeline (DAP) used to derive high-level data producrts from the PHANGS-MUSE data. It is derived in large parts from the GIST code (Bittner et al., 2019). A full description of the DAP is can be found in Emsellem et al. 2021.

## Installation

First clone the full repo by or clicking the download button on GitLab

`git clone --recursive https://gitlab.com/francbelf/ifu-pipeline.git`

generate a python environment with all the required dependencies
(it is not obligatory of course to generate a separate python environment, this is just provived for convenience. The pipeline will install the required dependences in the install step anyway)
`conda env create -f requirementsPython3.yml`

activate the environment
`conda activate TardisPipeline`

install the pipeline
`python -m pip install -e .`
Now the pipeline is within your python path!

## Usage

The pipeline input is found in the two input files, a configuration file and a galaxy list file
The configuration file (Config.ini) sets the paramters of the key analysis steps, and must also contain explicit paths of the data directory and the galaxy list file. For a test run the user can leave the values of other parameters fixed to those provided in the example configuration files and just change the paths to data and galaxy list file (DATADIR, GALAXY_LIST).

The galaxy list file is a plain ascii file containing galaxy name (the input datacubes in DATADIR needs to have the same name and end in ".fits"), redshift/systemic velocity and, optionally, the value of foreground Galactic extinction. If the third column (Galactic extinciton) is not provided it is assumed to be zero.

To run the "PHANGS-style" full set of pipeline recipes one can simply

Activate the environment
`source activate TardisPipeline`

Open an ipython prompt
`ipython`

Import the pipeline and run all recipes
`from TardisPipeline import MainPipeline`
`MainPipeline.run_all(path=config_path)`
`config_path` is the path to the Config.ini file. If you do not define `config_path` the code will look for a Config.ini file in the current working directory.

### Running a list of recipes
This is currently in development.

### Available Templates
`from TardisPipeline.utilities import util` 
to print a list of the available stellar continuum templates (many more are available but have not been uploaded them to the git repo) do:
`util.printTemplates()`
to print a lit of the templates available for stellar population analysis (these are a subset of the full set of stellar tempaltes avaialble) do:
`util.printStellarPopTemplates()`

### Available IFUs

to print a list of the supported IFUs
`from TardisPipeline.utilities import util` 
`util.printIFUs()`

To use the pipeline with a new IFU you just need to write a new reading routing for the data. In general, this will require you to write new python code that mimicks that of the IFU instruments already available, in order to specifiy the format the flux, errors, line spread function and optional masks for the new dataset. 

